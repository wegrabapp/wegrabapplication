//
//  CityDetails.h
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CityDetails : NSObject

@property (nonatomic, strong)NSString * cityName;
@property (nonatomic, strong)NSString * cityId;

@end
