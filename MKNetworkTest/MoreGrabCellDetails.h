//
//  MoreGrabCellDetails.h
//  WeGrab
//
//  Created by satheesh on 1/9/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoreGrabCellDetails : NSObject

@property (nonatomic, retain) NSString *dealID;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *dealDescription;
@property (nonatomic, retain) NSString *cityName;
@property (nonatomic, retain) NSString *countryName;
@property (nonatomic, retain) NSString *dealImage;
@property (nonatomic, retain) NSString *oldPrice;
@property (nonatomic, retain) NSString *currentPrice;

@end
