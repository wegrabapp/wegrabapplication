//
//  CountryDetails.h
//  WeGrab
//
//  Created by satheesh on 1/5/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CountryDetails : NSObject

@property (nonatomic, retain) NSString *country_id;
@property (nonatomic, retain) NSString *countryName;
@property (nonatomic, retain) NSArray *cities;
@property (nonatomic, retain) NSArray *city_id;
@end
