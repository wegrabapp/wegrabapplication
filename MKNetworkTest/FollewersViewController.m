//
//  FollewersViewController.m
//  WeGrab
//
//  Created by satheesh on 1/12/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "FollewersViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "SectionInfo.h"
#import "SectionInfoFollowers.h"
#import "FollowerDetails.h"
#import "FollowerTableViewCell.h"

@interface FollewersViewController ()
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * followerList;
- (void) setCategoryArray;

@end

@implementation FollewersViewController

@synthesize sectionInfoArray;
@synthesize openSectionIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = YES;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Followers";
    
    self.followersTableView.scrollEnabled = NO;
    
    self.openSectionIndex = NSNotFound;
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getCountryData];
    }
    
    self.followersTableView.sectionHeaderHeight = 45;
    self.followersTableView.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
}


-(void)getCountryData{
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/follower_service"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                    // [userLoginStatus objectForKey:@"UserId"], @"user_id",
                                    // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return [self.followerList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SectionInfoFollowers *array = [self.sectionInfoArray objectAtIndex:section];
    
    NSInteger rows = 1;
    NSLog(@"row count...%d",rows);
    return (array.open) ? rows : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    FollowerTableViewCell *cell = (FollowerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"FollowerTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    FollowerDetails * tempFollowerDetails;
    tempFollowerDetails = [self.followerList objectAtIndex:indexPath.section];
    
    NSMutableDictionary *followersdetailsDictionary = [[NSMutableDictionary alloc]init];
    NSLog(@"%@",tempFollowerDetails.followerDetails);
    followersdetailsDictionary = tempFollowerDetails.followerDetails;
    NSLog(@"%@",followersdetailsDictionary);
    
    cell.emailValueLbl.text = [followersdetailsDictionary objectForKey:@"email"];
    cell.amountEarnedValueLbl.text = [followersdetailsDictionary objectForKey:@"amount_earned"];
    cell.statusValueLbl.text = [followersdetailsDictionary objectForKey:@"Status"];
    
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfoFollowers *array  = [self.sectionInfoArray objectAtIndex:section];
    if (!array.sectionView)
    {
        NSString *title = array.follower.followerName;
        array.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, self.followersTableView.bounds.size.width, 45) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionView;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (void) sectionClosed : (NSInteger) section{
    
    SectionInfoFollowers *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.followersTableView numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.followersTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;

}

- (void) sectionOpened : (NSInteger) section
{
    SectionInfoFollowers *array = [self.sectionInfoArray objectAtIndex:section];
    
    array.open = YES;
    NSInteger count = 1;
    NSLog(@"11.....%d",count);
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    if (previousOpenIndex != NSNotFound)
    {
        SectionInfoFollowers *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        sectionArray.open = NO;
        NSInteger counts = 1;
        [sectionArray.sectionView toggleButtonPressed:FALSE];
        for (NSInteger i = 0; i<counts; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.followersTableView beginUpdates];
    [self.followersTableView insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
    [self.followersTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.followersTableView endUpdates];
    self.openSectionIndex = section;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    followerDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    
    followerDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",followerDataDictionary);
    
    if ([ApplicationDelegate isValid:[followerDataDictionary objectForKey:@"amount_earn_curr_month"]]) {
        self.amountEarnedTHisMonthValueLbl.text = [NSString stringWithFormat:@"%@ $",[followerDataDictionary objectForKey:@"amount_earn_curr_month"]];
    }
    else{
        self.amountEarnedTHisMonthValueLbl.text = [NSString stringWithFormat:@"%@ $",@"0"];
    }
    
    if ([ApplicationDelegate isValid:[followerDataDictionary objectForKey:@"total_amount_earned"]]){
        self.totalAmountEarnedValueLbl.text = [NSString stringWithFormat:@"%@ $",[followerDataDictionary objectForKey:@"total_amount_earned"]];
    }
    else{
        self.totalAmountEarnedValueLbl.text = [NSString stringWithFormat:@"%@ $",@"0"];
    }
    
    if ([[followerDataDictionary objectForKey:@"follower"] count] > 0) {
        
        NSMutableArray *followerArray = [[NSMutableArray alloc] initWithCapacity:[[followerDataDictionary objectForKey:@"follower"] count]];
        for (NSDictionary *dictionary in [followerDataDictionary objectForKey:@"follower"]) {
            FollowerDetails *followerDetails = [[FollowerDetails alloc] init];
            followerDetails.followerName = [dictionary objectForKey:@"FollowerName"];
            followerDetails.followerUserId = [dictionary objectForKey:@"user_id"];
            followerDetails.followerDetails = [dictionary objectForKey:@"followerDetails"];
            [followerArray addObject:followerDetails];
        }
        self.followerList = followerArray;
        
        if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.followersTableView])) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (FollowerDetails *follower in self.followerList) {
                SectionInfoFollowers *section = [[SectionInfoFollowers alloc] init];
                section.follower = follower;
                section.open = NO;
                
                NSNumber *defaultHeight = [NSNumber numberWithInt:100];
                //NSInteger count = [[section.follower followerDetails] count];
                NSInteger count = 1;
                //NSLog(@"%d count...",count);
                
                for (NSInteger i= 0; i<count; i++) {
                    
                    NSLog(@"5.....%@",[section.follower followerName]);
                    [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                    NSLog(@"6......%@",section);
                }
                
                [array addObject:section];
            }
            //NSLog(@"%@",array);
            self.sectionInfoArray = array;
        }
        
        
        [self.followersTableView reloadData];
        
        CGRect frame = self.followersTableView.frame;
        frame.size.height = self.followersTableView.contentSize.height;
        self.followersTableView.frame = frame;
        
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.followersTableView.frame.origin.y + self.followersTableView.frame.size.height);
        
        [ApplicationDelegate removeProgressHUD];
    }
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect frame = self.followersTableView.frame;
    frame.size.height = self.followersTableView.contentSize.height;
    self.followersTableView.frame = frame;
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.followersTableView.frame.origin.y + self.followersTableView.frame.size.height);
}


@end
