//
//  MyAccountBalanceViewController.m
//  WeGrab
//
//  Created by satheesh on 1/27/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MyAccountBalanceViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "SectionInfo.h"
#import "SectionInfoBalance.h"
#import "AccountBalanceDetails.h"
#import "MyAccountCell.h"

@interface MyAccountBalanceViewController ()
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * balanceList;
- (void) setCategoryArray;
@end

@implementation MyAccountBalanceViewController

@synthesize sectionInfoArray;
@synthesize openSectionIndex;

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Balance";
    
    self.myAccountTableView.scrollEnabled = NO;
    
    self.openSectionIndex = NSNotFound;
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getBalanceData];
    }
    
    self.myAccountTableView.sectionHeaderHeight = 45;
    self.myAccountTableView.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
}

-(void)getBalanceData{
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/my_account_bal"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.balanceList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SectionInfoBalance *array = [self.sectionInfoArray objectAtIndex:section];
    
    NSInteger rows = 1;
    NSLog(@"row count...%d",rows);
    return (array.open) ? rows : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    MyAccountCell *cell = (MyAccountCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyAccountCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    AccountBalanceDetails * tempAccountBalanceDetails;
    tempAccountBalanceDetails = [self.balanceList objectAtIndex:indexPath.section];
    
    NSMutableDictionary *BalancedetailsDictionary = [[NSMutableDictionary alloc]init];
    NSLog(@"%@",tempAccountBalanceDetails.historyDetails);
    BalancedetailsDictionary = tempAccountBalanceDetails.historyDetails;
    NSLog(@"%@",BalancedetailsDictionary);
    
    cell.transactionDateValueLbl.text = [BalancedetailsDictionary objectForKey:@"Transaction Date"];
    cell.expiryDateValueLbl.text = [BalancedetailsDictionary objectForKey:@"Expiary Date"];
    cell.amountValueLbl.text = [BalancedetailsDictionary objectForKey:@"Amount"];
    cell.statusValueLbl.text = [BalancedetailsDictionary objectForKey:@"Status"];
    
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfoBalance *array  = [self.sectionInfoArray objectAtIndex:section];
    if (!array.sectionView)
    {
        NSString *title = array.balanceDetails.history_name;
        array.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, self.myAccountTableView.bounds.size.width, 45) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionView;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


- (void) sectionClosed : (NSInteger) section{
    
    SectionInfoBalance *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.myAccountTableView numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.myAccountTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
    
}

- (void) sectionOpened : (NSInteger) section
{
    SectionInfoBalance *array = [self.sectionInfoArray objectAtIndex:section];
    
    array.open = YES;
    NSInteger count = 1;
    NSLog(@"11.....%d",count);
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    if (previousOpenIndex != NSNotFound)
    {
        SectionInfoBalance *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        sectionArray.open = NO;
        NSInteger counts = 1;
        [sectionArray.sectionView toggleButtonPressed:FALSE];
        for (NSInteger i = 0; i<counts; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.myAccountTableView beginUpdates];
    [self.myAccountTableView insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
    [self.myAccountTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.myAccountTableView endUpdates];
    self.openSectionIndex = section;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    accountBalanceDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    
    accountBalanceDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",accountBalanceDataDictionary);
    
    if ([ApplicationDelegate isValid:[accountBalanceDataDictionary objectForKey:@"monthly_balance"]]) {
        self.thisMonthBalanceValueLbl.text = [NSString stringWithFormat:@"%@ $",[accountBalanceDataDictionary objectForKey:@"monthly_balance"]];
    }
    else{
        self.thisMonthBalanceValueLbl.text = [NSString stringWithFormat:@"%@ $",@"0"];
    }
    
    if ([ApplicationDelegate isValid:[accountBalanceDataDictionary objectForKey:@"total_amount_earned"]]){
        self.totalBalanceValueLbl.text = [NSString stringWithFormat:@"%@ $",[accountBalanceDataDictionary objectForKey:@"total_amount_earned"]];
    }
    else{
        self.totalBalanceValueLbl.text = [NSString stringWithFormat:@"%@ $",@"0"];
    }
    
    if ([[accountBalanceDataDictionary objectForKey:@"account_history"] count] > 0) {
        
        NSMutableArray *balanceArray = [[NSMutableArray alloc] initWithCapacity:[[accountBalanceDataDictionary objectForKey:@"account_history"] count]];
        NSLog(@"%@",[accountBalanceDataDictionary objectForKey:@"account_history"]);
        for (NSDictionary *dictionary in [accountBalanceDataDictionary objectForKey:@"account_history"]) {
            AccountBalanceDetails *accountBalanceDetails = [[AccountBalanceDetails alloc] init];
            accountBalanceDetails.history_name = [dictionary objectForKey:@"history_name"];
            accountBalanceDetails.historyDetails = [dictionary objectForKey:@"historyDetails"];
            [balanceArray addObject:accountBalanceDetails];
        }
        self.balanceList = balanceArray;
        
        if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.myAccountTableView])) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (AccountBalanceDetails * balanceDetails in self.balanceList) {
                SectionInfoBalance *section = [[SectionInfoBalance alloc] init];
                section.balanceDetails = balanceDetails;
                section.open = NO;
                
                NSNumber *defaultHeight = [NSNumber numberWithInt:120];
                //NSInteger count = [[section.follower followerDetails] count];
                NSInteger count = 1;
                //NSLog(@"%d count...",count);
                
                for (NSInteger i= 0; i<count; i++) {
                    
                    NSLog(@"5.....%@",[section.balanceDetails history_name]);
                    [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                    NSLog(@"6......%@",section);
                }
                
                [array addObject:section];
            }
            //NSLog(@"%@",array);
            self.sectionInfoArray = array;
        }
        
        
        [self.myAccountTableView reloadData];
        
        CGRect frame = self.myAccountTableView.frame;
        frame.size.height = self.myAccountTableView.contentSize.height;
        self.myAccountTableView.frame = frame;
        
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.myAccountTableView.frame.origin.y + self.myAccountTableView.frame.size.height);
        
        [ApplicationDelegate removeProgressHUD];
    }
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect frame = self.myAccountTableView.frame;
    frame.size.height = self.myAccountTableView.contentSize.height;
    self.myAccountTableView.frame = frame;
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.myAccountTableView.frame.origin.y + self.myAccountTableView.frame.size.height);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
