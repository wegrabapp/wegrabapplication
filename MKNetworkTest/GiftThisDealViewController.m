//
//  GiftThisDealViewController.m
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "GiftThisDealViewController.h"
#import "GrabItInnerViewController.h"

@interface GiftThisDealViewController ()

@end

@implementation GiftThisDealViewController

- (void) viewWillAppear:(BOOL)animated{
    self.giftToTxtView.text = self.giftToTextData;
    self.giftFromTxtView.text = self.giftFromTextData;
    self.giftEmailTxtView.text = self.giftEmailTextData;
    self.giftMessageTxtView.text = self.giftMessageTextData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",self.dealId);
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cancelBtnActn:(UIButton *)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)grabItBtnActn:(UIButton *)sender {
    
    if (self.giftToTxtView.text.length == 0) {
        [ApplicationDelegate showAlertWithMessage:@"Please enter the name of the gift recipient" title:nil];
    } else if (self.giftFromTxtView.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your name" title:nil];
    }else if (self.giftEmailTxtView.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter recepients email address" title:nil];
    }else{
        //[self.view ]
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
        GrabItInnerViewController * grabitInnerVc = [[GrabItInnerViewController alloc]initWithNibName:@"GrabItInnerViewController" bundle:nil];
        grabitInnerVc.view.backgroundColor = [UIColor clearColor];
        //NSLog(@"%@",self.dealId);
        
        grabitInnerVc.grabItState = @"Gift";
        grabitInnerVc.grabItDealId = self.dealId;
        grabitInnerVc.giftTo = self.giftToTxtView.text;
        grabitInnerVc.giftFrom = self.giftFromTxtView.text;
        grabitInnerVc.giftEmail = self.giftEmailTxtView.text;
        grabitInnerVc.giftMessage = self.giftMessageTxtView.text;
        
        
        //NSLog(@"%@",self.rootNav);
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[grabitInnerVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:grabitInnerVc animated:NO];
            //[self.rootNav pushViewController:grabitInnerVc animated:NO];
        }
    }
}
@end
