//
//  MyCitiesEditTableViewCell.h
//  WeGrab
//
//  Created by satheesh on 2/2/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCitiesEditTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *countryCityNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *radioImageView;
@end
