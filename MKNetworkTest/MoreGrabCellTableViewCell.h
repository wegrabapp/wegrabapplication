//
//  MoreGrabCellTableViewCell.h
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreGrabCellTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *cellImage;
@property (weak, nonatomic) IBOutlet UILabel *placeLbl;
@property (weak, nonatomic) IBOutlet UILabel *cellHeading;
@property (weak, nonatomic) IBOutlet UILabel *cellDescription;
//@property (weak, nonatomic) IBOutlet UITextView *cellDescription;
@property (weak, nonatomic) IBOutlet UILabel *oldPrice;
@property (weak, nonatomic) IBOutlet UILabel *currentPrice;

@end
