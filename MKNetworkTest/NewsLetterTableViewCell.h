//
//  NewsLetterTableViewCell.h
//  WeGrab
//
//  Created by Ronish on 2/5/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

//@protocol SOTableViewCellActionDelegate <NSObject>
//
//@required
//-(void)tableViewCell:(UITableViewCell *)cell didFireActionForSender:(id)sender;
//
//@end

@interface NewsLetterTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *emailTickBtn;
@property (weak, nonatomic) IBOutlet UIButton *smsTickBtn;
//@property (nonatomic, weak) id<SOTableViewCellActionDelegate> delegate;
@end

