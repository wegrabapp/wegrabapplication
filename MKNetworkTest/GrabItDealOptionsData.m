//
//  GrabItDealOptionsData.m
//  WeGrab
//
//  Created by satheesh on 1/21/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "GrabItDealOptionsData.h"

@implementation GrabItDealOptionsData

@synthesize optionID;
@synthesize optionName;
@synthesize optionPrice;
@synthesize optionQuantity;

@end
