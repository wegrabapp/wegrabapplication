//
//  BuyGiftVoucherViewController.m
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "BuyGiftVoucherViewController.h"
#import "GiftVoucherCell.h"
#import "GiftVoucherDetails.h"
#import "BuyGiftVoucherDetailsViewController.h"

@interface BuyGiftVoucherViewController ()
@property (nonatomic, strong) NSArray * voucherList;
@property (nonatomic, strong) NSMutableArray * voucherPriceList;
@property (nonatomic, strong) NSMutableArray * voucherIDList;
@property (nonatomic, strong) NSMutableArray * voucherQuantityList;
@end

@implementation BuyGiftVoucherViewController

@synthesize giftVoucherTableView;

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Buy Gift Voucher";
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
//    self.giftVoucherTableView.allowsSelection = NO;
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getGiftVoucherData];
    }
    
    // Do any additional setup after loading the view from its nib.
}

-(void)getGiftVoucherData{
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/list_all_gift_vouchers"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.voucherList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    GiftVoucherCell *cell = (GiftVoucherCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GiftVoucherCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.voucherAmountLbl.text = [NSString stringWithFormat:@"$ %@",[[self.voucherList objectAtIndex:indexPath.row]objectForKey:@"Price"]];
    //cell.contentView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    return 45;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BuyGiftVoucherDetailsViewController * buyGiftVoucherDetailsViewObj = [[BuyGiftVoucherDetailsViewController alloc]initWithNibName:@"BuyGiftVoucherDetailsViewController" bundle:nil];
    buyGiftVoucherDetailsViewObj.voucherID = [[self.voucherList objectAtIndex:indexPath.row]objectForKey:@"Voucher Id"];
    buyGiftVoucherDetailsViewObj.voucherAmount = [[self.voucherList objectAtIndex:indexPath.row]objectForKey:@"Price"];
    buyGiftVoucherDetailsViewObj.voucherQuantity = [[self.voucherList objectAtIndex:indexPath.row]objectForKey:@"Quantity"];
    buyGiftVoucherDetailsViewObj.voucherAmountArray = self.voucherPriceList;
    buyGiftVoucherDetailsViewObj.voucherIDArray = self.voucherIDList;
    buyGiftVoucherDetailsViewObj.voucherQuantityArray = self.voucherQuantityList;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:buyGiftVoucherDetailsViewObj animated:NO completion:nil];
    
    //[self.navigationController pushViewController:giftViewObj animated:YES];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    giftVoucherDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    NSLog(@"%@",responseData);
    giftVoucherDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",giftVoucherDataDictionary);
    NSLog(@"%lu",[[giftVoucherDataDictionary objectForKey:@"GiftVoucher"] count]);
    
    ////////////////////////
    NSLog(@"%@",[giftVoucherDataDictionary objectForKey:@"Message"]);
//    self.introductionTextView.attributedText = [[NSAttributedString alloc] initWithData:[[giftVoucherDataDictionary objectForKey:@"Message"] dataUsingEncoding:NSUnicodeStringEncoding]options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
//    } documentAttributes:nil error:nil];
    
    self.introductionTextView.attributedText = [[NSAttributedString alloc] initWithData:[[giftVoucherDataDictionary objectForKey:@"Message"] dataUsingEncoding:NSUTF8StringEncoding]
                                     options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                               NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                          documentAttributes:nil error:nil];
    
    NSLog(@"%@",[[NSAttributedString alloc] initWithData:[[giftVoucherDataDictionary objectForKey:@"Message"] dataUsingEncoding:NSUTF8StringEncoding]
                                                 options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                           NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)}
                                      documentAttributes:nil error:nil]);
    ////////////////////////
    
    if ([[giftVoucherDataDictionary objectForKey:@"GiftVoucher"] count] > 0) {
        self.voucherPriceList = [[NSMutableArray alloc]init];
        self.voucherIDList = [[NSMutableArray alloc]init];
        self.voucherQuantityList = [[NSMutableArray alloc]init];
        NSMutableArray *priceArray = [[NSMutableArray alloc] initWithCapacity:[[giftVoucherDataDictionary objectForKey:@"GiftVoucher"] count]];
        NSMutableArray *idArray = [[NSMutableArray alloc] initWithCapacity:[[giftVoucherDataDictionary objectForKey:@"GiftVoucher"] count]];
        NSMutableArray *countArray = [[NSMutableArray alloc] initWithCapacity:[[giftVoucherDataDictionary objectForKey:@"GiftVoucher"] count]];
        for (NSDictionary *dictionary in [giftVoucherDataDictionary objectForKey:@"GiftVoucher"]) {
            [priceArray addObject:[dictionary objectForKey:@"Price"]];
            [idArray addObject:[dictionary objectForKey:@"Voucher Id"]];
            [countArray addObject:[dictionary objectForKey:@"Quantity"]];
        }
        self.voucherPriceList = priceArray;
        self.voucherIDList = idArray;
        self.voucherQuantityList = countArray;
        
        self.voucherList = [giftVoucherDataDictionary objectForKey:@"GiftVoucher"];
        [self.giftVoucherTableView reloadData];
        
        CGRect frame = self.giftVoucherTableView.frame;
        frame.size.height = self.giftVoucherTableView.contentSize.height;
        self.giftVoucherTableView.frame = frame;
        
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.giftVoucherTableView.frame.origin.y + self.giftVoucherTableView.frame.size.height);
        
        [ApplicationDelegate removeProgressHUD];
    }
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
