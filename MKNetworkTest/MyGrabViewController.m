//
//  MyGrabViewController.m
//  WeGrab
//
//  Created by Ronish on 2/11/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "MyGrabViewController.h"
#import "MyGrabDetails.h"
#import "MyGrabTableViewCell.h"

@interface MyGrabViewController ()
@property (nonatomic, strong) NSArray * myGrabListList;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
//@property (nonatomic, strong) NSMutableArray *countryNameArray;
//@property (nonatomic, strong) NSMutableArray *citySelectionListArray;
//@property (nonatomic, strong) NSString *addedCountryId;
//@property (nonatomic, strong) NSString *addedCityId;
//@property (nonatomic, strong) NSString * tickStatus;
@end

@implementation MyGrabViewController
@synthesize myGrabListList;
@synthesize sectionInfoArray;
//@synthesize countryNameArray;

-(void)viewWillAppear:(BOOL)animated{
    [self getMyGrabData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.myGrabTableView.sectionHeaderHeight = 45;
    self.myGrabTableView.sectionFooterHeight = 0;
    // Do any additional setup after loading the view from its nib.
}

-(void)getMyGrabData{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/mygrabs"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

#pragma mark -
#pragma mark TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[arrayForBool objectAtIndex:section] boolValue]) {
        return 1;
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyGrabDetails *myGrabDetails = [self.myGrabListList objectAtIndex:indexPath.section];
    static NSString *CellIdentifier = @"Cell";
    MyGrabTableViewCell *cell = (MyGrabTableViewCell *)[self.myGrabTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyGrabTableViewCell" owner:self options:nil];
        if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Cities"]]) {
            if ([[myGrabDetails.deals objectForKey:@"Cities"] length] == 0) {
                cell = [nib objectAtIndex:1];
            }
            else{
                cell = [nib objectAtIndex:0];
            }
        } else {
            cell = [nib objectAtIndex:0];
        }
    }
    
    /*  dont delete....
//    cell.dealDescriptionValueLbl.attributedText = [[NSAttributedString alloc] initWithData:[[myGrabDetails.more_Details objectForKey:@"Deal Description"] dataUsingEncoding:NSUnicodeStringEncoding]options:@{     NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType
//                                                                                  } documentAttributes:nil error:nil];
     
     */
    
    
    if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Order Deal Description"]]) {
        cell.dealDescriptionValueLbl.text = [myGrabDetails.deals objectForKey:@"Order Deal Description"];
    }
    
//   cell.dealDescriptionValueLbl.textColor = [UIColor lightGrayColor];
    if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Cities"]]) {
        cell.cityValueLbl.text = [myGrabDetails.deals objectForKey:@"Cities"];
        if ([[myGrabDetails.deals objectForKey:@"Cities"] length] == 0) {
            [cell.cityLbl removeFromSuperview];
            [cell.cityValueLbl removeFromSuperview];
            CGRect frame;
            frame = cell.purchaseDateLbl.frame;
            NSLog(@"%f",frame.origin.y);
            frame.origin.y = frame.origin.y - 20;
            NSLog(@"%f",frame.origin.y);
            cell.purchaseDateLbl.frame = frame;
            cell.voucherBackView.frame = CGRectMake(0, 0, 100, 100);
        }
    }
    else{
        
    }
    if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Purchase Date"]]) {
        cell.purchaseDateValueLbl.text = [myGrabDetails.deals objectForKey:@"Purchase Date"];
    }
    if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Expiry Date"]]) {
        cell.expiryDateValueLbl.text = [myGrabDetails.deals objectForKey:@"Expiry Date"];
    }
    if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Price"]]) {
        cell.priceValueLbl.text = [myGrabDetails.deals objectForKey:@"Price"];
    }
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [sectionTitleArray count];
    return [self.myGrabListList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*
    MyGrabDetails *myGrab = (MyGrabDetails *)[self.myGrabListList objectAtIndex:indexPath.section];

    self.tickStatus = @"YES";
    self.addedCountryId = myGrab.country_id;
    if (myGrab.cities.count == indexPath.row) {
        
    } else {
        if ([[self.citySelectionListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [self.citySelectionListArray setObject:@"0" atIndexedSubscript:indexPath.row];
        } else {
            [self.citySelectionListArray setObject:@"1" atIndexedSubscript:indexPath.row];
        }
        [self.myGrabTableView reloadData];
    }
    
    //    [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
    //
    //    [self.myGrabTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    */
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyGrabDetails *myGrabDetails = [self.myGrabListList objectAtIndex:indexPath.section];
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        if ([ApplicationDelegate isValid:[myGrabDetails.deals objectForKey:@"Cities"]]) {
            if ([[myGrabDetails.deals objectForKey:@"Cities"] length] == 0) {
                return 144;
            }
            return 178;
        }
        else{
            return 144;
        }
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    MyGrabDetails *country = (MyGrabDetails *)[self.myGrabListList objectAtIndex:section];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.myGrabTableView.frame.size.width,40)];
    sectionView.tag=section;
    
    UIImageView * backGroundView = [[UIImageView alloc]init];
    backGroundView.frame = sectionView.bounds;
    backGroundView.backgroundColor = [UIColor darkGrayColor];
    [sectionView addSubview:backGroundView];
    UIImageView * seperatorView = [[UIImageView alloc]init];
    seperatorView.frame = CGRectMake(backGroundView.frame.origin.x, backGroundView.frame.size.height-1, backGroundView.frame.size.width,1);
    seperatorView.backgroundColor = [UIColor lightGrayColor];
    [sectionView addSubview:seperatorView];
    
    CGRect LabelFrame = sectionView.bounds;
    LabelFrame.size.width -= 30;
    LabelFrame.origin.x += 10;
    CGRectInset(LabelFrame, 0.0, 5.0);
    
    UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame];
    label.text = [NSString stringWithFormat:@"%@",country.grab_Type];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentLeft;
    [sectionView addSubview:label];
    //self.sectionTitle = label;
    
    CGRect buttonFrame = CGRectMake(LabelFrame.size.width, 0, 20, LabelFrame.size.height);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = buttonFrame;
    [button setImage:[UIImage imageNamed:@"rightarrow1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"downarrow1.png"] forState:UIControlStateSelected];
    BOOL selectionStatus  = [[arrayForBool objectAtIndex:section] boolValue];
    if (selectionStatus) {
        button.selected = YES;
    }
    else{
        button.selected = NO;
    }
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button addTarget:self action:@selector(sectionHeaderTapped:) forControlEvents:UIControlEventTouchUpInside];
    //button.selected = YES;
    [sectionView addSubview:button];
    //self.discButton = button;
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    return  sectionView;
}


#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    //self.citySelectionListArray = [[NSMutableArray alloc]init];
    //self.tickStatus = @"NO";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    NSLog(@"%@",indexPath);
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[self.myGrabListList count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            else{
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
            }
        }
        [self.myGrabTableView reloadData];
        [self.myGrabTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    if (connection == connectionAddMyCity) {
        responseDataAddCity = [[NSMutableData alloc] init];
        addCityDataDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseData = [[NSMutableData alloc] init];
        myGrabDataDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    if (connection == connectionAddMyCity) {
        [responseDataAddCity appendData:data];
    } else {
        [responseData appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (connection == connectionAddMyCity) {
//        NSError* error;
//        addCityDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataAddCity options:kNilOptions error:&error];
//        NSLog(@"%@",addCityDataDictionary);
//        
//        [ApplicationDelegate showAlertWithMessage:[addCityDataDictionary objectForKey:@"message"] title:nil];
//        
//        [ApplicationDelegate removeProgressHUD];
    } else {
        NSError* error;
        [myGrabDataDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error]objectForKey:@"Mygrab_Information"] forKey:@"Mygrab_Information"];
        NSLog(@"%@",myGrabDataDictionary);
        
        if ([[myGrabDataDictionary objectForKey:@"Mygrab_Information"] count] > 0) {
            
            NSMutableArray *myGrabArray = [[NSMutableArray alloc] initWithCapacity:[[myGrabDataDictionary objectForKey:@"Mygrab_Information"] count]];
            for (NSDictionary *dictionary in [myGrabDataDictionary objectForKey:@"Mygrab_Information"]) {
                MyGrabDetails *myGrabDetails = [[MyGrabDetails alloc] init];
                myGrabDetails.grab_Type = [dictionary objectForKey:@"Grab Type"];
                myGrabDetails.deals = [dictionary objectForKey:@"Deals"];
                myGrabDetails.more_Details = [dictionary objectForKey:@"More_Details"];
                [myGrabArray addObject:myGrabDetails];
            }
            self.myGrabListList = myGrabArray;
            
            if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.myGrabTableView])) {
                NSMutableArray *array = [[NSMutableArray alloc] init];
                arrayForBool=[[NSMutableArray alloc]init];
                //self.countryNameArray = [[NSMutableArray alloc]init];
                for (MyGrabDetails *country in self.myGrabListList) {
                    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                    [array addObject:country];
                }
                self.sectionInfoArray = array;
            }
            [self.myGrabTableView reloadData];
            [ApplicationDelegate removeProgressHUD];
        }
    }
    [ApplicationDelegate removeProgressHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
