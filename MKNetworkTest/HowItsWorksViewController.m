//
//  HowItsWorksViewController.m
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "HowItsWorksViewController.h"

@interface HowItsWorksViewController ()

@end

@implementation HowItsWorksViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = YES;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"How It Works";
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
