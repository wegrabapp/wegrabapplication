//
//  SectionViewMoreGrab.m
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "SectionViewMoreGrab.h"
#import <QuartzCore/QuartzCore.h>

@implementation SectionViewMoreGrab

@synthesize section;
@synthesize sectionTitle;
@synthesize discButton;
@synthesize delegate;

+ (Class)layerClass {
    
    return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame WithTitle: (NSString *) title Section:(NSInteger)sectionNumber delegate: (id <SectionViewMoreGrab>) Delegate
{
    self = [super initWithFrame:frame];
    if (self) {
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(discButtonPressed:)];
        [self addGestureRecognizer:tapGesture];
        
        self.userInteractionEnabled = YES;
        
        self.section = sectionNumber;
        self.delegate = Delegate;
        
        CGRect LabelFrame = self.bounds;
        LabelFrame.size.width -= 30;
        LabelFrame.origin.x += 10;
        CGRectInset(LabelFrame, 0.0, 5.0);
        
        UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame];
        label.text = title;
        label.font = [UIFont boldSystemFontOfSize:16.0];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor blackColor];
        label.textAlignment = UITextAlignmentLeft;
        [self addSubview:label];
        self.sectionTitle = label;
        
//        CGRect buttonFrame = CGRectMake(LabelFrame.size.width, 0, 20, LabelFrame.size.height);
//        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
//        button.frame = buttonFrame;
//        [button setImage:[UIImage imageNamed:@"rightarrow1.png"] forState:UIControlStateNormal];
//        [button setImage:[UIImage imageNamed:@"downarrow1.png"] forState:UIControlStateSelected];
//        button.imageView.contentMode = UIViewContentModeScaleAspectFit;
//        [button addTarget:self action:@selector(discButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
        //[self addSubview:button];
        //self.discButton = button;
        
        static NSMutableArray *colors = nil;
        if (colors == nil) {
            colors = [[NSMutableArray alloc] initWithCapacity:3];
            UIColor *color = nil;
            //            color = [UIColor colorWithRed:0.61 green:0.74 blue:0.78 alpha:1];
            //            [colors addObject:(id)[color CGColor]];
            //            color = [UIColor colorWithRed:0.50 green:0.54 blue:0.58 alpha:1];
            //            [colors addObject:(id)[color CGColor]];
            //            color = [UIColor colorWithRed:0.15 green:0.20 blue:0.23 alpha:1];
            //            [colors addObject:(id)[color CGColor]];
            
            color = [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
            [colors addObject:(id)[color CGColor]];
        }
        [(CAGradientLayer *)self.layer setColors:colors];
        [(CAGradientLayer *)self.layer setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    }
    return self;
}

- (void) discButtonPressed : (id) sender
{
    [self toggleButtonPressed:TRUE];
}

- (void) toggleButtonPressed : (BOOL) flag
{
    self.discButton.selected = !self.discButton.selected;
    if(flag)
    {
        if (self.discButton.selected)
        {
            if ([self.delegate respondsToSelector:@selector(sectionOpened:)])
            {
                [self.delegate sectionOpened:self.section];
            }
        } else
        {
            if ([self.delegate respondsToSelector:@selector(sectionClosed:)])
            {
                [self.delegate sectionClosed:self.section];
            }
        }
    }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
