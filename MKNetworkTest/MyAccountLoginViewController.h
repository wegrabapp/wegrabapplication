//
//  MyAccountLoginViewController.h
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountLoginViewController : UIViewController<UIAlertViewDelegate>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
}
- (IBAction)logOutBtnActn:(UIButton *)sender;
- (IBAction)profileBtnActn:(UIButton *)sender;
- (IBAction)myCitiesBtnActn:(UIButton *)sender;
- (IBAction)MyAccountBalanceBtnActn:(UIButton *)sender;
- (IBAction)buyGiftVoucherBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScroll;
- (IBAction)newsLetterBtnActn:(UIButton *)sender;
- (IBAction)billingBtnActn:(UIButton *)sender;
- (IBAction)myGrabBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *signoutView;
@end
