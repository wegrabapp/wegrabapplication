//
//  NewsLetterViewController.m
//  WeGrab
//
//  Created by Ronish on 2/5/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "NewsLetterViewController.h"
#import "NewsLetterTableViewCell.h"
#import "SectionInfo.h"
#import "SectionInfoCities.h"
#import "NewsLetterCityDetails.h"

@interface NewsLetterViewController ()

@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * cityList;

@end

@implementation NewsLetterViewController

@synthesize openSectionIndex;
@synthesize sectionInfoArray;
@synthesize cityList;

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].title = @"Newsletter";
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self loadNewsLetterDataFromServer];
    }
    
    self.newsLetterCitiesTableview.sectionHeaderHeight = 45;
    self.newsLetterCitiesTableview.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadNewsLetterDataFromServer{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/my_cities"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionGetInitialData = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.cityList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SectionInfoCities *array = [self.sectionInfoArray objectAtIndex:section];
    
    NSInteger rows = 1;
    NSLog(@"row count...%ld",(long)rows);
    return (array.open) ? rows : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    NewsLetterTableViewCell *cell = (NewsLetterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"NewsLetterTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    //UIButton *button0 = cell.emailTickBtn;
    //button0.tag = 0;
    //[button0 addTarget:cell action:@selector(fireAction:)
     // forControlEvents:UIControlEventTouchUpInside];
    //[cell.contentView addSubview:button0];
    
    [cell.emailTickBtn addTarget:self action:@selector(EmailBtnActn:) forControlEvents:UIControlEventTouchUpInside];
    [cell.smsTickBtn addTarget:self action:@selector(SmsBtnActn:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.emailTickBtn.tag = indexPath.section;
    cell.smsTickBtn.tag = indexPath.section;
    if ([[self.newsLetterEmailArray objectAtIndex:indexPath.section]isEqualToString:@"1"]) {
        cell.emailTickBtn.selected = YES;
    } else {
        cell.emailTickBtn.selected = NO;
    }
    if ([[self.newsLetterSmsArray objectAtIndex:indexPath.section]isEqualToString:@"1"]) {
        cell.smsTickBtn.selected = YES;
    } else {
        cell.smsTickBtn.selected = NO;
    }
    
    
    return cell;
}



-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
    //NSLog(@"%f",[[array objectInRowHeightsAtIndex:indexPath.row] floatValue]);
    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfoCities *array  = [self.sectionInfoArray objectAtIndex:section];
    NSLog(@"%@",array.newslettercitydetails);
    if (!array.sectionView)
    {
        NSString *title = [NSString stringWithFormat:@"%@ - %@",array.newslettercitydetails.city_Name,array.newslettercitydetails.country_Name];
        NSLog(@"%@",title);
        array.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, self.newsLetterCitiesTableview.bounds.size.width, 45) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionView;
}
#pragma mark - Table view delegate


-(void)EmailBtnActn: (UIButton *)btn{
    if ([[self.newsLetterEmailArray objectAtIndex:btn.tag]isEqualToString:@"0"]) {
        [self.newsLetterEmailArray setObject:@"1" atIndexedSubscript:btn.tag];
    } else {
        [self.newsLetterEmailArray setObject:@"0" atIndexedSubscript:btn.tag];
    }
    [self.newsLetterCitiesTableview reloadData];
}

-(void)SmsBtnActn: (UIButton *)btn{
    if ([[self.newsLetterSmsArray objectAtIndex:btn.tag]isEqualToString:@"0"]) {
        [self.newsLetterSmsArray setObject:@"1" atIndexedSubscript:btn.tag];
    } else {
        [self.newsLetterSmsArray setObject:@"0" atIndexedSubscript:btn.tag];
    }
    [self.newsLetterCitiesTableview reloadData];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (int i =0; i< self.newsLetterEmailArray.count; i++) {
        if (indexPath.section == i) {
            [self.newsLetterEmailArray setObject:@"1" atIndexedSubscript:i];
        } else {
            [self.newsLetterEmailArray setObject:@"0" atIndexedSubscript:i];
        }
    }
    [self.newsLetterCitiesTableview reloadData];
}


- (void) sectionClosed : (NSInteger) section{
    
    SectionInfoCities *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.newsLetterCitiesTableview numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.newsLetterCitiesTableview deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
    
}

- (void) sectionOpened : (NSInteger) section
{
    SectionInfoCities *array = [self.sectionInfoArray objectAtIndex:section];
    
    array.open = YES;
    NSInteger count = 1;
    NSLog(@"11.....%d",count);
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    if (previousOpenIndex != NSNotFound)
    {
        SectionInfoCities *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        sectionArray.open = NO;
        NSInteger counts = 1;
        [sectionArray.sectionView toggleButtonPressed:FALSE];
        for (NSInteger i = 0; i<counts; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.newsLetterCitiesTableview beginUpdates];
    [self.newsLetterCitiesTableview insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
    [self.newsLetterCitiesTableview deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.newsLetterCitiesTableview endUpdates];
    self.openSectionIndex = section;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionGetInitialData) {
        responseData = [[NSMutableData alloc] init];
        newsLetterInitialDataDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseDataSubscribe = [[NSMutableData alloc] init];
        subscribeDataDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == connectionGetInitialData) {
        [responseData appendData:data];
    } else {
        [responseDataSubscribe appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (connection == connectionGetInitialData) {
        NSError* error;
        
        newsLetterInitialDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSLog(@"%@",newsLetterInitialDataDictionary);
        
        NSLog(@"%u",[[newsLetterInitialDataDictionary objectForKey:@"City_Information"]count]);
        
        self.newsLetterEmailArray = [[NSMutableArray alloc] initWithCapacity:[[newsLetterInitialDataDictionary objectForKey:@"City_Information"] count]];
        
        self.newsLetterSmsArray = [[NSMutableArray alloc] initWithCapacity:[[newsLetterInitialDataDictionary objectForKey:@"City_Information"] count]];
        
        if ([ApplicationDelegate isValid:[newsLetterInitialDataDictionary objectForKey:@"Email"]]) {
            self.emailLabel.text = [newsLetterInitialDataDictionary objectForKey:@"Email"];
        }
        
        if ([ApplicationDelegate isValid:[newsLetterInitialDataDictionary objectForKey:@"Mobile_No"]]) {
            self.mobileNumberLabel.text = [newsLetterInitialDataDictionary objectForKey:@"Mobile_No"];
        }
        
        if ([[newsLetterInitialDataDictionary objectForKey:@"City_Information"] count] > 0) {
            
            NSMutableArray *citiesArray = [[NSMutableArray alloc] initWithCapacity:[[[newsLetterInitialDataDictionary objectForKey:@"User_Cities"] objectForKey:@"City_Information"] count]];
            NSLog(@"%@",[newsLetterInitialDataDictionary objectForKey:@"City_Information"]);
            
            for (NSDictionary *dictionary in [newsLetterInitialDataDictionary objectForKey:@"City_Information"]) {
                NewsLetterCityDetails *newsLetterCityDetails = [[NewsLetterCityDetails alloc] init];
                newsLetterCityDetails.city_id = [dictionary objectForKey:@"City_id"];
                newsLetterCityDetails.city_Name = [dictionary objectForKey:@"City_Name"];
                newsLetterCityDetails.country_id = [dictionary objectForKey:@"Country_id"];
                newsLetterCityDetails.country_Name = [dictionary objectForKey:@"Country_Name"];
                newsLetterCityDetails.email_notification = [dictionary objectForKey:@"email_noti"];
                newsLetterCityDetails.sms_notificaton = [dictionary objectForKey:@"sms"];
                if ([ApplicationDelegate isValid:[dictionary objectForKey:@"email_noti"]]) {
                    [self.newsLetterEmailArray addObject:[dictionary objectForKey:@"email_noti"]];
                } else {
                    [self.newsLetterEmailArray addObject:@"0"];
                }
                if ([ApplicationDelegate isValid:[dictionary objectForKey:@"sms"]]) {
                    [self.newsLetterSmsArray addObject:[dictionary objectForKey:@"sms"]];
                } else {
                    [self.newsLetterSmsArray addObject:@"0"];
                }
                [citiesArray addObject:newsLetterCityDetails];
                //NSLog(@"%@",dictionary);
            }
            self.cityList = citiesArray;
            //        NSLog(@"%@",self.cityList);
            NSLog(@"%@",self.newsLetterEmailArray);
            NSLog(@"%@",self.newsLetterSmsArray);
            
            //        NewsLetterCityDetails *newsLetterCityDetails1 = [[NewsLetterCityDetails alloc] init];
            //        newsLetterCityDetails1 = [self.cityList objectAtIndex:1];
            //        NSLog(@"%@",newsLetterCityDetails1.city_Name);
            
            if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.newsLetterCitiesTableview])) {
                NSMutableArray *array = [[NSMutableArray alloc] init];
                
                for (NewsLetterCityDetails * newslettercitydetails in self.cityList) {
                    SectionInfoCities *section = [[SectionInfoCities alloc] init];
                    section.newslettercitydetails = newslettercitydetails;
                    section.open = NO;
                    
                    NSNumber *defaultHeight = [NSNumber numberWithInt:44];
                    //NSInteger count = [[section.follower followerDetails] count];
                    NSInteger count = 1;
                    //NSLog(@"%d count...",count);
                    
                    
                    for (NSInteger i= 0; i<count; i++) {
                        
                        //NSLog(@"5.....%@",[section.citydetails cityName]);
                        [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                        //NSLog(@"6......%@",section);
                    }
                    
                    
                    [array addObject:section];
                }
                NSLog(@"%@",array);
                self.sectionInfoArray = array;
            }
            
            [self.newsLetterCitiesTableview reloadData];
            
            //        CGRect frame = self.newsLetterCitiesTableview.frame;
            //        frame.size.height = self.newsLetterCitiesTableview.contentSize.height;
            //        self.newsLetterCitiesTableview.frame = frame;
            
            //self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.newsLetterCitiesTableview.frame.origin.y + self.newsLetterCitiesTableview.frame.size.height);
            
            [ApplicationDelegate removeProgressHUD];
        }
    } else {
        NSError* error;
        //NSLog(@"%@",responseDataSubscribe);
        subscribeDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataSubscribe options:kNilOptions error:&error];
        NSLog(@"%@",subscribeDataDictionary);
        [ApplicationDelegate showAlertWithMessage:[subscribeDataDictionary objectForKey:@"message"] title:nil];
        [self loadNewsLetterDataFromServer];
        [ApplicationDelegate removeProgressHUD];
    }
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}


- (IBAction)subscribeBtnActn:(UIButton *)sender {
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/subscribe_news_letter"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSMutableDictionary * Subcription_Information = [[NSMutableDictionary alloc]init];
        NSLog(@"%lu",(unsigned long)self.cityList.count);
        NSLog(@"%lu",(unsigned long)self.newsLetterEmailArray.count);
        NSLog(@"%lu",(unsigned long)self.newsLetterSmsArray.count);
//        for (NewsLetterCityDetails * newslettercitydetails in self.cityList) {
//            NSMutableDictionary * subscriptionDetails = [[NSMutableDictionary alloc]init];
//            [subscriptionDetails setObject:newslettercitydetails.city_id forKey:@"City_id"];
//        }
        NSMutableArray * subscriptionDetailsArray = [[NSMutableArray alloc]init];
        for (int i=0; i < self.cityList.count; i++) {
            NewsLetterCityDetails * newslettercitydetails = [self.cityList objectAtIndex:i];
            NSMutableDictionary * subscriptionDetails = [[NSMutableDictionary alloc]init];
            [subscriptionDetails setObject:newslettercitydetails.city_id forKey:@"City_id"];
            [subscriptionDetails setObject:[self.newsLetterEmailArray objectAtIndex:i] forKey:@"email_noti"];
            [subscriptionDetails setObject:[self.newsLetterSmsArray objectAtIndex:i] forKey:@"sms"];
            [subscriptionDetailsArray addObject:subscriptionDetails];
        }
        
        //NSLog(@"%@",Subcription_Information);
        
//        [Subcription_Information setObject:subscriptionDetailsArray forKey:@"Subcription_Information"];
//        [Subcription_Information setObject:[userLoginStatus objectForKey:@"UserId"] forKey:@"user_id"];
        
        [Subcription_Information setObject:subscriptionDetailsArray forKey:@"Subcription_Information"];
        [Subcription_Information setObject:@"3" forKey:@"user_id"];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     Subcription_Information,@"User_NewLetter_Subcription",
                                     nil];
        NSLog(@"%@",requestData);
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionPostSubscription = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}
@end
