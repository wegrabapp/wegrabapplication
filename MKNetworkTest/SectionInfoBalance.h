//
//  SectionInfoBalance.h
//  WeGrab
//
//  Created by satheesh on 1/27/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SectionView;
@class CategoryName;
@class CountryDetails;
@class FollowerDetails;
@class AccountBalanceDetails;

@interface SectionInfoBalance : NSObject

@property (assign) BOOL open;
@property (strong) CategoryName *category;
@property (strong) CountryDetails *country;
@property (strong) FollowerDetails *follower;
@property (strong) AccountBalanceDetails *balanceDetails;
@property (strong) SectionView *sectionView;
@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;

@end
