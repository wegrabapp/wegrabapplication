//
//  MyAccountLoginViewController.m
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MyAccountLoginViewController.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "MyAccountBalanceViewController.h"
#import "BuyGiftVoucherViewController.h"
#import "MyCitiesViewController.h"
#import "NewsLetterViewController.h"
#import "BillingViewController.h"
#import "MyGrabViewController.h"

@interface MyAccountLoginViewController ()

@end

@implementation MyAccountLoginViewController

-(void)viewWillAppear:(BOOL)animated{
    [HomeViewController sharedViewController].backBtn.hidden = YES;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"My Account";
    
    if ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"]) {
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mainScroll.contentSize = CGSizeMake(self.mainScroll.frame.size.width, self.signoutView.frame.origin.y + self.signoutView.frame.size.height + 5);
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logOutBtnActn:(UIButton *)sender {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                    message:@"Do you want to log out?"
                                                   delegate:self
                                          cancelButtonTitle:@"Cancel"
                                          otherButtonTitles:@"ok", nil];
    
    [alert show];
}

- (IBAction)profileBtnActn:(UIButton *)sender {
    ProfileViewController *profileViewObj = [[ProfileViewController alloc] initWithNibName:@"ProfileViewController" bundle:nil];
    profileViewObj.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[profileViewObj class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:profileViewObj animated:NO];
    }
}

- (IBAction)myCitiesBtnActn:(UIButton *)sender {
    MyCitiesViewController *mycitiesVc = [[MyCitiesViewController alloc] initWithNibName:@"MyCitiesViewController" bundle:nil];
    mycitiesVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[mycitiesVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:mycitiesVc animated:NO];
    }
}

- (IBAction)MyAccountBalanceBtnActn:(UIButton *)sender {
    
    MyAccountBalanceViewController *myAccountBalanceVc = [[MyAccountBalanceViewController alloc] initWithNibName:@"MyAccountBalanceViewController" bundle:nil];
    myAccountBalanceVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myAccountBalanceVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:myAccountBalanceVc animated:NO];
    }
}

- (IBAction)buyGiftVoucherBtnActn:(UIButton *)sender {
    BuyGiftVoucherViewController *buyGiftVoucherVc = [[BuyGiftVoucherViewController alloc] initWithNibName:@"BuyGiftVoucherViewController" bundle:nil];
    buyGiftVoucherVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[buyGiftVoucherVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:buyGiftVoucherVc animated:NO];
    }
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == [alertView cancelButtonIndex]){
        //cancel clicked ...do your action
        //[self.moreMenuTable reloadData];
    }else{
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        [userLoginStatus setObject:@"LOGOUT" forKey:@"STATUS"];
        [userLoginStatus synchronize];
        if (([userLoginStatus objectForKey:@"STATUS"] == nil) || ([[userLoginStatus objectForKey:@"STATUS"]isEqual:[NSNull null]]) || ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"])) {
            [[HomeViewController sharedViewController].moreGrabsBtn setImage:[UIImage imageNamed:@"moregrabs.png"] forState:UIControlStateNormal];
            [[HomeViewController sharedViewController].moreGrabsBtn setImage:[UIImage imageNamed:@"moregrabs_sel.png"] forState:UIControlStateSelected];
            [[HomeViewController sharedViewController].howItWorksBtn setImage:[UIImage imageNamed:@"howitworks.png"] forState:UIControlStateNormal];
            [[HomeViewController sharedViewController].howItWorksBtn setImage:[UIImage imageNamed:@"howitworks_sel.png"] forState:UIControlStateSelected];
        } else {
            [[HomeViewController sharedViewController].moreGrabsBtn setImage:[UIImage imageNamed:@"myothergrabs.png"] forState:UIControlStateNormal];
            [[HomeViewController sharedViewController].moreGrabsBtn setImage:[UIImage imageNamed:@"myothergrabs_sel.png"] forState:UIControlStateSelected];
            [[HomeViewController sharedViewController].howItWorksBtn setImage:[UIImage imageNamed:@"followers.png"] forState:UIControlStateNormal];
            [[HomeViewController sharedViewController].howItWorksBtn setImage:[UIImage imageNamed:@"followers_sel.png"] forState:UIControlStateSelected];
            
        }
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
    }
}
- (IBAction)newsLetterBtnActn:(UIButton *)sender {
    NewsLetterViewController *newsLetterVc = [[NewsLetterViewController alloc] initWithNibName:@"NewsLetterViewController" bundle:nil];
    newsLetterVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[newsLetterVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:newsLetterVc animated:NO];
    }
}

- (IBAction)billingBtnActn:(UIButton *)sender {
    BillingViewController *billingVc = [[BillingViewController alloc] initWithNibName:@"BillingViewController" bundle:nil];
    billingVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[billingVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:billingVc animated:NO];
    }
}

- (IBAction)myGrabBtnActn:(UIButton *)sender {
    MyGrabViewController *myGrabVc = [[MyGrabViewController alloc] initWithNibName:@"MyGrabViewController" bundle:nil];
    myGrabVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myGrabVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:myGrabVc animated:NO];
    }
}
@end
