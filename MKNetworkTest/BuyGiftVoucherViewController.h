//
//  BuyGiftVoucherViewController.h
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyGiftVoucherViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    //NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * giftVoucherDataDictionary;
}
@property (weak, nonatomic) IBOutlet UITableView *giftVoucherTableView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITextView *introductionTextView;


@end
