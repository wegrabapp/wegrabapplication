//
//  NewsLetterViewController.h
//  WeGrab
//
//  Created by Ronish on 2/5/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SectionView.h"

@interface NewsLetterViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SectionView,NSURLConnectionDelegate>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableData * responseDataSubscribe;
    
    NSURLConnection * connectionGetInitialData;
    NSURLConnection * connectionPostSubscription;
    NSMutableDictionary * newsLetterInitialDataDictionary;
    NSMutableDictionary * subscribeDataDictionary;
}
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) NSMutableArray * newsLetterEmailArray;
@property (strong, nonatomic) NSMutableArray * newsLetterSmsArray;
@property (weak, nonatomic) IBOutlet UITableView *newsLetterCitiesTableview;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
- (IBAction)subscribeBtnActn:(UIButton *)sender;

@end
