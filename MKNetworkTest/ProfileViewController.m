//
//  ProfileViewController.m
//  WeGrab
//
//  Created by satheesh on 1/19/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@end

@implementation ProfileViewController

-(void)viewWillAppear:(BOOL)animated{
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Profile";
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        NSLog(@"%@",[userLoginStatus objectForKey:@"UserId"]);
        [self fetchDatafromServer];
    }
    [userLoginStatus synchronize];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.calendarBackView.layer.cornerRadius = 4;
    
    self.birthDatePicker.maximumDate=[NSDate date];
    [self.birthDatePicker setMaximumDate:[NSDate date]];
    
    
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.lastLoginBackView.frame.origin.y + self.lastLoginBackView.frame.size.height + 5);
    
    static NSMutableArray *colors = nil;
    if (colors == nil) {
        colors = [[NSMutableArray alloc] initWithCapacity:3];
        UIColor *color = nil;
        color = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
        [colors addObject:(id)[color CGColor]];
        color = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
        [colors addObject:(id)[color CGColor]];
        color = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1];
        [colors addObject:(id)[color CGColor]];
    }
    CAGradientLayer *gradientName = [CAGradientLayer layer];
    CGRect frame = self.nameBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientName.frame = frame;
    //gradientName.frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    //CGFloat screenHeight = screenRect.size.height;
    NSLog(@"%f",gradientName.frame.size.width);
    NSLog(@"%f",self.view.frame.size.width);
    NSLog(@"%f",screenWidth);
    [gradientName setColors:colors];
    [gradientName setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.nameBackgroundIMG.layer addSublayer:gradientName];
    
    CAGradientLayer *gradientPassword = [CAGradientLayer layer];
    frame = self.passwordBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientPassword.frame = frame;
    [gradientPassword setColors:colors];
    [gradientPassword setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.passwordBackgroundIMG.layer addSublayer:gradientPassword];
    
    CAGradientLayer *gradientEmail = [CAGradientLayer layer];
    frame = self.emailBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientEmail.frame = frame;
    [gradientEmail setColors:colors];
    [gradientEmail setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.emailBackgroundIMG.layer addSublayer:gradientEmail];
    
    CAGradientLayer *gradientTel = [CAGradientLayer layer];
    frame = self.telBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientTel.frame = frame;
    [gradientTel setColors:colors];
    [gradientTel setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.telBackgroundIMG.layer addSublayer:gradientTel];
    
    CAGradientLayer *gradientBirthDate = [CAGradientLayer layer];
    frame = self.birthDateBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientBirthDate.frame = frame;
    [gradientBirthDate setColors:colors];
    [gradientBirthDate setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.birthDateBackgroundIMG.layer addSublayer:gradientBirthDate];
    
    CAGradientLayer *gradientGender = [CAGradientLayer layer];
    frame = self.genderBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientGender.frame = frame;
    [gradientGender setColors:colors];
    [gradientGender setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.genderBackgroundIMG.layer addSublayer:gradientGender];
    
    CAGradientLayer *gradientMember = [CAGradientLayer layer];
    frame = self.memberBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientMember.frame = frame;
    [gradientMember setColors:colors];
    [gradientMember setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.memberBackgroundIMG.layer addSublayer:gradientMember];
    
    CAGradientLayer *gradientLastLogin = [CAGradientLayer layer];
    frame = self.memberBackgroundIMG.bounds;
    frame.size.width = [[UIScreen mainScreen] bounds].size.width;
    gradientLastLogin.frame = frame;
    [gradientLastLogin setColors:colors];
    [gradientLastLogin setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
    [self.lastLoginBackgroundIMG.layer addSublayer:gradientLastLogin];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)tableSwapInOutBtnActn:(UIButton *)sender {
    
    [self.passwordArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    [self.emailArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    [self.telArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    [self.birthDateArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    [self.genderArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    
    self.passwordBackView.frame = CGRectMake(self.passwordBackView.frame.origin.x, self.passwordBackView.frame.origin.y, self.passwordBackView.frame.size.width, 45);
    self.emailBackView.frame = CGRectMake(self.emailBackView.frame.origin.x, (self.passwordBackView.frame.origin.y + self.passwordBackView.frame.size.height), self.emailBackView.frame.size.width, 45);
    self.telBackView.frame = CGRectMake(self.telBackView.frame.origin.x, (self.emailBackView.frame.origin.y + self.emailBackView.frame.size.height), self.telBackView.frame.size.width, 45);
    self.birthDateBackView.frame = CGRectMake(self.birthDateBackView.frame.origin.x, (self.telBackView.frame.origin.y + self.telBackView.frame.size.height), self.birthDateBackView.frame.size.width, 45);
    self.genderBackView.frame = CGRectMake(self.genderBackView.frame.origin.x, (self.birthDateBackView.frame.origin.y + self.birthDateBackView.frame.size.height), self.genderBackView.frame.size.width, 45);
    self.memberBackView.frame = CGRectMake(self.memberBackView.frame.origin.x, (self.genderBackView.frame.origin.y + self.genderBackView.frame.size.height), self.memberBackView.frame.size.width, self.memberBackView.frame.size.height);
    self.lastLoginBackView.frame = CGRectMake(self.lastLoginBackView.frame.origin.x, (self.memberBackView.frame.origin.y + self.memberBackView.frame.size.height), self.lastLoginBackView.frame.size.width, self.lastLoginBackView.frame.size.height);
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.lastLoginBackView.frame.origin.y + self.lastLoginBackView.frame.size.height + 5);
    
    if (sender.tag == 1) {
        if ([self.passwordButton isSelected]) {
            self.passwordButton.selected = NO;
            [self.passwordArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
            self.passwordBackView.frame = CGRectMake(self.passwordBackView.frame.origin.x, self.passwordBackView.frame.origin.y, self.passwordBackView.frame.size.width, 45);
        } else {
            self.passwordButton.selected = YES;
            self.emailButton.selected = NO;
            self.telButton.selected = NO;
            self.birthDateButton.selected = NO;
            self.genderButton.selected = NO;
            [self.passwordArrowImg setImage:[UIImage imageNamed:@"downarrow1.png"]];
            self.passwordBackView.frame = CGRectMake(self.passwordBackView.frame.origin.x, self.passwordBackView.frame.origin.y, self.passwordBackView.frame.size.width, (self.passwordEditContentView.frame.origin.y + self.passwordEditContentView.frame.size.height));
        }
    } else if (sender.tag == 2){
        if ([self.emailButton isSelected]) {
            self.emailButton.selected = NO;
            [self.emailArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
            self.emailBackView.frame = CGRectMake(self.emailBackView.frame.origin.x, self.emailBackView.frame.origin.y, self.emailBackView.frame.size.width, 45);
        } else {
            self.passwordButton.selected = NO;
            self.emailButton.selected = YES;
            self.telButton.selected = NO;
            self.birthDateButton.selected = NO;
            self.genderButton.selected = NO;
            [self.emailArrowImg setImage:[UIImage imageNamed:@"downarrow1.png"]];
            self.emailBackView.frame = CGRectMake(self.emailBackView.frame.origin.x, self.emailBackView.frame.origin.y, self.emailBackView.frame.size.width, (self.emailEditContentView.frame.origin.y + self.emailEditContentView.frame.size.height));
        }
    }
    else if (sender.tag == 3){
        if ([self.telButton isSelected]) {
            self.telButton.selected = NO;
            [self.telArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
            self.telBackView.frame = CGRectMake(self.telBackView.frame.origin.x, self.telBackView.frame.origin.y, self.telBackView.frame.size.width, 45);
        } else {
            self.passwordButton.selected = NO;
            self.emailButton.selected = NO;
            self.telButton.selected = YES;
            self.birthDateButton.selected = NO;
            self.genderButton.selected = NO;
            [self.telArrowImg setImage:[UIImage imageNamed:@"downarrow1.png"]];
            self.telBackView.frame = CGRectMake(self.telBackView.frame.origin.x, self.telBackView.frame.origin.y, self.telBackView.frame.size.width, (self.telEditContentView.frame.origin.y + self.telEditContentView.frame.size.height));
        }
    }
    else if (sender.tag == 4){
        if ([self.birthDateButton isSelected]) {
            self.birthDateButton.selected = NO;
            [self.birthDateArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
            self.birthDateBackView.frame = CGRectMake(self.birthDateBackView.frame.origin.x, self.birthDateBackView.frame.origin.y, self.birthDateBackView.frame.size.width, 45);
        } else {
            self.passwordButton.selected = NO;
            self.emailButton.selected = NO;
            self.telButton.selected = NO;
            self.birthDateButton.selected = YES;
            self.genderButton.selected = NO;
            [self.birthDateArrowImg setImage:[UIImage imageNamed:@"downarrow1.png"]];
            self.birthDateBackView.frame = CGRectMake(self.birthDateBackView.frame.origin.x, self.birthDateBackView.frame.origin.y, self.birthDateBackView.frame.size.width, (self.birthDateEditContentView.frame.origin.y + self.birthDateEditContentView.frame.size.height));
        }
    }
    else if (sender.tag == 5){
        if ([self.genderButton isSelected]) {
            self.genderButton.selected = NO;
            [self.genderArrowImg setImage:[UIImage imageNamed:@"rightarrow1.png"]];
            self.genderBackView.frame = CGRectMake(self.genderBackView.frame.origin.x, self.genderBackView.frame.origin.y, self.genderBackView.frame.size.width, 45);
        } else {
            self.passwordButton.selected = NO;
            self.emailButton.selected = NO;
            self.telButton.selected = NO;
            self.birthDateButton.selected = NO;
            self.genderButton.selected = YES;
            [self.genderArrowImg setImage:[UIImage imageNamed:@"downarrow1.png"]];
            self.genderBackView.frame = CGRectMake(self.genderBackView.frame.origin.x, self.genderBackView.frame.origin.y, self.genderBackView.frame.size.width, (self.genderEditContentView.frame.origin.y + self.genderEditContentView.frame.size.height));
        }
    }
    
    self.passwordBackView.frame = CGRectMake(self.passwordBackView.frame.origin.x, self.passwordBackView.frame.origin.y, self.passwordBackView.frame.size.width, self.passwordBackView.frame.size.height);
    self.emailBackView.frame = CGRectMake(self.emailBackView.frame.origin.x, (self.passwordBackView.frame.origin.y + self.passwordBackView.frame.size.height), self.emailBackView.frame.size.width, self.emailBackView.frame.size.height);
    self.telBackView.frame = CGRectMake(self.telBackView.frame.origin.x, (self.emailBackView.frame.origin.y + self.emailBackView.frame.size.height), self.telBackView.frame.size.width, self.telBackView.frame.size.height);
    self.birthDateBackView.frame = CGRectMake(self.birthDateBackView.frame.origin.x, (self.telBackView.frame.origin.y + self.telBackView.frame.size.height), self.birthDateBackView.frame.size.width, self.birthDateBackView.frame.size.height);
    self.genderBackView.frame = CGRectMake(self.genderBackView.frame.origin.x, (self.birthDateBackView.frame.origin.y + self.birthDateBackView.frame.size.height), self.genderBackView.frame.size.width, self.genderBackView.frame.size.height);
    self.memberBackView.frame = CGRectMake(self.memberBackView.frame.origin.x, (self.genderBackView.frame.origin.y + self.genderBackView.frame.size.height), self.memberBackView.frame.size.width, self.memberBackView.frame.size.height);
    self.lastLoginBackView.frame = CGRectMake(self.lastLoginBackView.frame.origin.x, (self.memberBackView.frame.origin.y + self.memberBackView.frame.size.height), self.lastLoginBackView.frame.size.width, self.lastLoginBackView.frame.size.height);
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.lastLoginBackView.frame.origin.y + self.lastLoginBackView.frame.size.height + 5);
    
}

-(void)fetchDatafromServer
{
    /////////////////////////// nsurl /////////////////////////
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request ;
        
        NSDictionary *requestData;
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        request = [NSMutableURLRequest
                   requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/profile_page"]];
        requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [userLoginStatus objectForKey:@"UserId"], @"user_id",
                       nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionProfile = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
    }
    /////////////////////////// nsurl /////////////////////////
}


////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionUpdatePassword) {
        responseDataUpdatePassword = [[NSMutableData alloc] init];
        self.userUpdatePasswordDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseData = [[NSMutableData alloc] init];
        self.userProfileDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    if (connection == connectionUpdatePassword) {
        [responseDataUpdatePassword appendData:data];
    } else {
        [responseData appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    if (connection == connectionUpdatePassword) {
        self.userUpdatePasswordDictionary = [NSJSONSerialization JSONObjectWithData:responseDataUpdatePassword options:kNilOptions error:&error];
        //NSLog(@"%@",[self.userUpdatePasswordDictionary objectForKey:@"Message"]);
        self.oldPasswordTxt.text = @"";
        self.passwordNewTxt.text = @"";
        self.confirmPasswordTxt.text = @"";
        [self.passwordButton sendActionsForControlEvents:UIControlEventTouchUpInside];
        [ApplicationDelegate showAlertWithMessage:[self.userUpdatePasswordDictionary objectForKey:@"Message"] title:nil];
        
    } else {
        self.userProfileDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        
        if ([ApplicationDelegate isValid:[self.userProfileDictionary objectForKey:@"user_profile"]])
        {
            NSLog(@"%@",[self.userProfileDictionary objectForKey:@"user_profile"]);
            NSLog(@"%@",[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"name"]);
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"name"]])
            {
                self.nameValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"name"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"email"]])
            {
                self.emailValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"email"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"mobile"]])
            {
                self.telValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"mobile"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"birthdate"]])
            {
                self.birthDateValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"birthdate"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"gender"]])
            {
                self.genderValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"gender"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"join_date"]])
            {
                self.memberValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"join_date"];
            }
            if ([ApplicationDelegate isValid:[[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"last_login"]])
            {
                self.lastLoginValueLabel.text = [[[self.userProfileDictionary objectForKey:@"user_profile"] objectAtIndex:0] objectForKey:@"last_login"];
            }
        }
    }
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////

- (IBAction)passwordUpdateBtnActn:(UIButton *)sender {
    if (self.oldPasswordTxt.text.length == 0) {
        [ApplicationDelegate showAlertWithMessage:@"Please enter your current password" title:@"Sorry..!"];
    }
    else if(self.passwordNewTxt.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your new password" title:@"Sorry..!"];
    }
    else if(self.confirmPasswordTxt.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please confirm your new password" title:@"Sorry..!"];
    }
    else if (![self.passwordNewTxt.text isEqualToString:self.confirmPasswordTxt.text]){
        [ApplicationDelegate showAlertWithMessage:@"Confirmation password is wrong" title:@"Sorry..!"];
    }
    else{
        [self postPasswordDetails];
    }
}

-(void)postPasswordDetails{
    /////////////////////////// nsurl /////////////////////////
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request ;
        
        NSDictionary *requestData;
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        request = [NSMutableURLRequest
                   requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/password_updation"]];
        requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                       [userLoginStatus objectForKey:@"UserId"], @"user_id",
                       self.oldPasswordTxt.text, @"old_password",
                       self.passwordNewTxt.text, @"new_password",
                       nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionUpdatePassword = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
    }
    /////////////////////////// nsurl /////////////////////////
}

- (IBAction)emailUpdateBtnActn:(UIButton *)sender {
}

- (IBAction)telUpdateBtnActn:(UIButton *)sender {
}
- (IBAction)birthDateValueChanged:(UIDatePicker *)sender {
    NSDate *chosen = [self.birthDatePicker date];
    NSLog(@"%@",chosen);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    //Optionally for time zone conversions
    [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
    
    NSString *stringFromDate = [formatter stringFromDate:chosen];
    self.dateOfBirthNewTxt.text = stringFromDate;
}

- (IBAction)birthDateUpdateBtnActn:(UIButton *)sender {
}

- (IBAction)genderUpdateBtnActn:(UIButton *)sender {
    NSString * genderCategory;
    if (self.maleBtn.selected) {
        genderCategory = [NSString stringWithFormat:@"MALE"];
    } else {
        genderCategory = [NSString stringWithFormat:@"FEMALE"];
    }
}
- (IBAction)genderSelectBtnActn:(UIButton *)sender {
    if (self.maleBtn.selected) {
        self.maleBtn.selected = NO;
        self.femaleBtn.selected = YES;
    } else {
        self.maleBtn.selected = YES;
        self.femaleBtn.selected = NO;
    }
}
@end
