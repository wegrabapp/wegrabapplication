//
//  NewsLetterCityDetails.h
//  WeGrab
//
//  Created by Ronish on 2/5/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NewsLetterCityDetails : NSObject

@property (nonatomic, retain) NSString * city_id;
@property (nonatomic, retain) NSString * city_Name;
@property (nonatomic, retain) NSString * country_id;
@property (nonatomic, retain) NSString * country_Name;
@property (nonatomic, retain) NSString * email_notification;
@property (nonatomic, retain) NSString * sms_notificaton;

@end
