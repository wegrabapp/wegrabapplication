//
//  GrabItInnerViewController.h
//  WeGrab
//
//  Created by satheesh on 1/22/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GrabItDealOptionsData.h"

@interface GrabItInnerViewController : UIViewController{
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableDictionary * quantityDataDictionary;
    NSMutableData * responseDataGiftDeal;
    NSMutableData * responseDataGrabDeal;
    NSMutableData * responseDataGiftVoucher;
    NSMutableDictionary * giftDealDataDictionary;
    NSMutableDictionary * giftVoucherDataDictionary;
    NSMutableDictionary * grabDealDataDictionary;
    NSURLConnection *connectionQuantity;
    NSURLConnection *connectionGiftDeal;
    NSURLConnection *connectionGrabDeal;
    NSURLConnection *connectionGiftDealVoucher;
}
@property (strong, nonatomic) GrabItDealOptionsData * grabItDealData;
@property (strong, nonatomic) NSString * grabItDealId;
@property (strong, nonatomic) NSString * grabItVoucherId;
@property (strong, nonatomic) NSString * grabItQuantity;
@property (strong, nonatomic) NSString * grabItVoucherAmount;
@property (strong, nonatomic) NSString * grabItRate;

@property (strong, nonatomic) NSMutableArray * voucherAmountArray;
@property (strong, nonatomic) NSMutableArray * voucherIDArray;
@property (strong, nonatomic) NSMutableArray * voucherQuantityArray;

@property (strong, nonatomic) NSString * giftTo;
@property (strong, nonatomic) NSString * giftFrom;
@property (strong, nonatomic) NSString * giftEmail;
@property (strong, nonatomic) NSString * giftMessage;

@property (strong, nonatomic) NSString * grabItState;

@property (weak, nonatomic) IBOutlet UITextField *quantityValueTxtView;
@property (weak, nonatomic) IBOutlet UILabel *priceValueLBL;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLBL;
- (IBAction)billingInformationBtnActn:(UIButton *)sender;
- (IBAction)grabItBtnActn:(UIButton *)sender;
- (IBAction)quantityChanged:(UITextField *)sender;

@end
