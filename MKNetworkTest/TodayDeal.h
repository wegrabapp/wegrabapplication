//
//  TodayDeal.h
//  WeGrab
//
//  Created by Sreeraj VR on 07/01/2016.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TodayDeal : NSObject

@property (nonatomic, retain) NSString *DealID;
@property (nonatomic, retain) NSString *DealImage;
@property (nonatomic, retain) NSString *DealHeadline;
@property (nonatomic, retain) NSString *DealDescription;
@property (nonatomic, retain) NSString *OriginalPrice;
@property (nonatomic, retain) NSString *OfferPrice;
@property (nonatomic, retain) NSString *Discount;
@property (nonatomic, retain) NSString *ExtraDiscount;
@property (nonatomic, retain) NSString *MinimumNoofBuyer;
@property (nonatomic, retain) NSString *MaximumBuyerNo;
@property (nonatomic, retain) NSString *Timer;
@property (nonatomic, retain) NSString *AboutTheBusiness;
@property (nonatomic, retain) NSString *FinePrint;
@property (nonatomic, retain) NSString *SoldDealsNumber;
@property (nonatomic, retain) NSString *CurrentBuyerNo;


@end
