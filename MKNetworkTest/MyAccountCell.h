//
//  MyAccountCell.h
//  WeGrab
//
//  Created by satheesh on 1/27/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *transactionDateValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusValueLbl;

@end
