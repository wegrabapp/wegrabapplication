//
//  AppEngine.h
//  MKNetworkTest
//
//  Created by Sreeraj VR on 29/10/2014.
//  Copyright (c) 2014 mawaqaa. All rights reserved.
//

#import "MKNetworkEngine.h"

@interface AppEngine : MKNetworkEngine

typedef void (^ResponseDictionaryBlock)(NSMutableDictionary* responseDictionary);

typedef void (^ResponseArrayBlock)(NSMutableArray* responseArray);

-(void)getCountryList:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;


-(void)todaydealList:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock;

@end
