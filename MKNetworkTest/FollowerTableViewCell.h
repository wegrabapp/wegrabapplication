//
//  FollowerTableViewCell.h
//  WeGrab
//
//  Created by satheesh on 1/20/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *emailValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountEarnedValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *statusValueLbl;

@end
