//
//  MoreGrabViewController.h
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SectionViewMoreGrab.h"

@interface MoreGrabViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SectionViewMoreGrab,NSURLConnectionDelegate>{
    NSMutableDictionary * countryDataDictionary;
    NSUserDefaults * userLocation;
    NSMutableData * responseData;
    NSUserDefaults * userLoginStatus;
}
@property (weak, nonatomic) IBOutlet UITableView *moreGrabTableView;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) NSString * locationString;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIView *labelBackView;
@property (strong, nonatomic) NSMutableDictionary * moreGrabDictionary;
- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
