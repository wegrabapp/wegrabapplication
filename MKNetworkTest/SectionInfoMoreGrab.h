//
//  SectionInfoMoreGrab.h
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@class SectionViewMoreGrab;
@class CategoryName;
@class CountryDetails;
@class MoreGrabDetails;

@interface SectionInfoMoreGrab : NSObject

@property (assign) BOOL open;
@property (strong) CategoryName *category;
@property (strong) CountryDetails *country;
@property (strong) MoreGrabDetails *moreGrabDetails;
@property (strong) SectionViewMoreGrab *sectionViewMoreGrab;
@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;

@end
