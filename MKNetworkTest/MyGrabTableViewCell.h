//
//  MyGrabTableViewCell.h
//  WeGrab
//
//  Created by Ronish on 2/11/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGrabTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dealDescriptionLbl;
@property (weak, nonatomic) IBOutlet UILabel *dealDescriptionValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *cityLbl;
@property (weak, nonatomic) IBOutlet UILabel *cityValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *purchaseDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *purchaseDateValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateLbl;
@property (weak, nonatomic) IBOutlet UILabel *expiryDateValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceLbl;
@property (weak, nonatomic) IBOutlet UILabel *priceValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *moreDetailsLbl;
@property (weak, nonatomic) IBOutlet UIButton *moreDetailsBtn;
@property (weak, nonatomic) IBOutlet UIView *voucherBackView;

@end
