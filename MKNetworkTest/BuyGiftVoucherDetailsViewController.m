//
//  BuyGiftVoucherDetailsViewController.m
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "BuyGiftVoucherDetailsViewController.h"
#import "GrabItInnerViewController.h"

@interface BuyGiftVoucherDetailsViewController ()

@end

@implementation BuyGiftVoucherDetailsViewController

- (void) viewWillAppear:(BOOL)animated{
    self.amountLabel.text = [NSString stringWithFormat:@"$%@",self.voucherAmount];
    
    self.toNameTxtView.text = self.giftToTextData;
    self.fromNameTxtView.text = self.giftFromTextData;
    self.emailTextView.text = self.giftEmailTextData;
    self.messageTxtView.text = self.giftMessageTextData;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"%@",self.voucherID);
    NSLog(@"%@",self.voucherAmountArray);
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cancelBtnActn:(UIButton *)sender {
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}



#pragma mark -
#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:
(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView
numberOfRowsInComponent:(NSInteger)component
{
    return self.voucherAmountArray.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView
             titleForRow:(NSInteger)row
            forComponent:(NSInteger)component
{
    return self.voucherAmountArray [row];
}

#pragma mark -
#pragma mark PickerView Delegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row
      inComponent:(NSInteger)component
{
    
    self.amountLabel.text = [NSString stringWithFormat:@"$%@",self.voucherAmountArray [row]];
    self.voucherID = [self.voucherIDArray objectAtIndex:row];
    self.voucherQuantity = [self.voucherQuantityArray objectAtIndex:row];
    self.voucherAmount = [self.voucherAmountArray objectAtIndex:row];
}

- (IBAction)grabItBtnActn:(UIButton *)sender {
    if (self.toNameTxtView.text.length == 0) {
        [ApplicationDelegate showAlertWithMessage:@"Please enter the name of the gift recipient" title:nil];
    } else if (self.fromNameTxtView.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your name" title:nil];
    }else if (self.emailTextView.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter recepients email address" title:nil];
    }else{
        //[self.view ]
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [self.view.window.layer addAnimation:transition forKey:nil];
        [self dismissViewControllerAnimated:NO completion:nil];
        
        
        GrabItInnerViewController * grabitInnerVc = [[GrabItInnerViewController alloc]initWithNibName:@"GrabItInnerViewController" bundle:nil];
        grabitInnerVc.view.backgroundColor = [UIColor clearColor];
        //NSLog(@"%@",self.dealId);
        
        grabitInnerVc.grabItState = @"Voucher";
        grabitInnerVc.grabItVoucherId = self.voucherID;
        grabitInnerVc.grabItVoucherAmount = self.voucherAmount;
        grabitInnerVc.grabItQuantity = self.voucherQuantity;
        grabitInnerVc.giftTo = self.toNameTxtView.text;
        grabitInnerVc.giftFrom = self.fromNameTxtView.text;
        grabitInnerVc.giftEmail = self.emailTextView.text;
        grabitInnerVc.giftMessage = self.messageTxtView.text;
        
        grabitInnerVc.voucherAmountArray = self.voucherAmountArray;
        grabitInnerVc.voucherIDArray = self.voucherIDArray;
        grabitInnerVc.voucherQuantityArray = self.voucherQuantityArray;
        
        
        //NSLog(@"%@",self.rootNav);
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[grabitInnerVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:grabitInnerVc animated:NO];
            //[self.rootNav pushViewController:grabitInnerVc animated:NO];
        }
    }
}

- (IBAction)editBtnActn:(UIButton *)sender {
    self.editAmountBackView.hidden = NO;
}

- (IBAction)okBtnAction:(UIButton *)sender {
    self.editAmountBackView.hidden = YES;
}

- (IBAction)cancelButtonActn:(UIButton *)sender {
    self.editAmountBackView.hidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
