//
//  MyCitiesEditViewController.h
//  WeGrab
//
//  Created by satheesh on 2/2/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCitiesEditViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableDictionary * myCitiesDataDictionary;
    NSMutableData * responseDataMyCitiesDelete;
    NSMutableDictionary * myCitiesDeleteDataDictionary;
    NSURLConnection *connectionDeleteMyCity;
}
@property (weak, nonatomic) IBOutlet UITableView *myCitiesEditTableView;
@property (strong, nonatomic) NSMutableArray * myOtherCityArray;
- (IBAction)deleteBtnActn:(UIButton *)sender;
- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
