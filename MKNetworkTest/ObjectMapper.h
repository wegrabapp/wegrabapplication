//
//  ObjectMapper.h
//  WeGrab
//
//  Created by Sreeraj VR on 04/01/2016.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TodayDeal.h"
#import "MoreGrabCellDetails.h"
#import "GrabItDealOptionsData.h"

@interface ObjectMapper : NSObject

-(TodayDeal *)getTodayDealListFromDictionary:(NSMutableDictionary *)TodayDealDictionary;
-(MoreGrabCellDetails *)getMoreGrabDealListFromDictionary:(NSMutableDictionary *)TodayDealDictionary;
-(GrabItDealOptionsData *)getGrabItOptionsFromDictionary:(NSMutableDictionary *)GrabItOptionDictionary;

@end
