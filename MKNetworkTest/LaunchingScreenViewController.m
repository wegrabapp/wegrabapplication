//
//  LaunchingScreenViewController.m
//  WeGrab
//
//  Created by satheesh on 12/30/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import "LaunchingScreenViewController.h"
#import "HomeViewController.h"
#import "CountryLocationViewController.h"
#import "GrabItDealPageViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)


@interface LaunchingScreenViewController (){
    HomeViewController * homePageViewObj;
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    CLPlacemark *placemark;
}



@end

@implementation LaunchingScreenViewController{
//    CLLocationManager * locationManager;
}


-(void)viewWillAppear:(BOOL)animated{
    
    //[self getCountryName];
    
}

-(void)getCountryName
{
    

    [ApplicationDelegate.engine getCountryList:^(NSMutableDictionary *response)
     {
         
          [ApplicationDelegate addProgressHUDToView:self.view];
        {
            
            NSLog(@"%@",response);
           
        }
    } errorHandler:^(NSError *error) {
        
     
        
    }];

}




- (void)viewDidLoad {
    [super viewDidLoad];
    
    failureFlag = 0;
    
    self.navigationController.navigationBarHidden = YES;
    // Do any additional setup after loading the view from its nib.
    
    locationManager = [[CLLocationManager alloc]init];
    geocoder = [[CLGeocoder alloc] init];
    
    if(IS_OS_8_OR_LATER) {
        //[locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
    
    [locationManager startUpdatingLocation];
    
    [self performSelector:@selector(getCurrentLocationFunc)
               withObject:self
               afterDelay:2];
    
    //[self getCurrentLocation:NULL];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getCurrentLocationFunc{
    
    userLocation = [NSUserDefaults standardUserDefaults];
    //(sender!=nil)&&(![sender isEqual:[NSNull null]])
    if (([userLocation objectForKey:@"Country"] == nil) || ([[userLocation objectForKey:@"Country"]isEqual:[NSNull null]])) {
        NSLog(@"Empty");
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        [locationManager startUpdatingLocation];
    } else {
        [locationManager stopUpdatingLocation];
        NSLog(@"Not empty");
        NSLog(@"%@",[userLocation objectForKey:@"Country"]);
        NSLog(@"%@",[userLocation objectForKey:@"City"]);
        
//        GrabItDealPageViewController * grabDealPageViewObj;
//        
//        grabDealPageViewObj = [[GrabItDealPageViewController alloc]initWithNibName:@"GrabItDealPageViewController" bundle:nil];
//        
//        [self.navigationController presentViewController:grabDealPageViewObj animated:YES completion:nil];
        
        HomeViewController * homeViewObj;
        
        homeViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        
        [self.navigationController pushViewController:homeViewObj animated:YES];
    }
    
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    //NSLog(@"Hello......");
    if (failureFlag == 0) {
        failureFlag = 1;
//        CountryLocationTableViewController * countrySelectionViewObj;
//        
//        countrySelectionViewObj = [[CountryLocationTableViewController alloc]initWithNibName:@"CountryLocationTableViewController" bundle:nil];
//        
//        [self.navigationController presentViewController:countrySelectionViewObj animated:YES completion:nil];
        NSLog(@"%@",self.navigationController);
        CountryLocationViewController * countrySelectionViewObj;
        
        countrySelectionViewObj = [[CountryLocationViewController alloc]initWithNibName:@"CountryLocationViewController" bundle:nil];
        countrySelectionViewObj.locationString = @"Initial";
        [self.navigationController pushViewController:countrySelectionViewObj animated:YES ];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"didUpdateToLocation: %@", newLocation);
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation != nil) {
    
    }
    
    // Reverse Geocoding
    NSLog(@"Resolving the Address");
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Found placemarks: %@, error: %@", placemarks, error);
        if (error == nil && [placemarks count] > 0) {
            placemark = [placemarks lastObject];
            userLocation = [NSUserDefaults standardUserDefaults];
            [userLocation setObject:placemark.country forKey:@"Country"];
            [userLocation setObject:placemark.locality forKey:@"City"];
            [userLocation synchronize];
            [locationManager stopUpdatingLocation];
            HomeViewController * homeViewObj;
            
            homeViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
            
            [self.navigationController pushViewController:homeViewObj animated:YES];
        } else {
            NSLog(@"%@", error.debugDescription);
        }
    } ];
    
}

-(BOOL)locationServicesIsEnabled
{
    if (![CLLocationManager locationServicesEnabled] || ![CLLocationManager     authorizationStatus])
        return NO;
    return YES;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
