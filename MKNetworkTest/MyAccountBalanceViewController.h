//
//  MyAccountBalanceViewController.h
//  WeGrab
//
//  Created by satheesh on 1/27/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SectionViewMoreGrab.h"
#import "SectionView.h"

@interface MyAccountBalanceViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SectionViewMoreGrab,NSURLConnectionDelegate,SectionView>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    //NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * accountBalanceDataDictionary;
}

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *myAccountTableView;
//@property (strong, nonatomic) NSMutableDictionary * moreGrabDictionary;
@property (weak, nonatomic) IBOutlet UILabel *totalBalanceValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *thisMonthBalanceValueLbl;

@end
