//
//  MyCitiesViewController.h
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SectionViewCities.h"
//#import "SectionViewMoreGrab.h"
//#import "SectionView.h"

@interface MyCitiesViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SectionViewCities,NSURLConnectionDelegate>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    //NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * myCitiesDataDictionary;
    }
- (IBAction)editButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITableView *myCitiesTableView;
@property (strong, nonatomic) NSMutableArray * myOtherCityArray;
@property (weak, nonatomic) IBOutlet UIView *myCitiesInnerContainerView;
- (IBAction)addAnotherCityBtnActn:(UIButton *)sender;
@end
