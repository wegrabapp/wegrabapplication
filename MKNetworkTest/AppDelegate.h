//
//  AppDelegate.h
//  MKNetworkTest
//
//  Created by Sreeraj VR on 29/10/2014.
//  Copyright (c) 2014 mawaqaa. All rights reserved.
//

#define ApplicationDelegate ((AppDelegate *)[UIApplication sharedApplication].delegate)

#import <UIKit/UIKit.h>

#import "LaunchingScreenViewController.h"

#import "HomeViewController.h"

#import "AppEngine.h"

#import "MBProgressHUD.h"

#import "ObjectMapper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate,MBProgressHUDDelegate>
{
     Reachability* internetReachable;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) AppEngine *engine;

@property(strong,nonatomic) MBProgressHUD *HUD;

@property (strong, nonatomic)LaunchingScreenViewController *launchViewObj;

@property (strong, nonatomic) ObjectMapper *mapper;

@property (strong, nonatomic)HomeViewController *homeTabObj;

@property (strong, nonatomic)UINavigationController *homeTabNav;

@property (strong, nonatomic)UINavigationController *mainNav;

-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title;

-(void)addProgressHUDToView:(UIView *)parentview;

-(void)removeProgressHUD;

-(BOOL)isValid:(id)sender;

-(NSString *)getHTTPCorrectedURLFromUrl:(NSString *)urlString;

-(void)loadMarqueeLabelWithText:(NSString *)lblText Font:(UIFont *)fontVal InPlaceOfLabel:(UILabel *)uilbl;

-(BOOL)checkNetworkAvailability;

@end
