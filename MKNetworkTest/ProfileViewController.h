//
//  ProfileViewController.h
//  WeGrab
//
//  Created by satheesh on 1/19/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController{
    NSMutableData * responseData;
    NSMutableData * responseDataUpdatePassword;
    NSUserDefaults * userLoginStatus;
    NSURLConnection *connectionUpdatePassword;
    NSURLConnection *connectionProfile;
}
@property (strong, nonatomic) NSMutableDictionary * userProfileDictionary;
@property (strong, nonatomic) NSMutableDictionary * userUpdatePasswordDictionary;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (weak, nonatomic) IBOutlet UIView *nameBackView;
@property (weak, nonatomic) IBOutlet UIImageView *nameBackgroundIMG;
@property (weak, nonatomic) IBOutlet UILabel *nameValueLabel;
@property (weak, nonatomic) IBOutlet UIView *passwordBackView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *emailBackView;
@property (weak, nonatomic) IBOutlet UIImageView *emailBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *telBackView;
@property (weak, nonatomic) IBOutlet UIImageView *telBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *birthDateBackView;
@property (weak, nonatomic) IBOutlet UIImageView *birthDateBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *genderBackView;
@property (weak, nonatomic) IBOutlet UIImageView *genderBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *memberBackView;
@property (weak, nonatomic) IBOutlet UIImageView *memberBackgroundIMG;
@property (weak, nonatomic) IBOutlet UIView *lastLoginBackView;
@property (weak, nonatomic) IBOutlet UIImageView *lastLoginBackgroundIMG;
@property (weak, nonatomic) IBOutlet UILabel *passwordValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *telValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *birthDateValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *memberValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastLoginValueLabel;
@property (weak, nonatomic) IBOutlet UIImageView *passwordArrowImg;
@property (weak, nonatomic) IBOutlet UIImageView *emailArrowImg;
@property (weak, nonatomic) IBOutlet UIImageView *telArrowImg;
@property (weak, nonatomic) IBOutlet UIImageView *birthDateArrowImg;
@property (weak, nonatomic) IBOutlet UIImageView *genderArrowImg;

@property (weak, nonatomic) IBOutlet UIView *passwordEditContentView;
@property (weak, nonatomic) IBOutlet UIView *emailEditContentView;
@property (weak, nonatomic) IBOutlet UIView *telEditContentView;
@property (weak, nonatomic) IBOutlet UIView *birthDateEditContentView;
@property (weak, nonatomic) IBOutlet UIView *genderEditContentView;
- (IBAction)tableSwapInOutBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *passwordButton;
@property (weak, nonatomic) IBOutlet UIButton *emailButton;
@property (weak, nonatomic) IBOutlet UIButton *telButton;
@property (weak, nonatomic) IBOutlet UIButton *birthDateButton;
@property (weak, nonatomic) IBOutlet UIButton *genderButton;
- (IBAction)passwordUpdateBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTxt;
@property (weak, nonatomic) IBOutlet UITextField *passwordNewTxt;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTxt;
@property (weak, nonatomic) IBOutlet UITextField *emailNewTxt;
@property (weak, nonatomic) IBOutlet UITextField *telePhoneNewTxt;
@property (weak, nonatomic) IBOutlet UIView *calendarBackView;
@property (weak, nonatomic) IBOutlet UITextField *dateOfBirthNewTxt;
- (IBAction)emailUpdateBtnActn:(UIButton *)sender;
- (IBAction)telUpdateBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIDatePicker *birthDatePicker;
- (IBAction)birthDateValueChanged:(UIDatePicker *)sender;
- (IBAction)birthDateUpdateBtnActn:(UIButton *)sender;
- (IBAction)genderUpdateBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *maleBtn;
@property (weak, nonatomic) IBOutlet UIButton *femaleBtn;
- (IBAction)genderSelectBtnActn:(UIButton *)sender;
@end
