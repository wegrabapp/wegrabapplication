//
//  AccountBalanceDetails.h
//  WeGrab
//
//  Created by satheesh on 1/27/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AccountBalanceDetails : NSObject

@property (nonatomic, retain) NSString *history_name;
@property (nonatomic, retain) NSArray *historyDetails;

@end
