//
//  MyAccountLogoutViewController.m
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MyAccountLogoutViewController.h"
#import "HomeViewController.h"
#import "LoginViewController.h"

@interface MyAccountLogoutViewController ()

@end

@implementation MyAccountLogoutViewController

-(void)viewWillAppear:(BOOL)animated{
    [HomeViewController sharedViewController].backBtn.hidden = YES;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"My Account";
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    //(sender!=nil)&&(![sender isEqual:[NSNull null]])
    if ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGIN"]) {
        [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
    } 
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)loginBtnActn:(UIButton *)sender {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    LoginViewController * loginViewObj = [[LoginViewController alloc]initWithNibName:@"LoginViewController" bundle:nil];
    [self.view.window.rootViewController presentViewController:loginViewObj animated:NO completion:nil];
}
@end
