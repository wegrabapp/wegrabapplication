//
//  AddCityViewController.h
//  WeGrab
//
//  Created by satheesh on 2/3/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCityViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>{
    NSMutableArray  *arrayForBool;
    NSArray *sectionTitleArray;
    NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * addCityDataDictionary;
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableData * responseDataAddCity;
    NSURLConnection *connectionAddMyCity;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UITableView *countryTableView;
@property (weak, nonatomic) NSString * locationString;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIView *labelBackView;
- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
