//
//  GrabItViewController.m
//  WeGrab
//
//  Created by satheesh on 1/21/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "GrabItViewController.h"
#import "GrabItDealOptionsData.h"
#import "GrabItOfferCell.h"
#import "GrabItInnerViewController.h"

@interface GrabItViewController ()

@end

@implementation GrabItViewController

-(void)viewWillAppear:(BOOL)animated{
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Grab it!";
    
    //NSLog(@"%@",self.grabItDealOptionDetailArray);
    if (self.grabItDealOptionDetailArray.count > 1) {
        
    }
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.grabItDealOptionDetailArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    GrabItOfferCell *cell = (GrabItOfferCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"GrabItOfferCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    GrabItDealOptionsData * tempGrabItDealOption;
    tempGrabItDealOption = [self.grabItDealOptionDetailArray objectAtIndex:indexPath.row];
    cell.GrabitOfferLabel.text = [NSString stringWithFormat:@"%@ :%@($%@)",tempGrabItDealOption.optionName,tempGrabItDealOption.optionQuantity,tempGrabItDealOption.optionPrice];
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    return 55;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    NSMutableDictionary * tempDictionary = [[NSMutableDictionary alloc]initWithDictionary:[[self.grabItDealOptionDetailArray objectForKey:@"DealItem"] objectAtIndex:0]];
//    GrabItDealOptionsData *grabItDealOptionDetails = [[GrabItDealOptionsData alloc] init];
//    grabItDealOptionDetails = [ApplicationDelegate.mapper getGrabItOptionsFromDictionary:tempDictionary];
    
    GrabItInnerViewController *grabItInnerViewObj = [[GrabItInnerViewController alloc] initWithNibName:@"GrabItInnerViewController" bundle:nil];
    grabItInnerViewObj.grabItDealData = [self.grabItDealOptionDetailArray objectAtIndex:indexPath.row];
    grabItInnerViewObj.view.backgroundColor = [UIColor clearColor];
    //grabItInnerViewObj.grabItDealId =
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[grabItInnerViewObj class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:grabItInnerViewObj animated:NO];
    }
}


@end
