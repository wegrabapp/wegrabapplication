//
//  SingleGrabItDealViewController.h
//  WeGrab
//
//  Created by Ronish on 2/15/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SingleGrabItDealViewController : UIViewController<NSURLConnectionDelegate>{
    NSMutableData * responseDataTodaysDeal;
    NSMutableData * responseDataGrabit;
    NSUserDefaults * userLocation;
    NSURLConnection *connectionTodaysDeal;
    NSURLConnection *connectionGrabit;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollFirst;
//@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollSecond;

@property (weak, nonatomic) IBOutlet UIButton *giftItBtn;
@property (weak, nonatomic) IBOutlet UIButton *shareItBtn;

@property (weak, nonatomic) IBOutlet UIView *bottomViewFirst;
//@property (weak, nonatomic) IBOutlet UIView *bottomViewSecond;

- (IBAction)giftItBtnActn:(UIButton *)sender;
- (IBAction)shareItBtnActn:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *firstMainView;
//@property (weak, nonatomic) IBOutlet UIView *secondMainView;

@property (strong, nonatomic) IBOutlet UILabel *SecondsLabelFirst;
@property (strong, nonatomic) IBOutlet UILabel *MinutesLabelFirst;
@property (strong, nonatomic) IBOutlet UILabel *HoursLabelFirst;

//@property (strong, nonatomic) IBOutlet UILabel *SecondsLabelSecond;
//@property (strong, nonatomic) IBOutlet UILabel *MinutesLabelSecond;
//@property (strong, nonatomic) IBOutlet UILabel *HoursLabelSecond;

@property (strong, nonatomic) NSString *dealID;
@property (strong, nonatomic) NSString *dealFirstID;
//@property (strong, nonatomic) NSString *dealSecondID;
@property (weak, nonatomic) IBOutlet UIImageView *dealImageFirst;
//@property (weak, nonatomic) IBOutlet UIImageView *dealImageSecond;
@property (weak, nonatomic) IBOutlet UILabel *dealHeadingFirst;
//@property (weak, nonatomic) IBOutlet UILabel *dealHeadingSecond;
@property (weak, nonatomic) IBOutlet UITextView *dealDiscriptionFirst;
//@property (weak, nonatomic) IBOutlet UITextView *dealDiscriptionSecond;
@property (weak, nonatomic) IBOutlet UILabel *orginalPriceFirst;
//@property (weak, nonatomic) IBOutlet UILabel *orginalPriceSecond;
@property (weak, nonatomic) IBOutlet UILabel *offerPriceFirst;
//@property (weak, nonatomic) IBOutlet UILabel *offerPriceSecond;
@property (weak, nonatomic) IBOutlet UILabel *discountLblFirst;
//@property (weak, nonatomic) IBOutlet UILabel *discountLblSecond;
@property (weak, nonatomic) IBOutlet UILabel *countFirst;
//@property (weak, nonatomic) IBOutlet UILabel *countSecond;
@property (weak, nonatomic) IBOutlet UILabel *extradiscountFirst;
//@property (weak, nonatomic) IBOutlet UILabel *extradiscountSecond;
@property (weak, nonatomic) IBOutlet UILabel *minimumNumberFirst;
//@property (weak, nonatomic) IBOutlet UILabel *minimumNumberSecond;
@property (weak, nonatomic) IBOutlet UILabel *maximumNumberFirst;
//@property (weak, nonatomic) IBOutlet UILabel *maximumNumberSecond;
@property (weak, nonatomic) IBOutlet UILabel *soldCountFirst;
//@property (weak, nonatomic) IBOutlet UILabel *soldCountSecond;
@property (weak, nonatomic) IBOutlet UILabel *broughtCountFirst;
//@property (weak, nonatomic) IBOutlet UILabel *broughtCountSecond;
@property (weak, nonatomic) IBOutlet UILabel *remainingCountFirst;
//@property (weak, nonatomic) IBOutlet UILabel *remainingCountSecond;

@property (strong, nonatomic) IBOutlet UIWebView *webViewDescFirst;

//@property (strong, nonatomic) IBOutlet UIWebView *webViewDescSecond;

- (IBAction)buttonClicked:(id)sender;

@property (strong, nonatomic) NSMutableDictionary * todayDealDictionary;
@property (strong, nonatomic) NSMutableDictionary * grabItDictionary;
@property (strong, nonatomic) NSMutableArray * grabItDealOptionDetailArray;

@property (strong, nonatomic) IBOutlet UIButton *FinePrintButton;

@property (strong, nonatomic) IBOutlet UIButton *AboutButton;

//@property (strong, nonatomic) IBOutlet UIButton *FinePrintButtonSecond;

//@property (strong, nonatomic) IBOutlet UIButton *AboutButtonSecond;

@property (strong, nonatomic) IBOutlet UIProgressView *progressFirst;
//@property (strong, nonatomic) IBOutlet UIProgressView *progressSecond;

- (IBAction)grabitBtnActn:(UIButton *)sender;
@end
