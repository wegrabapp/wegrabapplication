//
//  GiftThisDealViewController.h
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftThisDealViewController : UIViewController{
    NSUserDefaults * userLocation;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *grabItBtn;
@property (strong, nonatomic) NSString * dealId;
@property (strong, nonatomic) NSString * giftToTextData;
@property (strong, nonatomic) NSString * giftFromTextData;
@property (strong, nonatomic) NSString * giftEmailTextData;
@property (strong, nonatomic) NSString * giftMessageTextData;

//@property (strong, nonatomic) UINavigationController * rootNav;
@property (weak, nonatomic) IBOutlet UITextField *giftToTxtView;
@property (weak, nonatomic) IBOutlet UITextField *giftFromTxtView;
@property (weak, nonatomic) IBOutlet UITextField *giftEmailTxtView;
@property (weak, nonatomic) IBOutlet UITextField *giftMessageTxtView;

- (IBAction)cancelBtnActn:(UIButton *)sender;
- (IBAction)grabItBtnActn:(UIButton *)sender;

@end
