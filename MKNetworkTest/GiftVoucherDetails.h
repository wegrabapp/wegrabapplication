//
//  GiftVoucherDetails.h
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GiftVoucherDetails : NSObject

@property (nonatomic, retain) NSString *voucherID;
@property (nonatomic, retain) NSString *voucherPrice;

@end
