//
//  MyGrabDetails.h
//  WeGrab
//
//  Created by Ronish on 2/11/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MyGrabDetails : NSObject

@property (nonatomic, retain) NSString * grab_Type;
@property (nonatomic, retain) NSMutableDictionary * deals;
@property (nonatomic, retain) NSMutableDictionary * more_Details;

@end
