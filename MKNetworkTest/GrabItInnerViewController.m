//
//  GrabItInnerViewController.m
//  WeGrab
//
//  Created by satheesh on 1/22/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "GrabItInnerViewController.h"
#import "GiftThisDealViewController.h"
#import "BillingViewController.h"
#import "BuyGiftVoucherDetailsViewController.h"

@interface GrabItInnerViewController ()

@end

@implementation GrabItInnerViewController

-(void)viewWillAppear:(BOOL)animated{
    //NSLog(@"%@",self.grabItDealId);
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    if ([self.grabItState isEqualToString:@"GrabItMain"]) {
        
    } else if ([self.grabItState isEqualToString:@"Gift"]){
        [[HomeViewController sharedViewController].backBtn addTarget:self action:@selector(backBtnActnOverRide) forControlEvents:UIControlEventTouchUpInside];
    }else if ([self.grabItState isEqualToString:@"Voucher"]){
        [[HomeViewController sharedViewController].backBtn addTarget:self action:@selector(backBtnActnOverRideVoucher) forControlEvents:UIControlEventTouchUpInside];
    }
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"Grab It!";
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        if ([self.grabItState isEqualToString:@"GrabItMain"] || [self.grabItState isEqualToString:@"Gift"]) {
            [self getQuantityData];
        } else {
            self.quantityValueTxtView.text = self.grabItQuantity;
            //self.grabItRate = [[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"];
            self.grabItRate = self.grabItVoucherAmount;
            self.priceValueLBL.text = [NSString stringWithFormat:@"$%.3f",[self.grabItVoucherAmount floatValue]];
            self.totalValueLBL.text = [NSString stringWithFormat:@"$%.3f",[self.grabItVoucherAmount floatValue]];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.quantityValueTxtView.text = self.grabItDealData.optionQuantity;
    NSLog(@"%@",self.grabItDealData.optionPrice);
    self.priceValueLBL.text = [NSString stringWithFormat:@"$%.3f",[self.grabItDealData.optionPrice floatValue]];
    self.totalValueLBL.text = [NSString stringWithFormat:@"$%.3f",[self.grabItDealData.optionPrice floatValue]* [self.grabItDealData.optionQuantity floatValue]];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) getQuantityData{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/grab_gift_deal_quantity"]];
        
//        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
//         [userLoginStatus objectForKey:@"UserId"], @"user_id",
//                                     self.grabItDealId, @"deal_id",
//                                     nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     self.grabItDealId, @"deal_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionQuantity = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionQuantity) {
        responseData = [[NSMutableData alloc] init];
        quantityDataDictionary = [[NSMutableDictionary alloc]init];
    } else if (connection == connectionGiftDeal){
        responseDataGiftDeal = [[NSMutableData alloc] init];
        giftDealDataDictionary = [[NSMutableDictionary alloc]init];
    }else if (connection == connectionGrabDeal){
        responseDataGrabDeal = [[NSMutableData alloc] init];
        grabDealDataDictionary = [[NSMutableDictionary alloc]init];
    }else if (connection == connectionGiftDealVoucher){
        responseDataGiftVoucher = [[NSMutableData alloc] init];
        giftVoucherDataDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == connectionQuantity) {
        [responseData appendData:data];
    } else if (connection == connectionGiftDeal){
        [responseDataGiftDeal appendData:data];
    }else if (connection == connectionGrabDeal){
        [responseDataGrabDeal appendData:data];
    }else if (connection == connectionGiftDealVoucher){
        [responseDataGiftVoucher appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    if (connection == connectionQuantity) {
        self.grabItRate = [[NSString alloc]init];
        self.grabItQuantity = [[NSString alloc]init];
        
        quantityDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSLog(@"%@",[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]);
        
        NSLog(@"%@",[[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"]);
        
        self.quantityValueTxtView.text = [NSString stringWithFormat:@"%@",[[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Quantity"]];
        self.grabItQuantity = [NSString stringWithFormat:@"%@",[[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Quantity"]];
        self.grabItRate = [[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"];
        
        //self.priceValueLBL.text = [[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"];
        //self.totalValueLBL.text = [[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"];
        
        self.priceValueLBL.text = [NSString stringWithFormat:@"$%.3f",[[[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"] floatValue]];
        self.totalValueLBL.text = [NSString stringWithFormat:@"$%.3f",[[[[quantityDataDictionary objectForKey:@"Deal_Information"]objectAtIndex:0]objectForKey:@"Price"] floatValue]* [self.grabItDealData.optionQuantity floatValue]];
        
    } else if (connection == connectionGiftDeal){
        
        giftDealDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataGiftDeal options:kNilOptions error:&error];
        NSLog(@"%@",giftDealDataDictionary);
        [ApplicationDelegate showAlertWithMessage:[giftDealDataDictionary objectForKey:@"Gift_Message"] title:@"Gift_Message"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (connection == connectionGrabDeal){
        
        grabDealDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataGrabDeal options:kNilOptions error:&error];
        NSLog(@"%@",grabDealDataDictionary);
        [ApplicationDelegate showAlertWithMessage:[grabDealDataDictionary objectForKey:@"Message"] title:@"Message"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }else if (connection == connectionGiftDealVoucher){
        
        giftVoucherDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataGiftVoucher options:kNilOptions error:&error];
        NSLog(@"%@",giftVoucherDataDictionary);
        [ApplicationDelegate showAlertWithMessage:[giftVoucherDataDictionary objectForKey:@"Message"] title:@"Message"];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    [ApplicationDelegate removeProgressHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

- (void) backBtnActnOverRide{
    GiftThisDealViewController * giftViewObj = [[GiftThisDealViewController alloc]initWithNibName:@"GiftThisDealViewController" bundle:nil];
    giftViewObj.dealId = self.grabItDealId;
    NSLog(@"%@",self.giftTo);
    
    giftViewObj.giftToTextData = self.giftTo;
    giftViewObj.giftFromTextData = self.giftFrom;
    giftViewObj.giftEmailTextData = self.giftEmail;
    giftViewObj.giftMessageTextData = self.giftMessage;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:giftViewObj animated:NO completion:nil];
    //[self.navigationController presentViewController:giftViewObj animated:NO completion:nil];
    //[self.navigationController pushViewController:giftViewObj animated:YES];
    
}

- (void) backBtnActnOverRideVoucher{
    
    BuyGiftVoucherDetailsViewController * buyGiftVoucherDetailsViewObj = [[BuyGiftVoucherDetailsViewController alloc]initWithNibName:@"BuyGiftVoucherDetailsViewController" bundle:nil];
    buyGiftVoucherDetailsViewObj.voucherID = self.grabItVoucherId;
    buyGiftVoucherDetailsViewObj.voucherAmount = self.grabItVoucherAmount;
    buyGiftVoucherDetailsViewObj.voucherQuantity = self.grabItQuantity;
    buyGiftVoucherDetailsViewObj.voucherAmountArray = self.voucherAmountArray;
    buyGiftVoucherDetailsViewObj.voucherIDArray = self.voucherIDArray;
    buyGiftVoucherDetailsViewObj.voucherQuantityArray = self.voucherQuantityArray;
    
    buyGiftVoucherDetailsViewObj.giftToTextData = self.giftTo;
    buyGiftVoucherDetailsViewObj.giftFromTextData = self.giftFrom;
    buyGiftVoucherDetailsViewObj.giftEmailTextData = self.giftEmail;
    buyGiftVoucherDetailsViewObj.giftMessageTextData = self.giftMessage;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    //transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:buyGiftVoucherDetailsViewObj animated:NO completion:nil];
    
    /*
     
    GiftThisDealViewController * giftViewObj = [[GiftThisDealViewController alloc]initWithNibName:@"GiftThisDealViewController" bundle:nil];
    giftViewObj.dealId = self.grabItDealId;
    NSLog(@"%@",self.giftTo);
    
    giftViewObj.giftToTextData = self.giftTo;
    giftViewObj.giftFromTextData = self.giftFrom;
    giftViewObj.giftEmailTextData = self.giftEmail;
    giftViewObj.giftMessageTextData = self.giftMessage;
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:giftViewObj animated:NO completion:nil];
    //[self.navigationController presentViewController:giftViewObj animated:NO completion:nil];
    //[self.navigationController pushViewController:giftViewObj animated:YES];
    */
}

- (IBAction)billingInformationBtnActn:(UIButton *)sender {
    BillingViewController * billingVc = [[BillingViewController alloc]initWithNibName:@"BillingViewController" bundle:nil];
    billingVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[billingVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:billingVc animated:NO];
    }
}

- (IBAction)grabItBtnActn:(UIButton *)sender {
    if ([self.quantityValueTxtView.text integerValue] > [self.grabItQuantity integerValue]) {
        [ApplicationDelegate showAlertWithMessage:[NSString stringWithFormat:@"Available quantity is %@",self.grabItQuantity] title:@"Sorry!!"];
    } else {
        if ([self.quantityValueTxtView.text isEqualToString:@"0"]) {
            [ApplicationDelegate showAlertWithMessage:@"Please enter a valid quantity" title:@"Sorry!!"];
        }
        else{
            [self purchaseGrabit];
        }
    }
}

- (void)purchaseGrabit{
    if ([self.grabItState isEqualToString:@"GrabItMain"]) {
        if ([ApplicationDelegate checkNetworkAvailability])
        {
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            userLoginStatus = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *request = [NSMutableURLRequest
                                            requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/grab_purchase"]];
            
            //        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
            //         [userLoginStatus objectForKey:@"UserId"], @"user_id",
            //                                     self.grabItDealId, @"deal_id",
            //                                     nil];
            
            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"3", @"user_id",
                                         self.grabItDealId, @"deal_id",
                                         self.quantityValueTxtView.text, @"quantity",
                                         nil];
            NSLog(@"%@",requestData);
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            connectionGrabDeal = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
            
        }
    } else if ([self.grabItState isEqualToString:@"Gift"]){
        if ([ApplicationDelegate checkNetworkAvailability])
        {
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            userLoginStatus = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *request = [NSMutableURLRequest
                                            requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/grab_gift_deal_purchase"]];
            
            //        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
            //         [userLoginStatus objectForKey:@"UserId"], @"user_id",
            //                                     self.grabItDealId, @"deal_id",
            //                                     nil];
            
            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"3", @"user_id",
                                         self.grabItDealId, @"deal_id",
                                         self.giftFrom, @"from",
                                         self.giftTo, @"to",
                                         self.giftMessage, @"personalmsg",
                                         self.giftEmail, @"email",
                                         self.quantityValueTxtView.text, @"quantity",
                                         nil];
            NSLog(@"%@",requestData);
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            connectionGiftDeal = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
            
        }
    }else if ([self.grabItState isEqualToString:@"Voucher"]){
        if ([ApplicationDelegate checkNetworkAvailability])
        {
            
            [ApplicationDelegate addProgressHUDToView:self.view];
            
            userLoginStatus = [NSUserDefaults standardUserDefaults];
            
            NSMutableURLRequest *request = [NSMutableURLRequest
                                            requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/by_gift_voucher"]];
            
            //        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
            //         [userLoginStatus objectForKey:@"UserId"], @"user_id",
            //                                     self.grabItDealId, @"deal_id",
            //                                     nil];
            
            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         self.grabItVoucherId, @"voucher_id",
                                         self.giftFrom, @"from",
                                         self.giftTo, @"to",
                                         self.giftMessage, @"gift_voucher_message",
                                         self.giftEmail, @"email_to",
                                         self.quantityValueTxtView.text, @"quantity",
                                         nil];
            NSLog(@"%@",requestData);
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            connectionGiftDealVoucher = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        }
        
        else
        {
            [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
            
        }
    }
}

- (IBAction)quantityChanged:(UITextField *)sender {
    self.totalValueLBL.text = [NSString stringWithFormat:@"$%.02f",([self.grabItRate floatValue] * [self.quantityValueTxtView.text floatValue])];
}

@end
