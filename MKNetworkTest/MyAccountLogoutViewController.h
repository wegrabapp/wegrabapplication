//
//  MyAccountLogoutViewController.h
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyAccountLogoutViewController : UIViewController{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
}
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)loginBtnActn:(UIButton *)sender;

@end
