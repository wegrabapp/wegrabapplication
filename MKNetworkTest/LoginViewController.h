//
//  LoginViewController.h
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate,NSURLConnectionDelegate>{
    NSUserDefaults * userLoginStatus;
}
@property (weak, nonatomic) IBOutlet UIScrollView *contentScrollView;

@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UIView *mainBackView;
@property (weak, nonatomic) IBOutlet UIView *buttonBackView;
@property (weak, nonatomic) IBOutlet UIView *contentLoginBackView;
@property (weak, nonatomic) IBOutlet UIView *contentSignUpBackView;
@property (weak, nonatomic) IBOutlet UIView *contentForgotBackView;

@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgetBtn;
@property (weak, nonatomic) IBOutlet UIButton *loginProceedBtn;
@property (weak, nonatomic) IBOutlet UIButton *signUpProceedBtn;
@property (weak, nonatomic) IBOutlet UIButton *forgotProceedBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UITextField *loginEmailId;
@property (weak, nonatomic) IBOutlet UITextField *loginPassword;
@property (weak, nonatomic) IBOutlet UITextField *signUpName;
@property (weak, nonatomic) IBOutlet UITextField *signUpEmailId;
@property (weak, nonatomic) IBOutlet UITextField *signUpNumber;
@property (weak, nonatomic) IBOutlet UITextField *signUpBirthDate;
@property (weak, nonatomic) IBOutlet UITextField *signUpPassword;
@property (weak, nonatomic) IBOutlet UITextField *signUpConfirmPassword;
@property (weak, nonatomic) IBOutlet UITextField *forgotEmailId;

@property (strong, nonatomic) NSMutableDictionary * loginDataDictionary;
@property (strong, nonatomic) NSMutableDictionary * signUpDataDictionary;
@property (strong, nonatomic) NSMutableDictionary * forgotDataDictionary;
@property (weak, nonatomic) IBOutlet UIButton *mobileRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *landRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *maleRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *femaleRadioBtn;
@property (weak, nonatomic) IBOutlet UIButton *tickBtn;

- (IBAction)logBttnActn:(UIButton *)sender;
- (IBAction)cancelBtnActn:(UIButton *)sender;

- (IBAction)loginBtnAction:(UIButton *)sender;
- (IBAction)signBtnAction:(UIButton *)sender;
- (IBAction)sendBtnActn:(UIButton *)sender;

- (IBAction)telRadioBtnActn:(UIButton *)sender;
- (IBAction)genderRadioBtnActn:(UIButton *)sender;
- (IBAction)tickBtnActn:(UIButton *)sender;

@end
