//
//  SectionInfoCities.h
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

//@class SectionViewMoreGrab;
@class SectionViewCities;
@class SectionView;
@class CategoryName;
//@class CountryDetails;
//@class FollowerDetails;
@class NewsLetterCityDetails;
@class CityDetails;

@interface SectionInfoCities : NSObject

@property (assign) BOOL open;
@property (strong) CategoryName *category;
//@property (strong) CountryDetails *country;
//@property (strong) FollowerDetails *follower;
@property (strong) CityDetails *citydetails;
@property (strong) NewsLetterCityDetails *newslettercitydetails;
@property (strong) SectionViewCities *sectionViewCities;
@property (strong) SectionView *sectionView;
//@property (strong) SectionViewMoreGrab *sectionViewMoreGrab;
@property (nonatomic,strong,readonly) NSMutableArray *rowHeights;

- (NSUInteger)countOfRowHeights;
- (id)objectInRowHeightsAtIndex:(NSUInteger)idx;
- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx;
- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx;
- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject;
- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes;
- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes;
- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray;

@end
