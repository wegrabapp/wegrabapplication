//
//  AppEngine.m
//  MKNetworkTest
//
//  Created by Sreeraj VR on 29/10/2014.
//  Copyright (c) 2014 mawaqaa. All rights reserved.
//


#import "AppEngine.h"

@implementation AppEngine


-(void)getCountryList:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    MKNetworkOperation *op = [self operationWithPath:kGet_Citilist_URL params:postDic httpMethod:@"GET"];
    //MKNetworkOperation *op = [self operationWithPath:kGet_Citilist_URL params:postDic httpMethod:@"POST"];
    //NSLog(@"%@",);
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
}

-(void)todaydealList:(NSDictionary *)postData CompletionHandler:(ResponseDictionaryBlock)responce errorHandler:(MKNKErrorBlock)errorBlock
{
    
    MKNetworkOperation *op = [self operationWithPath:kGet_TodayDealsList_URL params:postData httpMethod:@"POST"];
    NSMutableDictionary *header=[[NSMutableDictionary alloc] init];
    [header setValue:@"Application/Json" forKey:@"Content-Type"];
    [op addHeaders:header];
    
    [op setCustomPostDataEncodingHandler:^NSString *(NSDictionary *postDataDict) {
        
        return [postDataDict jsonEncodedKeyValueString];
        
    } forType:@"Application/Json"];
    
    
    [op addCompletionHandler:^(MKNetworkOperation *completedOperation) {
        
        [completedOperation responseJSONWithCompletionHandler:^(id jsonObject) {
            
            responce(jsonObject);
            
        }];
        
        
    } errorHandler:^(MKNetworkOperation *errorOp, NSError* error) {
        
        errorBlock(error);
    }];
    
    [self enqueueOperation:op];
    
    
}


@end
