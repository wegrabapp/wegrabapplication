//
//  SingleGrabItDealViewController.m
//  WeGrab
//
//  Created by Ronish on 2/15/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "SingleGrabItDealViewController.h"
#import "GiftThisDealViewController.h"
#import "ShareItViewController.h"
#import "HomeViewController.h"
#import "TodayDeal.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "UIColor+Hex.h"
#import "GrabItViewController.h"
#import "GrabItInnerViewController.h"
#import "GrabItDealOptionsData.h"

@interface SingleGrabItDealViewController (){
    NSTimer * countdownTimerFirst;
    NSTimer * countdownTimerSecond;
    long long remainingTicksFirst;
    long long remainingTicksSecond;
    NSString *First_FinePrint;
    NSString *First_AboutTheBusiness;
    NSString * Second_FinePrint;
    NSString * Second_AboutTheBusiness;
}

@end

@implementation SingleGrabItDealViewController

-(void)viewWillAppear:(BOOL)animated{
    userLocation = [NSUserDefaults standardUserDefaults];
    [HomeViewController sharedViewController].backBtn.hidden = YES;
    [HomeViewController sharedViewController].citiesBtn.hidden = NO;
    [HomeViewController sharedViewController].moreGrabsBtn.selected = NO;
    [HomeViewController sharedViewController].howItWorksBtn.selected = NO;
    [HomeViewController sharedViewController].myAccountBtn.selected = NO;
    [HomeViewController sharedViewController].cityLabel.text = [userLocation objectForKey:@"City"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.progressFirst.trackTintColor = [UIColor whiteColor];
    
    self.progressFirst.progressTintColor = [UIColor colorWithHex:0xffc300];
    
    
    //self.progressSecond.trackTintColor = [UIColor whiteColor];
    
    //self.progressSecond.progressTintColor = [UIColor colorWithHex:0xffc300];
    
    [self fetchDatafromServer];
    
    
    
    NSUserDefaults * userLocation = [NSUserDefaults standardUserDefaults];
    self.mainScrollFirst.contentSize = CGSizeMake(self.mainScrollFirst.frame.size.width, self.bottomViewFirst.frame.origin.y + self.bottomViewFirst.frame.size.height);
    //self.mainScrollSecond.contentSize = CGSizeMake(self.mainScrollSecond.frame.size.width, self.bottomViewSecond.frame.origin.y + self.bottomViewSecond.frame.size.height + 20);
    // Do any additional setup after loading the view from its nib.
    
    //    remainingTicks = 3666;
    //    [self updateLabel];
    //
    //    countdownTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self selector: @selector(handleTimerTick) userInfo: nil repeats: YES];
    //
    //     [[NSRunLoop currentRunLoop] addTimer:countdownTimer forMode:NSRunLoopCommonModes];
    
    /*
     
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
     
     */
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)giftItBtnActn:(UIButton *)sender {
    GiftThisDealViewController * giftViewObj = [[GiftThisDealViewController alloc]initWithNibName:@"GiftThisDealViewController" bundle:nil];
    //[self presentViewController:giftViewObj animated:YES completion:nil];
    //[self.navigationController presentViewController:giftViewObj animated:YES completion:^{
    
    //}];
    //    [self presentViewController:giftViewObj animated:YES completion:nil];
    
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:giftViewObj animated:NO completion:nil];
    //[self.navigationController pushViewController:giftViewObj animated:YES];
}

- (IBAction)shareItBtnActn:(UIButton *)sender {
    ShareItViewController * shareItViewObj = [[ShareItViewController alloc]initWithNibName:@"ShareItViewController" bundle:nil];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self.view.window.rootViewController presentViewController:shareItViewObj animated:NO completion:nil];
}


-(void)handleTimerTickFirst
{
    remainingTicksFirst--;
    [self updateLabelFirst];
    
    if (remainingTicksFirst <= 0) {
        [countdownTimerFirst invalidate];
        countdownTimerFirst = nil;
    }
}

-(void)handleTimerTickSecond
{
    remainingTicksSecond--;
    [self updateLabelSecond];
    
    if (remainingTicksSecond <= 0) {
        [countdownTimerSecond invalidate];
        countdownTimerSecond = nil;
    }
}


-(void)updateLabelFirst
{
    
    long long forHours = remainingTicksFirst / 3600;
    long long remainder = remainingTicksFirst % 3600;
    long long forMinutes = remainder / 60;
    long long forSeconds = remainder % 60;
    
    
    self.SecondsLabelFirst.text = [NSString stringWithFormat:@"%qi",forSeconds];
    
    self.MinutesLabelFirst.text= [NSString stringWithFormat:@"%qi",forMinutes];
    
    self.HoursLabelFirst.text= [NSString stringWithFormat:@"%qi",forHours];
    
}

-(void)updateLabelSecond
{
    
    
    long long forHours = remainingTicksSecond / 3600;
    long long remainder = remainingTicksSecond % 3600;
    long long forMinutes = remainder / 60;
    long long forSeconds = remainder % 60;
    
    //self.SecondsLabelSecond.text = [NSString stringWithFormat:@"%qi",forSeconds];
    
    //self.MinutesLabelSecond.text= [NSString stringWithFormat:@"%qi",forMinutes];
    
    //self.HoursLabelSecond.text = [NSString stringWithFormat:@"%qi",forHours];
    
    // self.HoursLabelSecond.text= [[NSNumber numberWithUnsignedInt: forHours] stringValue];
}


-(void)fetchDatafromServer
{
    
    /*    NSMutableDictionary *postData2 = [[NSMutableDictionary alloc] initWithDictionary:[self getPostData]];
     
     NSLog(@"%@",postData2);
     
     [ApplicationDelegate.engine todaydealList:postData2 CompletionHandler:^(NSMutableDictionary *responseDictionary) {
     
     NSLog(@"%@",responseDictionary);
     
     
     } errorHandler:^(NSError *error) {
     
     }];*/
    
    
    /////////////////////////// nsurl /////////////////////////
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request ;
        
        //            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        //                                         @"112", @"country_id",
        //                                         @"6", @"city_id",
        //                                         nil];
        
        NSDictionary *requestData;
        
        userLocation = [NSUserDefaults standardUserDefaults];
        NSLog(@"%@",[userLocation objectForKey:@"country_id"]);
        NSLog(@"%@",[userLocation objectForKey:@"city_id"]);
        
        if (([userLocation objectForKey:@"country_id"] == nil) || ([[userLocation objectForKey:@"country_id"]isEqual:[NSNull null]])) {
            NSLog(@"Empty");
            
            request = [NSMutableURLRequest
                       requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/location_country_city_deals"]];
            
            //            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
            //                           [userLocation objectForKey:@"Country"], @"CountryName",
            //                           [userLocation objectForKey:@"City"], @"city_name",
            //                           nil];
            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                           @"Kuwait", @"CountryName",
                           @"Sharaq", @"city_name",
                           nil];
            
        } else {
            
            request = [NSMutableURLRequest
                       requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/todaydeal"]];
            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [userLocation objectForKey:@"country_id"], @"country_id",
                           [userLocation objectForKey:@"city_id"], @"city_id",
                           nil];
        }
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionTodaysDeal = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
    }
    /////////////////////////// nsurl /////////////////////////
    
}

-(NSMutableDictionary*) getPostData
{
    NSMutableDictionary *postDic = [[NSMutableDictionary alloc] init];
    
    [postDic setObject:@"112" forKey:@"country_id"];
    
    [postDic setObject:@"6" forKey:@"city_id"];
    
    return postDic;
}

////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionTodaysDeal) {
        responseDataTodaysDeal = [[NSMutableData alloc] init];
        self.todayDealDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseDataGrabit = [[NSMutableData alloc] init];
        self.grabItDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == connectionTodaysDeal) {
        [responseDataTodaysDeal appendData:data];
    } else {
        [responseDataGrabit appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == connectionTodaysDeal) {
        
        NSError* error;
        self.todayDealDictionary = [NSJSONSerialization JSONObjectWithData:responseDataTodaysDeal options:kNilOptions error:&error];
        if ([ApplicationDelegate isValid:[self.todayDealDictionary objectForKey:@"Deals"]])
        {
            
            if ([[self.todayDealDictionary objectForKey:@"Deals"]count]>0) {
                self.firstMainView.hidden = NO;
                if ([ApplicationDelegate isValid:[[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:0]]) {
                    NSMutableDictionary * firstData = [[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:0];
                    
                    TodayDeal *dealObj = [ApplicationDelegate.mapper getTodayDealListFromDictionary:firstData];
                    
                    NSURL * imageURL = [[NSURL alloc]init];
                    
                    imageURL = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dealObj.DealImage]];
                    
                    [self.dealImageFirst sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"loading.png"]];
                    
                    First_FinePrint=dealObj.FinePrint;
                    
                    First_AboutTheBusiness=dealObj.AboutTheBusiness;
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            [self.webViewDescFirst loadHTMLString:First_FinePrint baseURL:nil];
                            
                            self.progressFirst.progress=[dealObj.CurrentBuyerNo floatValue]/[dealObj.MaximumBuyerNo floatValue];
                            
                        });
                    });
                    self.dealFirstID = [[NSString alloc]init];
                    self.dealFirstID = dealObj.DealID;
                    self.dealHeadingFirst.text = dealObj.DealHeadline;
                    [self.dealDiscriptionFirst setValue:dealObj.DealDescription forKey:@"contentToHTMLString"];
                    self.orginalPriceFirst.text = [NSString stringWithFormat:@"$ %@",dealObj.OriginalPrice];
                    self.offerPriceFirst.text = [NSString stringWithFormat:@"$ %@",dealObj.OfferPrice];
                    self.discountLblFirst.text = [NSString stringWithFormat:@"%@ off",dealObj.Discount];
                    self.countFirst.text = dealObj.MaximumBuyerNo;
                    self.extradiscountFirst.text = [NSString stringWithFormat:@"%@%%",dealObj.ExtraDiscount];
                    self.minimumNumberFirst.text = dealObj.MinimumNoofBuyer;
                    self.maximumNumberFirst.text = dealObj.MaximumBuyerNo;
                    self.soldCountFirst.text = dealObj.SoldDealsNumber;
                    self.broughtCountFirst.text = [NSString stringWithFormat:@"%@ brought",dealObj.CurrentBuyerNo];
                    self.remainingCountFirst.text = [NSString stringWithFormat:@"%d more need to buy for deal to be done",(([dealObj.MaximumBuyerNo integerValue]) - ([dealObj.CurrentBuyerNo integerValue]))];
                    
                    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                    
                    NSNumber *number = [formatter numberFromString: dealObj.Timer];
                    remainingTicksFirst = [number longLongValue]/10;
                    NSLog(@"FIRST Timer%qi",remainingTicksFirst);
                    
                    [self updateLabelFirst];
                    
                    countdownTimerFirst = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self selector: @selector(handleTimerTickFirst) userInfo: nil repeats: YES];
                    
                    [[NSRunLoop currentRunLoop] addTimer:countdownTimerFirst forMode:NSRunLoopCommonModes];
                    
                    NSLog(@"%@",dealObj.DealImage);
                }
                if ([ApplicationDelegate isValid:[[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:1]]) {
                    NSMutableDictionary * secondData = [[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:1];
                    TodayDeal *dealObj = [ApplicationDelegate.mapper getTodayDealListFromDictionary:secondData];
                    
                    NSURL * imageURL = [[NSURL alloc]init];
                    
                    imageURL = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:dealObj.DealImage]];
                    
                    //[self.dealImageSecond sd_setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"loading.png"]];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
                        
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            //[self.webViewDescSecond loadHTMLString:Second_FinePrint baseURL:nil];
                            
                            NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
                            
                            NSNumber *number = [formatter numberFromString: dealObj.Timer];
                            remainingTicksSecond = [number longLongValue]/10;
                            NSLog(@"Second Timer%qi",remainingTicksSecond);
                            
                            //self.progressSecond.progress=[dealObj.CurrentBuyerNo floatValue]/[dealObj.MaximumBuyerNo floatValue];
                            
                            [self updateLabelSecond];
                            
                            countdownTimerSecond = [NSTimer scheduledTimerWithTimeInterval: 1.0 target: self selector: @selector(handleTimerTickSecond) userInfo: nil repeats: YES];
                            
                            [[NSRunLoop currentRunLoop] addTimer:countdownTimerSecond forMode:NSRunLoopCommonModes];
                            
                        });
                    });
                    
                    Second_FinePrint=dealObj.FinePrint;
                    
                    Second_AboutTheBusiness=dealObj.AboutTheBusiness;
                    //self.dealSecondID = [[NSString alloc]init];
                    //self.dealSecondID = dealObj.DealID;
                    //self.dealHeadingSecond.text = dealObj.DealHeadline;
                    //[self.dealDiscriptionSecond setValue:dealObj.DealDescription forKey:@"contentToHTMLString"];
                    //self.orginalPriceSecond.text = [NSString stringWithFormat:@"$ %@",dealObj.OriginalPrice];
                    //self.offerPriceSecond.text = [NSString stringWithFormat:@"$ %@",dealObj.OfferPrice];
                    //self.discountLblSecond.text = [NSString stringWithFormat:@"%@ off",dealObj.Discount];
                    //self.countSecond.text = dealObj.MaximumBuyerNo;
                    //self.extradiscountSecond.text = [NSString stringWithFormat:@"%@%%",dealObj.ExtraDiscount];
                    //self.minimumNumberSecond.text = dealObj.MinimumNoofBuyer;
                    //self.maximumNumberSecond.text = dealObj.MaximumBuyerNo;
                    //self.soldCountSecond.text = dealObj.SoldDealsNumber;
                    //self.broughtCountSecond.text = [NSString stringWithFormat:@"%@ brought",dealObj.CurrentBuyerNo];
                    //self.remainingCountSecond.text = [NSString stringWithFormat:@"%ld more need to buy for deal to be done",(([dealObj.MaximumBuyerNo integerValue]) - ([dealObj.CurrentBuyerNo integerValue]))];
                    
                    NSLog(@"%@",dealObj.DealImage);
                }
            }
        }
    }
    else {
        NSError* error;
        self.grabItDictionary = [NSJSONSerialization JSONObjectWithData:responseDataGrabit options:kNilOptions error:&error];
        //NSLog(@"%@",self.grabItDictionary);
        if ([ApplicationDelegate isValid:[self.grabItDictionary objectForKey:@"DealItem"]])
        {
            self.grabItDealOptionDetailArray = [[NSMutableArray alloc]init];
            if ([[self.grabItDictionary objectForKey:@"DealItem"] count] > 1) {
                for (int i=0; i<[[self.grabItDictionary objectForKey:@"DealItem"] count]; i++) {
                    NSMutableDictionary * tempDictionary = [[NSMutableDictionary alloc]initWithDictionary:[[self.grabItDictionary objectForKey:@"DealItem"] objectAtIndex:i]];
                    GrabItDealOptionsData *grabItDealOptionDetails = [[GrabItDealOptionsData alloc] init];
                    grabItDealOptionDetails = [ApplicationDelegate.mapper getGrabItOptionsFromDictionary:tempDictionary];
                    [self.grabItDealOptionDetailArray addObject:grabItDealOptionDetails];
                }
                NSLog(@"%@",self.grabItDealOptionDetailArray);
                GrabItViewController *grabItViewObj = [[GrabItViewController alloc] initWithNibName:@"GrabItViewController" bundle:nil];
                grabItViewObj.grabItDealOptionDetailArray = self.grabItDealOptionDetailArray;
                grabItViewObj.view.backgroundColor = [UIColor clearColor];
                if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[grabItViewObj class]])
                {
                    [ApplicationDelegate.homeTabNav pushViewController:grabItViewObj animated:NO];
                }
                
            } else if ([[self.grabItDictionary objectForKey:@"DealItem"] count] == 1){
                
                NSMutableDictionary * tempDictionary = [[NSMutableDictionary alloc]initWithDictionary:[[self.grabItDictionary objectForKey:@"DealItem"] objectAtIndex:0]];
                GrabItDealOptionsData *grabItDealOptionDetails = [[GrabItDealOptionsData alloc] init];
                grabItDealOptionDetails = [ApplicationDelegate.mapper getGrabItOptionsFromDictionary:tempDictionary];
                
                GrabItInnerViewController *grabItInnerViewObj = [[GrabItInnerViewController alloc] initWithNibName:@"GrabItInnerViewController" bundle:nil];
                grabItInnerViewObj.grabItDealData = grabItDealOptionDetails;
                grabItInnerViewObj.view.backgroundColor = [UIColor clearColor];
                if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[grabItInnerViewObj class]])
                {
                    [ApplicationDelegate.homeTabNav pushViewController:grabItInnerViewObj animated:NO];
                }
            }
        }
    }
    [ApplicationDelegate removeProgressHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    self.firstMainView.hidden = YES;
    //self.secondMainView.hidden = NO;
    //Do what you want here
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    self.firstMainView.hidden = NO;
    //self.secondMainView.hidden = YES;
    //Do what you want here
}

- (IBAction)buttonClicked:(id)sender {
    
    
    UIButton *button = (UIButton *)sender;
    
    
    if(button.tag==0)
    {
        [button setImage:[UIImage imageNamed:@"fineprint_sel.png"] forState:UIControlStateNormal];
        [self.AboutButton setImage:[UIImage imageNamed:@"aboutthebussiness.png"] forState:UIControlStateNormal];
        [self.webViewDescFirst loadHTMLString:First_FinePrint baseURL:nil];
    }
    else if(button.tag==1)
    {
        [button setImage:[UIImage imageNamed:@"aboutthebussiness_sel.png"] forState:UIControlStateNormal];
        [self.FinePrintButton setImage:[UIImage imageNamed:@"fineprint.png"] forState:UIControlStateNormal];
        [self.webViewDescFirst loadHTMLString:First_AboutTheBusiness baseURL:nil];
    }
    else if(button.tag==2)
    {
        [button setImage:[UIImage imageNamed:@"fineprint_sel.png"] forState:UIControlStateNormal];
        //[self.AboutButtonSecond setImage:[UIImage imageNamed:@"aboutthebussiness.png"] forState:UIControlStateNormal];
        //[self.webViewDescSecond loadHTMLString:Second_FinePrint baseURL:nil];
    }
    
    else
    {
        [button setImage:[UIImage imageNamed:@"aboutthebussiness_sel.png"] forState:UIControlStateNormal];
        //[self.FinePrintButtonSecond setImage:[UIImage imageNamed:@"fineprint.png"] forState:UIControlStateNormal];
        //[self.webViewDescSecond loadHTMLString:Second_AboutTheBusiness baseURL:nil];
    }
    
    
}

- (IBAction)grabitBtnActn:(UIButton *)sender {
    NSLog(@"%ld",(long)sender.tag);
    NSString * grabId = [[NSString alloc]init];
    if (sender.tag == 1) {
        grabId = self.dealFirstID;
    } else {
        //grabId = self.dealSecondID;
    }
    //grabId = @"38";
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request ;
        NSDictionary *requestData;
        
        if ([ApplicationDelegate isValid:grabId]) {
            request = [NSMutableURLRequest
                       requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/grab_deal"]];
            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                           grabId, @"deal_id",nil];
        }
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionGrabit = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [ApplicationDelegate removeProgressHUD];
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
    }
}

@end
