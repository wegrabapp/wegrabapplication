//
//  CountryLocationViewController.m
//  WeGrab
//
//  Created by satheesh on 1/4/16.
//  Copyright (c) 2016 Mawaqaa. All rights reserved.
//

#import "CountryLocationViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeViewController.h"
#import "CountryDetails.h"
#import "SectionInfo.h"

@interface CountryLocationViewController (){
    HomeViewController * homePageViewObj;
}

@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * countryList;
- (void) setCategoryArray;

@end

@implementation CountryLocationViewController

@synthesize countryList;
@synthesize openSectionIndex;
@synthesize sectionInfoArray;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[self getCountryName];
    [self getCountryData];
    NSLog(@"%f",self.view.frame.size.height);
}

-(void)getCountryData{
    /////////////////////////// nsurl /////////////////////////
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
    
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    NSMutableURLRequest *request = [NSMutableURLRequest
                                    requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/cities"]];
    
//    NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                 @"112", @"country_id",
//                                 @"6", @"city_id",
//                                 nil];
    NSError *error;
//    NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    //[request setHTTPBody:postData];
    NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
         [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
    
    /////////////////////////// nsurl /////////////////////////
}

-(void)getCountryName
{
    [ApplicationDelegate addProgressHUDToView:self.view];
    
    [ApplicationDelegate.engine getCountryList:^(NSMutableDictionary *response)
     {
         {
             NSLog(@"1......%@",response);
             countryDataDictionary = [[NSMutableDictionary alloc]init];
             NSLog(@"2.....%@",[response objectForKey:@"countries"]);
             
             [countryDataDictionary setObject:[response objectForKey:@"countries"] forKey:@"countries"];
             NSLog(@"3........%lu",(unsigned long)[[countryDataDictionary objectForKey:@"countries"] count]);
             
             if ([[countryDataDictionary objectForKey:@"countries"] count] > 0) {
                 
                 NSMutableArray *countryArray = [[NSMutableArray alloc] initWithCapacity:[[countryDataDictionary objectForKey:@"countries"] count]];
                 for (NSDictionary *dictionary in [countryDataDictionary objectForKey:@"countries"]) {
                     CountryDetails *countryDetails = [[CountryDetails alloc] init];
                     countryDetails.country_id = [dictionary objectForKey:@"country_id"];
                     countryDetails.countryName = [dictionary objectForKey:@"CountryName"];
                     countryDetails.cities = [dictionary objectForKey:@"cities"];
                     countryDetails.city_id = [dictionary objectForKey:@"city_id"];
                     [countryArray addObject:countryDetails];
                 }
                 self.countryList = countryArray;
                 
                 if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.countryTableView])) {
                     NSMutableArray *array = [[NSMutableArray alloc] init];
                     
                     for (CountryDetails *country in self.countryList) {
                         SectionInfo *section = [[SectionInfo alloc] init];
                         section.country = country;
                         section.open = NO;
                         
                         NSNumber *defaultHeight = [NSNumber numberWithInt:44];
                         NSInteger count = [[section.country cities] count];
                         
                         for (NSInteger i= 0; i<count; i++) {
                             NSLog(@"4......%d count...",count);
                             NSLog(@"5.....%@",[section.country countryName]);
                             [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                             NSLog(@"6......%@",section);
                         }
                         
                         [array addObject:section];
                     }
                     //NSLog(@"%@",array);
                     self.sectionInfoArray = array;
                 }
                 
                 [self.countryTableView reloadData];
                 [ApplicationDelegate removeProgressHUD];
             }
         }
     } errorHandler:^(NSError *error) {
         
         
         
     }];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([self.locationString isEqualToString:@"Initial"]) {
        self.cancelBtn.hidden = YES;
    } else if ([self.locationString isEqualToString:@"NotLoged"]){
        userLocation = [NSUserDefaults standardUserDefaults];
        //(sender!=nil)&&(![sender isEqual:[NSNull null]])
        if (([userLocation objectForKey:@"City"] == nil) || ([[userLocation objectForKey:@"City"]isEqual:[NSNull null]])) {
            NSLog(@"Empty");
            
        } else {
            //NSLog(@"%@",[userLocation objectForKey:@"City"]);
            self.cityLabel.text = [userLocation objectForKey:@"City"];
        }
        self.cancelBtn.hidden = NO;
        self.labelBackView.hidden = NO;
        CGRect tempFrame = self.countryTableView.frame;
        self.countryTableView.frame = CGRectMake(tempFrame.origin.x, tempFrame.origin.y+(self.labelBackView.frame.size.height - 8), tempFrame.size.width, tempFrame.size.height-(self.labelBackView.frame.size.height - 8));
    }
    self.countryTableView.sectionHeaderHeight = 45;
    self.countryTableView.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    
    NSLog(@"7......%ld",(unsigned long)[self.countryList count]);
    return [self.countryList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:section];

    NSInteger rows = [[array.country cities] count];
    NSLog(@"row count...%d",rows);
    return (array.open) ? rows : 0;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        static NSMutableArray *colors = nil;
        if (colors == nil) {
            colors = [[NSMutableArray alloc] initWithCapacity:3];
            UIColor *color = nil;
            color = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
            [colors addObject:(id)[color CGColor]];
            color = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1];
            [colors addObject:(id)[color CGColor]];
        }
        //[(CAGradientLayer *)cell.layer setColors:colors];
        //[(CAGradientLayer *)cell.layer setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
        CAGradientLayer *gradient = [CAGradientLayer layer];
        CGRect frame = cell.bounds;
        frame.size.width = [[UIScreen mainScreen]bounds].size.width;
        gradient.frame = frame;
        [gradient setColors:colors];
        [gradient setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
        [cell.layer addSublayer:gradient];
        
    }
    //cell.backgroundColor = [UIColor blackColor];
    
    
    cell.textLabel.textColor = [UIColor whiteColor];
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:indexPath.section];
    NSLog(@"country name ... %@",country.countryName);
    cell.textLabel.text = [[country.cities objectAtIndex:indexPath.row] objectForKey:@"city_name"];
    //[cell.textLabel bringSubviewToFront:self.view];
    [cell addSubview:cell.textLabel];
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfo *array  = [self.sectionInfoArray objectAtIndex:section];
    if (!array.sectionView)
    {
//        NSString *title = array.category.name;
        NSString *title = array.country.countryName;
        array.sectionView = [[SectionView alloc] initWithFrame:CGRectMake(0, 0, self.countryTableView.bounds.size.width, 45) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionView;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"8.....%ld",(long)indexPath.section);
    NSLog(@"9....%ld",(long)indexPath.row);
    
    NSLog(@"99...%@",self.navigationController);
    
    SectionInfo *array  = [self.sectionInfoArray objectAtIndex:indexPath.section];
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:indexPath.section];
    
    userLocation = [NSUserDefaults standardUserDefaults];
    [userLocation setObject:array.country.countryName forKey:@"Country"];
    [userLocation setObject:array.country.country_id forKey:@"country_id"];
    [userLocation setObject:[[country.cities objectAtIndex:indexPath.row] objectForKey:@"city_name"] forKey:@"City"];
    [userLocation setObject:[[country.cities objectAtIndex:indexPath.row] objectForKey:@"city_id"] forKey:@"city_id"];
    [userLocation synchronize];
    NSLog(@"%@",[userLocation objectForKey:@"country_id"]);
    NSLog(@"%@",[userLocation objectForKey:@"city_id"]);
    homePageViewObj = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
    //[self.navigationController pushViewController:homePageViewObj animated:NO];
    if (!(self.navigationController)) {
        
        //UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:homePageViewObj];
        
        //self.view.window.rootViewController = navigation;
        [self presentViewController:homePageViewObj animated:NO completion:nil];
        //[self.view.window.rootViewController presentViewController:homePageViewObj animated:YES completion:nil];
    }
    else{
        [self.navigationController pushViewController:homePageViewObj animated:NO];
    }
}

- (void) sectionClosed : (NSInteger) section{
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.countryTableView numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.countryTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
}

- (void) sectionOpened : (NSInteger) section
{
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:section];
    
    array.open = YES;
    NSInteger count = [array.country.cities count];
    NSLog(@"11.....%d",count);
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    if (previousOpenIndex != NSNotFound)
    {
        SectionInfo *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        sectionArray.open = NO;
//        NSInteger counts = [sectionArray.category.list count];
        NSInteger counts = [sectionArray.country.cities count];
        [sectionArray.sectionView toggleButtonPressed:FALSE];
        for (NSInteger i = 0; i<counts; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.countryTableView beginUpdates];
    [self.countryTableView insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
    [self.countryTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.countryTableView endUpdates];
    self.openSectionIndex = section;
    
}

 
////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
    countryDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    //NSLog(@"%@",_responseData);
    //NSLog(@"response data - %@", [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding]);
    NSError* error;
    //countryDataDictionary = [[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error]objectForKey:@"countries"];
    [countryDataDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error]objectForKey:@"countries"] forKey:@"countries"];
    NSLog(@"%@",countryDataDictionary);
    
    if ([[countryDataDictionary objectForKey:@"countries"] count] > 0) {
        
        NSMutableArray *countryArray = [[NSMutableArray alloc] initWithCapacity:[[countryDataDictionary objectForKey:@"countries"] count]];
        for (NSDictionary *dictionary in [countryDataDictionary objectForKey:@"countries"]) {
            CountryDetails *countryDetails = [[CountryDetails alloc] init];
            countryDetails.country_id = [dictionary objectForKey:@"country_id"];
            countryDetails.countryName = [dictionary objectForKey:@"CountryName"];
            countryDetails.cities = [dictionary objectForKey:@"cities"];
            //countryDetails.city_id = [dictionary objectForKey:@"city_id"];
            //NSLog(@"%@",countryDetails.city_id);
            //NSLog(@"%@",dictionary);
            [countryArray addObject:countryDetails];
        }
        self.countryList = countryArray;
        
        if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.countryTableView])) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (CountryDetails *country in self.countryList) {
                SectionInfo *section = [[SectionInfo alloc] init];
                section.country = country;
                section.open = NO;
                
                NSNumber *defaultHeight = [NSNumber numberWithInt:44];
                NSInteger count = [[section.country cities] count];
                
                for (NSInteger i= 0; i<count; i++) {
                    NSLog(@"4......%d count...",count);
                    NSLog(@"5.....%@",[section.country countryName]);
                    [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                    NSLog(@"6......%@",section);
                }
                
                [array addObject:section];
            }
            //NSLog(@"%@",array);
            self.sectionInfoArray = array;
        }
        
        [self.countryTableView reloadData];
        [ApplicationDelegate removeProgressHUD];
    }
    
    //NSLog(@"%@",[self.todayDealDictionary objectForKey:@"Deals"]);
    //NSLog(@"%@",[[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:0]);
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////



- (IBAction)cancelBtnActn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
