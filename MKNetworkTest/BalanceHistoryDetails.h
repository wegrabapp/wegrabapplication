//
//  BalanceHistoryDetails.h
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BalanceHistoryDetails : NSObject
@property (nonatomic, retain) NSString * transactionDate;
@property (nonatomic, retain) NSString * expiryDate;
@property (nonatomic, retain) NSString * transactionType;
@property (nonatomic, retain) NSString * amount;
@property (nonatomic, retain) NSString * status;
@end
