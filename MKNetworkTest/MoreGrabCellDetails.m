//
//  MoreGrabCellDetails.m
//  WeGrab
//
//  Created by satheesh on 1/9/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MoreGrabCellDetails.h"

@implementation MoreGrabCellDetails

@synthesize dealID;
@synthesize dealImage;
@synthesize dealDescription;
@synthesize cityName;
@synthesize countryName;
@synthesize title;
@synthesize oldPrice;
@synthesize currentPrice;

@end
