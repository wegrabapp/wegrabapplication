//
//  ObjectMapper.m
//  WeGrab
//
//  Created by Sreeraj VR on 04/01/2016.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "ObjectMapper.h"

@implementation ObjectMapper

-(TodayDeal *)getTodayDealListFromDictionary:(NSMutableDictionary *)TodayDealDictionary
{
  
     TodayDeal  * dealObj = [[TodayDeal alloc] init];
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"DealId"]])
    {
        dealObj.DealID = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"DealId"]];
    }
    else
    {
        dealObj.DealID = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"DealImage"]])
    {
        dealObj.DealImage = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"DealImage"]];
    }
    else
    {
        dealObj.DealImage = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"DealHeadline"]])
    {
        dealObj.DealHeadline = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"DealHeadline"]];
    }
    else
    {
        dealObj.DealHeadline = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"DealDescription"]])
    {
        dealObj.DealDescription = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"DealDescription"]];
    }
    else
    {
        dealObj.DealDescription = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"OriginalPrice"]])
    {
        dealObj.OriginalPrice = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"OriginalPrice"]];
    }
    else
    {
        dealObj.OriginalPrice = @"";
    }
    
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"OfferPrice"]])
    {
        dealObj.OfferPrice = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"OfferPrice"]];
    }
    else
    {
        dealObj.OfferPrice = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Discount"]])
    {
        dealObj.Discount = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Discount"]];
    }
    else
    {
        dealObj.Discount = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"ExtraDiscount"]])
    {
        dealObj.ExtraDiscount = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"ExtraDiscount"]];
    }
    else
    {
        dealObj.ExtraDiscount = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Minimum_No_of_Buyer"]])
    {
        dealObj.MinimumNoofBuyer = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Minimum_No_of_Buyer"]];
    }
    else
    {
        dealObj.MinimumNoofBuyer = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Maximum_Buyer_No"]])
    {
        dealObj.MaximumBuyerNo = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Maximum_Buyer_No"]];
    }
    else
    {
        dealObj.MaximumBuyerNo = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Timer"]])
    {
        dealObj.Timer = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Timer"]];
    }
    else
    {
        dealObj.Timer = @"";
    }
    
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"About_The_Business"]])
    {
        dealObj.AboutTheBusiness = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"About_The_Business"]];
    }
    else
    {
        dealObj.AboutTheBusiness = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Fine_Print"]])
    {
        dealObj.FinePrint = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Fine_Print"]];
    }
    else
    {
        dealObj.FinePrint = @"";
    }
    
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"SoldDealsNumber"]])
    {
        dealObj.SoldDealsNumber = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"SoldDealsNumber"]];
    }
    else
    {
        dealObj.SoldDealsNumber = @"";
    }
    if ([ApplicationDelegate isValid:[TodayDealDictionary objectForKey:@"Current_Buyer_No"]])
    {
        dealObj.CurrentBuyerNo = [NSString stringWithFormat:@"%@",[TodayDealDictionary objectForKey:@"Current_Buyer_No"]];
    }
    else
    {
       dealObj.CurrentBuyerNo = @"";
    }
    return dealObj;
}


-(MoreGrabCellDetails *)getMoreGrabDealListFromDictionary:(NSMutableDictionary *)MoreDealDictionary
{
    
    MoreGrabCellDetails  * moreGrabDealObj = [[MoreGrabCellDetails alloc] init];
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"deal_id"]])
    {
        moreGrabDealObj.dealID = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"deal_id"]];
    }
    else
    {
        moreGrabDealObj.dealID = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"deal_image"]])
    {
        moreGrabDealObj.dealImage = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"deal_image"]];
    }
    else
    {
        moreGrabDealObj.dealImage = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"title"]])
    {
        moreGrabDealObj.title = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"title"]];
    }
    else
    {
        moreGrabDealObj.title = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"description"]])
    {
        moreGrabDealObj.dealDescription = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"description"]];
    }
    else
    {
        moreGrabDealObj.dealDescription = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"city_name"]])
    {
        moreGrabDealObj.cityName = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"city_name"]];
    }
    else
    {
        moreGrabDealObj.cityName = @"";
    }
    
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"country_name"]])
    {
        moreGrabDealObj.countryName = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"country_name"]];
    }
    else
    {
        moreGrabDealObj.countryName = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"old_price"]])
    {
        moreGrabDealObj.oldPrice = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"old_price"]];
    }
    else
    {
        moreGrabDealObj.oldPrice = @"";
    }
    
    if ([ApplicationDelegate isValid:[MoreDealDictionary objectForKey:@"current_price"]])
    {
        moreGrabDealObj.currentPrice = [NSString stringWithFormat:@"%@",[MoreDealDictionary objectForKey:@"current_price"]];
    }
    else
    {
        moreGrabDealObj.currentPrice = @"";
    }
    
    return moreGrabDealObj;
}

-(GrabItDealOptionsData *)getGrabItOptionsFromDictionary:(NSMutableDictionary *)GrabItOptionDictionary
{
    
    GrabItDealOptionsData  * grabItDealOptionObj = [[GrabItDealOptionsData alloc] init];
    
    if ([ApplicationDelegate isValid:[GrabItOptionDictionary objectForKey:@"ID"]])
    {
        grabItDealOptionObj.optionID = [NSString stringWithFormat:@"%@",[GrabItOptionDictionary objectForKey:@"ID"]];
    }
    else
    {
        grabItDealOptionObj.optionID = @"";
    }
    
    if ([ApplicationDelegate isValid:[GrabItOptionDictionary objectForKey:@"Name"]])
    {
        grabItDealOptionObj.optionName = [NSString stringWithFormat:@"%@",[GrabItOptionDictionary objectForKey:@"Name"]];
    }
    else
    {
        grabItDealOptionObj.optionName = @"";
    }
    
    if ([ApplicationDelegate isValid:[GrabItOptionDictionary objectForKey:@"Price"]])
    {
        grabItDealOptionObj.optionPrice = [NSString stringWithFormat:@"%@",[GrabItOptionDictionary objectForKey:@"Price"]];
    }
    else
    {
        grabItDealOptionObj.optionPrice = @"";
    }
    
    if ([ApplicationDelegate isValid:[GrabItOptionDictionary objectForKey:@"quantity"]])
    {
        grabItDealOptionObj.optionQuantity = [NSString stringWithFormat:@"%@",[GrabItOptionDictionary objectForKey:@"quantity"]];
    }
    else
    {
        grabItDealOptionObj.optionQuantity = @"";
    }
    
    return grabItDealOptionObj;
}
@end
