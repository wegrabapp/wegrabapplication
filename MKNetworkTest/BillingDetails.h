//
//  BillingDetails.h
//  WeGrab
//
//  Created by Ronish on 2/8/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BillingDetails : NSObject

@property (nonatomic, retain) NSString * card_No;
@property (nonatomic, retain) NSString * card_Id;
@property (nonatomic, retain) NSString * default_Card;
@property (nonatomic, retain) NSMutableDictionary * card_Details;
@end
