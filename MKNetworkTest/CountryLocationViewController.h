//
//  CountryLocationViewController.h
//  WeGrab
//
//  Created by satheesh on 1/4/16.
//  Copyright (c) 2016 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SectionView.h"

@interface CountryLocationViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SectionView>{
    NSMutableDictionary * countryDataDictionary;
    NSUserDefaults * userLocation;
    NSMutableData * responseData;
}
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UITableView *countryTableView;
@property (weak, nonatomic) NSString * locationString;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UIView *labelBackView;
- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
