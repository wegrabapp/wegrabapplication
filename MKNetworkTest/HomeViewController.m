//
//  HomeViewController.m
//  WeGrab
//
//  Created by satheesh on 1/4/16.
//  Copyright (c) 2016 Mawaqaa. All rights reserved.
//

#import "HomeViewController.h"
#import "GrabItDealPageViewController.h"
#import "CountryLocationViewController.h"
#import "MyAccountLoginViewController.h"
#import "MyAccountLogoutViewController.h"
#import "MoreGrabViewController.h"
#import "HowItsWorksViewController.h"
#import "FollewersViewController.h"

static HomeViewController *sharedObj = NULL;

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize homeTabContainerView;

-(void)viewWillAppear:(BOOL)animated{
    userLocation = [NSUserDefaults standardUserDefaults];
    //(sender!=nil)&&(![sender isEqual:[NSNull null]])
    if (([userLocation objectForKey:@"City"] == nil) || ([[userLocation objectForKey:@"City"]isEqual:[NSNull null]])) {
        NSLog(@"Empty");
    } else {
        //NSLog(@"%@",[userLocation objectForKey:@"City"]);
        self.cityLabel.text = [userLocation objectForKey:@"City"];
    }
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    //(sender!=nil)&&(![sender isEqual:[NSNull null]])
    if (([userLoginStatus objectForKey:@"STATUS"] == nil) || ([[userLoginStatus objectForKey:@"STATUS"]isEqual:[NSNull null]]) || ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"])) {
        [self.moreGrabsBtn setImage:[UIImage imageNamed:@"moregrabs.png"] forState:UIControlStateNormal];
        [self.moreGrabsBtn setImage:[UIImage imageNamed:@"moregrabs_sel.png"] forState:UIControlStateSelected];
        [self.howItWorksBtn setImage:[UIImage imageNamed:@"howitworks.png"] forState:UIControlStateNormal];
        [self.howItWorksBtn setImage:[UIImage imageNamed:@"howitworks_sel.png"] forState:UIControlStateSelected];
    } else {
        [self.moreGrabsBtn setImage:[UIImage imageNamed:@"myothergrabs.png"] forState:UIControlStateNormal];
        [self.moreGrabsBtn setImage:[UIImage imageNamed:@"myothergrabs_sel.png"] forState:UIControlStateSelected];
        [self.howItWorksBtn setImage:[UIImage imageNamed:@"followers.png"] forState:UIControlStateNormal];
        [self.howItWorksBtn setImage:[UIImage imageNamed:@"followers_sel.png"] forState:UIControlStateSelected];
        
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    ApplicationDelegate.window.rootViewController = ApplicationDelegate.mainNav;
    [self setUpInitialView];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setUpInitialView
{
    for (UIView *vw in self.homeTabContainerView.subviews)
    {
        [vw removeFromSuperview];
    }
    
    GrabItDealPageViewController *grabitDealsDetailVc = [[GrabItDealPageViewController alloc] initWithNibName:@"GrabItDealPageViewController" bundle:nil];
    grabitDealsDetailVc.view.backgroundColor = [UIColor clearColor];
    
    ApplicationDelegate.homeTabNav = [[UINavigationController alloc] initWithRootViewController:grabitDealsDetailVc];
    
    ApplicationDelegate.homeTabNav.navigationBar.translucent = NO;
    ApplicationDelegate.homeTabNav.view.backgroundColor = [UIColor clearColor];
    ApplicationDelegate.homeTabNav.navigationBarHidden=YES;
    
    ApplicationDelegate.homeTabNav.view.frame=CGRectMake(0, 0, self.homeTabContainerView.frame.size.width, self.homeTabContainerView.frame.size.height);
    
    [self.homeTabContainerView addSubview:ApplicationDelegate.homeTabNav.view];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

+(HomeViewController*)sharedViewController
{
    if ( !sharedObj || sharedObj == NULL )
    {
        NSString *nibName = @"HomeViewController";
        sharedObj = [[HomeViewController alloc] initWithNibName:nibName bundle:nil];
    }
    
    return sharedObj;
    
}

- (IBAction)homeTabButtonAction:(UIButton *)sender {
    [self didSelectTabItemAtIndex:sender.tag];
}

- (IBAction)backBtnActn:(UIButton *)sender {
    [ApplicationDelegate.homeTabNav popViewControllerAnimated:NO];
}

-(void)didSelectTabItemAtIndex:(NSInteger)itemIndex
{
    [self resetTabSelection];
    
    if (itemIndex == 1)
    {
        self.moreGrabsBtn.selected = YES;
        [self loadMoreGrabView];
    }
    else if (itemIndex == 2){
        self.howItWorksBtn.selected = YES;
        [self loadHowItWorksView];
    }
    else if (itemIndex == 3){
        self.myAccountBtn.selected = YES;
        [self loadMyAccountView];
    }
}

-(void)resetTabSelection
{
    self.moreGrabsBtn.selected = NO;
    self.howItWorksBtn.selected = NO;
    self.myAccountBtn.selected = NO;
}

-(void)loadMoreGrabView{
    MoreGrabViewController *moreGrabVc = [[MoreGrabViewController alloc] initWithNibName:@"MoreGrabViewController" bundle:nil];
    moreGrabVc.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[moreGrabVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:moreGrabVc animated:NO];
    }
}

-(void)loadHowItWorksView{
    if ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGIN"]) {
        FollewersViewController *followersVc = [[FollewersViewController alloc] initWithNibName:@"FollewersViewController" bundle:nil];
        followersVc.view.backgroundColor = [UIColor clearColor];
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[followersVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:followersVc animated:NO];
        }
    }else  if (([userLoginStatus objectForKey:@"STATUS"] == nil) || ([[userLoginStatus objectForKey:@"STATUS"]isEqual:[NSNull null]]) || ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"])){
        HowItsWorksViewController *howItsWorksVc = [[HowItsWorksViewController alloc] initWithNibName:@"HowItsWorksViewController" bundle:nil];
        howItsWorksVc.view.backgroundColor = [UIColor clearColor];
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[howItsWorksVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:howItsWorksVc animated:NO];
        }
    }
}

-(void)loadMyAccountView{
    if ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGIN"]) {
        MyAccountLoginViewController *myAccountLoginVc = [[MyAccountLoginViewController alloc] initWithNibName:@"MyAccountLoginViewController" bundle:nil];
        myAccountLoginVc.view.backgroundColor = [UIColor clearColor];
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myAccountLoginVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:myAccountLoginVc animated:NO];
        }
    }else  if (([userLoginStatus objectForKey:@"STATUS"] == nil) || ([[userLoginStatus objectForKey:@"STATUS"]isEqual:[NSNull null]]) || ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"])){
        MyAccountLogoutViewController *myAccountLogoutVc = [[MyAccountLogoutViewController alloc] initWithNibName:@"MyAccountLogoutViewController" bundle:nil];
        myAccountLogoutVc.view.backgroundColor = [UIColor clearColor];
        if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myAccountLogoutVc class]])
        {
            [ApplicationDelegate.homeTabNav pushViewController:myAccountLogoutVc animated:NO];
        }
    }
}

- (IBAction)citiesBtnActn:(UIButton *)sender {
    //[self.navigationController popToRootViewControllerAnimated:NO];
    CountryLocationViewController * countryLocViewObj = [[CountryLocationViewController alloc]initWithNibName:@"CountryLocationViewController" bundle:nil];
    //[self.view.window.rootViewController presentViewController:countryLocViewObj animated:YES completion:nil];
    countryLocViewObj.locationString = @"NotLoged";
    [self.navigationController pushViewController:countryLocViewObj animated:YES];
    //[self presentViewController:countryLocViewObj animated:YES completion:nil];
}
@end
