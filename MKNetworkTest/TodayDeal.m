//
//  TodayDeal.m
//  WeGrab
//
//  Created by Sreeraj VR on 07/01/2016.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "TodayDeal.h"

@implementation TodayDeal

@synthesize DealID,DealImage,DealHeadline,DealDescription,OriginalPrice,OfferPrice,Discount,ExtraDiscount;
@synthesize MinimumNoofBuyer,MaximumBuyerNo,Timer,AboutTheBusiness,FinePrint,SoldDealsNumber,CurrentBuyerNo;

@end
