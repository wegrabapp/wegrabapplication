//
//  BalanceHistoryDetails.m
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "BalanceHistoryDetails.h"

@implementation BalanceHistoryDetails

@synthesize transactionDate;
@synthesize expiryDate;
@synthesize transactionType;
@synthesize amount;
@synthesize status;

@end
