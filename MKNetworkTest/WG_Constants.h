//
//  WG_Constants.h
//  WeGrab
//
//  Created by Sreeraj VR on 04/01/2016.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#ifndef WeGrab_WG_Constants_h
#define WeGrab_WG_Constants_h
//#define kHOST_URL @"wegrab.mawaqaatest.com/en/api/webservice"
#define kHOST_URL @"wegrab.mawaqaatest.com"

//#define kGet_Citilist_URL @"cities"

#define kGet_Citilist_URL @"/en/api/webservice/cities"

//#define kGet_Citilist_URL @"/api/webservice/cities"

//#define kGet_TodayDealsList_URL @"select.php"
#define kGet_TodayDealsList_URL @"/webservice/todaydeal"
#endif
