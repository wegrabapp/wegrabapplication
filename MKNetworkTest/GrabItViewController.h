//
//  GrabItViewController.h
//  WeGrab
//
//  Created by satheesh on 1/21/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GrabItViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong, nonatomic) NSMutableArray * grabItDealOptionDetailArray;

@property (weak, nonatomic) IBOutlet UITableView *grabItOptionTableView;
@end
