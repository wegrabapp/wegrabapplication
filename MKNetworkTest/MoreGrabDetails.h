//
//  MoreGrabDetails.h
//  WeGrab
//
//  Created by satheesh on 1/8/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MoreGrabDetails : NSObject

@property (nonatomic, retain) NSString *sessionName;
@property (nonatomic, retain) NSArray *dealArray;

@end
