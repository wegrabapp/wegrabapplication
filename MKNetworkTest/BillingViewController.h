//
//  BillingViewController.h
//  WeGrab
//
//  Created by Ronish on 2/8/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DropDownView.h"

@interface BillingViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ListSelectionProtocol>{
    NSMutableArray  *arrayForBool;
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseDataInitial;
    NSMutableData * responseDataCountryList;
    NSMutableData * responseDataCityList;
    NSMutableData * responseDataDefaultCardReset;
    NSMutableData * responseDataDeleteCard;
    NSMutableData * responseDataAddNewCard;
    NSMutableData * responseDataAddCity;
    NSURLConnection *connectionAddMyCity;
    NSURLConnection *connectionInitial;
    NSURLConnection * connectionCountryList;
    NSURLConnection * connectionCityList;
    NSURLConnection * connectionDefaultCardReset;
    NSURLConnection * connectionDeleteCard;
    NSURLConnection * connectionAddNewCard;
    //NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * addCityDataDictionary;
    int dropDownTag;
}
@property (weak, nonatomic) IBOutlet UIView *headingBackView;

@property (weak, nonatomic) IBOutlet UIView *createNewCardBtnBackView;
@property (weak, nonatomic) IBOutlet UIView *createNewCardDetailsBackView;
@property (weak, nonatomic) IBOutlet UIScrollView *billingMainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *billingTableView;
//@property (strong, nonatomic) NSMutableArray * billingInformationArray;
@property (strong, nonatomic) NSMutableDictionary * billingInformationDictionary;
@property (strong, nonatomic) NSMutableDictionary * countryDataDictionary;
@property (strong, nonatomic) NSMutableDictionary * cityDataDictionary;
@property (strong, nonatomic) NSString * selectedCardId;
@property (strong, nonatomic) NSString * selectedCityId;
@property (strong, nonatomic) NSString * selectedCountryId;
//@property (strong, nonatomic) DropDownView *itemDropDownObj;
- (IBAction)createNewCardBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *createNewCardArrowView;
@property (weak, nonatomic) IBOutlet UIButton *createNewCardBtn;
@property (weak, nonatomic) IBOutlet UIImageView *countryUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *countryDownArrow;
@property (weak, nonatomic) IBOutlet UIImageView *upArrowCity;
@property (weak, nonatomic) IBOutlet UIImageView *downArrowCity;
- (IBAction)dropDownBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *dropDownCountryBtn;
@property (weak, nonatomic) IBOutlet UIButton *dropDownCityBtn;
@property (weak, nonatomic) IBOutlet UIButton *dropDownCardBtn;
@property (strong, nonatomic) DropDownView *countryDropDownObj;
@property (strong, nonatomic) DropDownView *cityDropDownObj;
@property (strong, nonatomic) DropDownView *cardDropDownObj;
@property (weak, nonatomic) IBOutlet UITextField *countryTxtView;
@property (weak, nonatomic) IBOutlet UITextField *cityTxtView;
@property (weak, nonatomic) IBOutlet UITextField *cardTxtView;

@property (strong, nonatomic) NSMutableArray * countryDropDownNameList;
@property (strong, nonatomic) NSMutableArray * countryDropDownIdList;

@property (strong, nonatomic) NSMutableArray * cityDropDownNameList;
@property (strong, nonatomic) NSMutableArray * cityDropDownIdList;

@property (strong, nonatomic) NSMutableArray * cardDropDownNameList;
@property (strong, nonatomic) NSMutableArray * cardDropDownIdList;

@property (weak, nonatomic) IBOutlet UIImageView *cardUpArrow;
@property (weak, nonatomic) IBOutlet UIImageView *cardDownArrow;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextView;
@property (weak, nonatomic) IBOutlet UITextField *expiryDateTextView;
@property (weak, nonatomic) IBOutlet UIButton *makeThisMyDefaultCreditCardBtn;

- (IBAction)makeThisMyDefaultCreditCardBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *fullNameTextView;
@property (weak, nonatomic) IBOutlet UITextField *address1TextView;
@property (weak, nonatomic) IBOutlet UITextField *address2TextView;
@property (weak, nonatomic) IBOutlet UITextField *stateProvinceTextView;
@property (weak, nonatomic) IBOutlet UITextField *postalCodeTextView;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumberTextView;
- (IBAction)addBtnActn:(UIButton *)sender;
@end
