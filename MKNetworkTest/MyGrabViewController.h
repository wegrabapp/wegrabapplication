//
//  MyGrabViewController.h
//  WeGrab
//
//  Created by Ronish on 2/11/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyGrabViewController : UIViewController<UITableViewDataSource,UITableViewDelegate>{
    NSMutableArray  *arrayForBool;
    NSArray *sectionTitleArray;
    NSMutableDictionary * myGrabDataDictionary;
    NSMutableDictionary * addCityDataDictionary;
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableData * responseDataAddCity;
    NSURLConnection *connectionAddMyCity;
}
@property (weak, nonatomic) IBOutlet UIView *myGrabHeadingView;
@property (weak, nonatomic) IBOutlet UITableView *myGrabTableView;
//@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
//@property (weak, nonatomic) IBOutlet UITableView *countryTableView;
@property (weak, nonatomic) NSString * locationString;
//@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
//@property (weak, nonatomic) IBOutlet UIView *labelBackView;
//- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
