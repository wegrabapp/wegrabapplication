//
//  AddCityNormalTableViewCell.h
//  WeGrab
//
//  Created by Ronish on 2/4/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddCityNormalTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *CityNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *addBtn;
@property (weak, nonatomic) IBOutlet UIImageView *backGroundView;
@property (weak, nonatomic) IBOutlet UIImageView *tickBox;

@end
