//
//  BuyGiftVoucherDetailsViewController.h
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BuyGiftVoucherDetailsViewController : UIViewController<UIPickerViewDelegate, UIPickerViewDataSource>{
    NSUserDefaults * userLocation;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *grabItBtn;

@property (strong, nonatomic) NSString * dealId;
@property (strong, nonatomic) NSString * giftToTextData;
@property (strong, nonatomic) NSString * giftFromTextData;
@property (strong, nonatomic) NSString * giftEmailTextData;
@property (strong, nonatomic) NSString * giftMessageTextData;

- (IBAction)cancelBtnActn:(UIButton *)sender;
- (IBAction)grabItBtnActn:(UIButton *)sender;

- (IBAction)editBtnActn:(UIButton *)sender;
- (IBAction)cancelButtonActn:(UIButton *)sender;
- (IBAction)okBtnAction:(UIButton *)sender;

@property (strong, nonatomic) NSString * voucherID;
@property (strong, nonatomic) NSString * voucherAmount;
@property (strong, nonatomic) NSString * voucherQuantity;
@property (strong, nonatomic) NSMutableArray * voucherAmountArray;
@property (strong, nonatomic) NSMutableArray * voucherIDArray;
@property (strong, nonatomic) NSMutableArray * voucherQuantityArray;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UITextField *toNameTxtView;
@property (weak, nonatomic) IBOutlet UITextField *fromNameTxtView;
@property (weak, nonatomic) IBOutlet UITextField *emailTextView;
@property (weak, nonatomic) IBOutlet UITextField *messageTxtView;
@property (weak, nonatomic) IBOutlet UIView *editAmountBackView;

@end
