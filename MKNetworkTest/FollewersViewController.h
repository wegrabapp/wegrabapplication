//
//  FollewersViewController.h
//  WeGrab
//
//  Created by satheesh on 1/12/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SectionViewMoreGrab.h"
#import "SectionView.h"

@interface FollewersViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,SectionViewMoreGrab,NSURLConnectionDelegate,SectionView>{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
    NSMutableData * responseData;
    NSMutableDictionary * countryDataDictionary;
    NSMutableDictionary * followerDataDictionary;
}
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;
@property (weak, nonatomic) IBOutlet UITableView *followersTableView;
@property (strong, nonatomic) NSMutableDictionary * moreGrabDictionary;
@property (weak, nonatomic) IBOutlet UILabel *totalAmountEarnedValueLbl;
@property (weak, nonatomic) IBOutlet UILabel *amountEarnedTHisMonthValueLbl;

@end
