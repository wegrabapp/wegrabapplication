//
//  MyCitiesTableViewCell.h
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCitiesTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tickBox;

@end
