//
//  LoginViewController.m
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController (){
    NSURLConnection *connectionLogin;
    NSURLConnection *connectionSignUp;
    NSURLConnection *connectionForgot;
    
    NSMutableData * responseDataLogin;
    NSMutableData * responseDataSignUp;
    NSMutableData * responseDataForgot;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.contentLoginBackView.hidden = NO;
    self.loginBtn.selected = YES;
    self.loginProceedBtn.hidden = NO;
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, 568);
    float tempy;
    tempy = self.contentScrollView.frame.origin.y + self.contentScrollView.frame.size.height + 50;
    self.cancelBtn.frame = CGRectMake(self.cancelBtn.frame.origin.x, tempy, self.cancelBtn.frame.size.width, self.cancelBtn.frame.size.height);
    self.loginProceedBtn.frame = CGRectMake(self.loginProceedBtn.frame.origin.x, tempy, self.loginProceedBtn.frame.size.width, self.loginProceedBtn.frame.size.height);
    
    self.loginEmailId.delegate = self;
    self.loginPassword.delegate = self;
    self.signUpName.delegate = self;
    self.signUpEmailId.delegate = self;
    self.signUpNumber.delegate = self;
    self.signUpBirthDate.delegate = self;
    self.signUpPassword.delegate = self;
    self.signUpConfirmPassword.delegate = self;
    self.forgotEmailId.delegate = self;
    
    
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    self.signUpNumber.inputAccessoryView = numberToolbar;
    
    UIToolbar* numberToolbarDOB = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    numberToolbarDOB.barStyle = UIBarStyleBlackTranslucent;
    numberToolbarDOB.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPadDOB)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPadDOB)]];
    [numberToolbar sizeToFit];
    self.signUpBirthDate.inputAccessoryView = numberToolbarDOB;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.signUpNumber];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(limitTextField:) name:@"UITextFieldTextDidChangeNotification" object:self.signUpBirthDate];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logBttnActn:(UIButton *)sender {
    self.loginBtn.selected = NO;
    self.signUpBtn.selected = NO;
    self.forgetBtn.selected = NO;
    self.loginProceedBtn.hidden = YES;
    self.signUpProceedBtn.hidden = YES;
    self.forgotProceedBtn.hidden = YES;
    
    float tempy;
    
    if (sender.tag == 1) {
        self.loginBtn.selected = YES;
        self.loginProceedBtn.hidden = NO;
        self.contentLoginBackView.hidden = NO;
        self.contentSignUpBackView.hidden = YES;
        self.contentForgotBackView.hidden = YES;
        self.contentScrollView.frame = CGRectMake(self.contentScrollView.frame.origin.x, self.contentScrollView.frame.origin.y, self.contentScrollView.frame.size.width, 352);
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, 568);
        NSLog(@"%f",self.cancelBtn.frame.origin.y);
        tempy = self.contentScrollView.frame.origin.y + self.contentScrollView.frame.size.height + 50;
        self.cancelBtn.frame = CGRectMake(self.cancelBtn.frame.origin.x, tempy, self.cancelBtn.frame.size.width, self.cancelBtn.frame.size.height);
        self.loginProceedBtn.frame = CGRectMake(self.loginProceedBtn.frame.origin.x, tempy, self.loginProceedBtn.frame.size.width, self.loginProceedBtn.frame.size.height);
    }
    else if (sender.tag == 2){
        self.signUpBtn.selected = YES;
        self.signUpProceedBtn.hidden = NO;
        self.contentLoginBackView.hidden = YES;
        self.contentSignUpBackView.hidden = NO;
        self.contentForgotBackView.hidden = YES;
        self.contentScrollView.frame = CGRectMake(self.contentScrollView.frame.origin.x, self.contentScrollView.frame.origin.y, self.contentScrollView.frame.size.width, 352 + 340);
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, 568 + 340);
        tempy = self.contentScrollView.frame.origin.y + self.contentScrollView.frame.size.height + 50;
        self.cancelBtn.frame = CGRectMake(self.cancelBtn.frame.origin.x, tempy, self.cancelBtn.frame.size.width, self.cancelBtn.frame.size.height);
        self.signUpProceedBtn.frame = CGRectMake(self.signUpProceedBtn.frame.origin.x, tempy , self.signUpProceedBtn.frame.size.width, self.signUpProceedBtn.frame.size.height);
    }
    else if (sender.tag == 3){
        self.forgetBtn.selected = YES;
        self.forgotProceedBtn.hidden = NO;
        self.contentLoginBackView.hidden = YES;
        self.contentSignUpBackView.hidden = YES;
        self.contentForgotBackView.hidden = NO;
        self.contentScrollView.frame = CGRectMake(self.contentScrollView.frame.origin.x, self.contentScrollView.frame.origin.y, self.contentScrollView.frame.size.width, 289);
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, 460);
        tempy = self.contentScrollView.frame.origin.y + self.contentScrollView.frame.size.height + 50;
        self.cancelBtn.frame = CGRectMake(self.cancelBtn.frame.origin.x, tempy, self.cancelBtn.frame.size.width, self.cancelBtn.frame.size.height);
        self.forgotProceedBtn.frame = CGRectMake(self.forgotProceedBtn.frame.origin.x, tempy, self.forgotProceedBtn.frame.size.width, self.forgotProceedBtn.frame.size.height);
    }
}

- (IBAction)cancelBtnActn:(UIButton *)sender {
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)loginBtnAction:(UIButton *)sender {
    
    if ((self.loginEmailId.text.length == 0) && (self.loginPassword.text.length == 0)) {
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter email id and password" title:@"Sorry..!"];
        
        
    } else if(self.loginEmailId.text.length == 0){
        
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter email" title:@"Sorry..!"];
        
        
    }else if(self.loginPassword.text.length == 0){
        
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter password" title:@"Sorry..!"];
        
        
    }else{
        if ([self NSStringIsValidEmail:self.loginEmailId.text]) {
            
            if ([ApplicationDelegate checkNetworkAvailability])
            {
                [ApplicationDelegate addProgressHUDToView:self.view];
                
                NSMutableURLRequest *request = [NSMutableURLRequest
                                                requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/login"]];
                
                NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             self.loginEmailId.text, @"Email",
                                             self.loginPassword.text, @"Password",
                                             nil];
                NSError *error;
                NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:postData];
                connectionLogin = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            }
            else
            {
                [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
                
            }
        } else {
            
            
            [ApplicationDelegate showAlertWithMessage:@"Please enter valid email id" title:@"Sorry..!"];
        }
    }
}

////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseDataLogin = [[NSMutableData alloc] init];
    responseDataSignUp = [[NSMutableData alloc] init];
    responseDataForgot = [[NSMutableData alloc] init];
    self.loginDataDictionary = [[NSMutableDictionary alloc]init];
    self.signUpDataDictionary = [[NSMutableDictionary alloc]init];
    self.forgotDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    if (connection == connectionLogin) {
        [responseDataLogin appendData:data];
    }
    else if (connection == connectionSignUp){
        [responseDataSignUp appendData:data];
    }
    else if (connection == connectionForgot){
        [responseDataForgot appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    
    if (connection == connectionLogin) {
        NSError* error;
        self.loginDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataLogin options:kNilOptions error:&error];
        
        if ([ApplicationDelegate isValid:[self.loginDataDictionary objectForKey:@"Success"]])
        {
            if ([[self.loginDataDictionary objectForKey:@"Success"] integerValue]) {
                [ApplicationDelegate removeProgressHUD];
                
                userLoginStatus = [NSUserDefaults standardUserDefaults];
                [userLoginStatus setObject:@"LOGIN" forKey:@"STATUS"];
                [userLoginStatus synchronize];
                
                if ([ApplicationDelegate isValid:[self.loginDataDictionary objectForKey:@"Users"]]) {
                    NSLog(@"%@",[self.loginDataDictionary objectForKey:@"Users"]);
                    if ([ApplicationDelegate isValid:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"email"]]) {
                        [userLoginStatus setObject:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"email"] forKey:@"email"];
                    }
                    if ([ApplicationDelegate isValid:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"FirstName"]]) {
                        [userLoginStatus setObject:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"FirstName"] forKey:@"FirstName"];
                    }
                    if ([ApplicationDelegate isValid:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"UserId"]]) {
                        [userLoginStatus setObject:[[self.loginDataDictionary objectForKey:@"Users"]objectForKey:@"UserId"] forKey:@"UserId"];
                    }
                }
                [userLoginStatus synchronize];
                
                [ApplicationDelegate showAlertWithMessage:[self.loginDataDictionary objectForKey:@"Message"] title:nil];
                CATransition *transition = [CATransition animation];
                transition.duration = 0.3;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromLeft;
                [self.view.window.layer addAnimation:transition forKey:nil];
                [self dismissViewControllerAnimated:NO completion:nil];
            } else {
                [ApplicationDelegate showAlertWithMessage:[self.loginDataDictionary objectForKey:@"Message"] title:nil];
                
                [ApplicationDelegate removeProgressHUD];
            }
        }
        else{
            ///////////////////////test////////////
            
            ///////////////////////test////////////
        }
    }
    else if (connection == connectionSignUp){
        NSError* error;
        self.signUpDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataSignUp options:kNilOptions error:&error];
        NSLog(@"%@",self.signUpDataDictionary);
        if ([ApplicationDelegate isValid:[[self.signUpDataDictionary objectForKey:@"signup_status"]objectForKey:@"Success"]])
        {
            if ([[[self.signUpDataDictionary objectForKey:@"signup_status"]objectForKey:@"Success"] integerValue]) {
                [ApplicationDelegate showAlertWithMessage:[[self.signUpDataDictionary objectForKey:@"signup_status"]objectForKey:@"Message"] title:nil];
                CATransition *transition = [CATransition animation];
                transition.duration = 0.3;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromLeft;
                [self.view.window.layer addAnimation:transition forKey:nil];
                [self dismissViewControllerAnimated:NO completion:nil];
                [ApplicationDelegate removeProgressHUD];
            } else {
                [ApplicationDelegate showAlertWithMessage:[[self.signUpDataDictionary objectForKey:@"signup_status"]objectForKey:@"Message"] title:nil];
                
                [ApplicationDelegate removeProgressHUD];
            }
        }
    }
    else if (connection == connectionForgot){
        NSError* error;
        self.forgotDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataForgot options:kNilOptions error:&error];
        NSLog(@"%@", self.forgotDataDictionary);
        if ([ApplicationDelegate isValid:[self.forgotDataDictionary objectForKey:@"Success"]])
        {
            if ([[self.forgotDataDictionary objectForKey:@"Success"] integerValue]) {
                [ApplicationDelegate showAlertWithMessage:[self.forgotDataDictionary objectForKey:@"Message"] title:nil];
                CATransition *transition = [CATransition animation];
                transition.duration = 0.3;
                transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                transition.type = kCATransitionPush;
                transition.subtype = kCATransitionFromLeft;
                [self.view.window.layer addAnimation:transition forKey:nil];
                [self dismissViewControllerAnimated:NO completion:nil];
                [ApplicationDelegate removeProgressHUD];
            } else {
                [ApplicationDelegate showAlertWithMessage:[self.forgotDataDictionary objectForKey:@"error"] title:nil];
                
                [ApplicationDelegate removeProgressHUD];
            }
        }
    }
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [ApplicationDelegate removeProgressHUD];
}


////////////////////// nsurl ///////////////////////

- (IBAction)signBtnAction:(UIButton *)sender {
    
    if ((self.signUpName.text.length == 0) && (self.signUpEmailId.text.length == 0) && (self.signUpNumber.text.length == 0) && (self.signUpPassword.text.length == 0) && (self.signUpConfirmPassword.text.length == 0) &&(self.signUpBirthDate.text.length == 0))
    {
        
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter your details" title:@"Sorry..!"];
        
        
    } else if(self.signUpName.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter your name" title:@"Sorry..!"];
        
    }
    //    else if(self.lastNameTextView.text.length == 0){
    //        alrtView = [[UIAlertView alloc]initWithTitle:@"Sorry..!" message:@"Please enter your Last name" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    //        [alrtView show];
    //    }
    else if(self.signUpEmailId.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter your email id" title:@"Sorry..!"];
        
    }
    else if(self.signUpPassword.text.length == 0)
    {
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter a password" title:@"Sorry..!"];
        
    }
    else if(self.signUpNumber.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter a valid number" title:@"Sorry..!"];
    }
    else if(self.signUpBirthDate.text.length == 0){
        [ApplicationDelegate showAlertWithMessage:@"Please enter a valid date of birth" title:@"Sorry..!"];
    }
    else if(!self.tickBtn.selected){
        [ApplicationDelegate showAlertWithMessage:@"To proceed, you have to agree terms and conditions" title:@"Sorry..!"];
    }
    else{
        if (![self NSStringIsValidEmail:self.signUpEmailId.text])
        {
            
            [ApplicationDelegate showAlertWithMessage:@"Please enter a valid email id" title:@"Sorry..!"];
            
            
        }else if(![self.signUpPassword.text isEqualToString:self.signUpConfirmPassword.text]){
            
            
            [ApplicationDelegate showAlertWithMessage:@"Confirmation password is wrong" title:@"Sorry..!"];
            
        }
        
        else{
            
            if ([ApplicationDelegate checkNetworkAvailability])
            {
                [ApplicationDelegate addProgressHUDToView:self.view];
                
                NSMutableURLRequest *request = [NSMutableURLRequest
                                                requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/signup"]];
                NSString * phoneCategory;
                if (self.mobileRadioBtn.selected) {
                    phoneCategory = [NSString stringWithFormat:@"MOBILE"];
                } else {
                    phoneCategory = [NSString stringWithFormat:@"LAND"];
                }
                NSString * genderCategory;
                if (self.maleRadioBtn.selected) {
                    genderCategory = [NSString stringWithFormat:@"MALE"];
                } else {
                    genderCategory = [NSString stringWithFormat:@"FEMALE"];
                }
                NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             self.signUpName.text, @"FirstName",
                                             self.signUpEmailId.text, @"Email",
                                             phoneCategory, @"TelCatagory",
                                             self.signUpNumber.text, @"TelNumber",
                                             self.signUpBirthDate.text, @"BirthDate",
                                             genderCategory, @"Gender",
                                             self.signUpPassword.text, @"Password",
                                             nil];
                
                NSLog(@"%@",requestData);
                NSError *error;
                NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:postData];
                connectionSignUp = [[NSURLConnection alloc] initWithRequest:request delegate:self];
                
            }
            else
            {
                [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
                
            }
        }
    }
}

- (IBAction)sendBtnActn:(UIButton *)sender {
    
    if(self.forgotEmailId.text.length == 0){
        
        [ApplicationDelegate showAlertWithMessage:@"Please enter email" title:@"Sorry..!"];
        
    }else{
        if ([self NSStringIsValidEmail:self.forgotEmailId.text]) {
            
            if ([ApplicationDelegate checkNetworkAvailability])
            {
                [ApplicationDelegate addProgressHUDToView:self.view];
                
                NSMutableURLRequest *request = [NSMutableURLRequest
                                                requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/forgot_password"]];
                
                NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                             self.forgotEmailId.text, @"Email",
                                             nil];
                NSError *error;
                NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
                [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
                [request setHTTPMethod:@"POST"];
                [request setHTTPBody:postData];
                connectionForgot = [[NSURLConnection alloc] initWithRequest:request delegate:self];
            }
            else
            {
                [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
            }
        } else {
            
            
            [ApplicationDelegate showAlertWithMessage:@"Please enter valid email id" title:@"Sorry..!"];
        }
    }
}

- (IBAction)telRadioBtnActn:(UIButton *)sender {
    if (self.mobileRadioBtn.selected) {
        self.mobileRadioBtn.selected = NO;
        self.landRadioBtn.selected = YES;
    } else {
        self.mobileRadioBtn.selected = YES;
        self.landRadioBtn.selected = NO;
    }
}

- (IBAction)genderRadioBtnActn:(UIButton *)sender {
    if (self.maleRadioBtn.selected) {
        self.maleRadioBtn.selected = NO;
        self.femaleRadioBtn.selected = YES;
    } else {
        self.maleRadioBtn.selected = YES;
        self.femaleRadioBtn.selected = NO;
    }
}

- (IBAction)tickBtnActn:(UIButton *)sender {
    if (self.tickBtn.selected) {
        self.tickBtn.selected = NO;
    }
    else{
        self.tickBtn.selected = YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void)cancelNumberPad{
    [ self.signUpNumber resignFirstResponder];
    self.signUpNumber.text = @"";
}

-(void)doneWithNumberPad{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.signUpNumber resignFirstResponder];
}

-(void)cancelNumberPadDOB{
    [ self.signUpBirthDate resignFirstResponder];
    self.signUpBirthDate.text = @"";
}

-(void)doneWithNumberPadDOB{
    // NSString *numberFromTheKeyboard =  self.mobileNumberTextView.text;
    [self.signUpBirthDate resignFirstResponder];
}

- (void)limitTextField:(NSNotification *)note {
    int limit = 15;
    int codeLimit = 10;
    if (self.signUpNumber.text.length > limit) {
        [self.signUpNumber setText:[self.signUpNumber.text substringToIndex:limit]];
    }else if(self.signUpBirthDate.text.length > codeLimit){
        [self.signUpBirthDate setText:[self.signUpBirthDate.text substringToIndex:codeLimit]];
    }else if(self.signUpBirthDate.text.length == 2){
        [self.signUpBirthDate setText:[NSString stringWithFormat:@"%@/",self.signUpBirthDate.text]];
    }else if(self.signUpBirthDate.text.length == 5){
        [self.signUpBirthDate setText:[NSString stringWithFormat:@"%@/",self.signUpBirthDate.text]];
    }
}

@end
