//
//  LaunchingScreenViewController.h
//  WeGrab
//
//  Created by satheesh on 12/30/15.
//  Copyright (c) 2015 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreLocation/CoreLocation.h>



@interface LaunchingScreenViewController : UIViewController<CLLocationManagerDelegate>{
    NSUserDefaults * userLocation;
    int failureFlag;
}
@end
