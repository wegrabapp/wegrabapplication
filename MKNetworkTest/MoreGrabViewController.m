//
//  MoreGrabViewController.m
//  WeGrab
//
//  Created by satheesh on 1/7/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MoreGrabViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "HomeViewController.h"
#import "MoreGrabCellTableViewCell.h"
#import "CountryDetails.h"
#import "SectionInfo.h"
#import "SectionInfoMoreGrab.h"
#import "MoreGrabDetails.h"
#import "MoreGrabCellDetails.h"
#import "SingleGrabItDealViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"

@interface MoreGrabViewController (){
    HomeViewController * homePageViewObj;
}
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * placeList;
@property (nonatomic, strong) NSArray * sectionList;
@property (nonatomic, strong) NSMutableArray * globalList;
- (void) setCategoryArray;

@end

@implementation MoreGrabViewController

@synthesize placeList;
@synthesize openSectionIndex;
@synthesize sectionInfoArray;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [HomeViewController sharedViewController].citiesBtn.hidden = NO;
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    if ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGIN"]) {
        [HomeViewController sharedViewController].cityLabel.text = @"My Other Grabs";
    }else  if (([userLoginStatus objectForKey:@"STATUS"] == nil) || ([[userLoginStatus objectForKey:@"STATUS"]isEqual:[NSNull null]]) || ([[userLoginStatus objectForKey:@"STATUS"] isEqualToString:@"LOGOUT"])){
        [HomeViewController sharedViewController].cityLabel.text = @"More Grabs";
    }
    //[self getCountryName];
    //NSLog(@"%f",self.view.frame.size.height);
}

//-(void)getCountryName
//{
//    [ApplicationDelegate addProgressHUDToView:self.view];
//    
//    [ApplicationDelegate.engine getCountryList:^(NSMutableDictionary *response)
//     {
//         {
//             NSLog(@"1......%@",response);
//             countryDataDictionary = [[NSMutableDictionary alloc]init];
//             NSLog(@"2.....%@",[response objectForKey:@"countries"]);
//             
//             [countryDataDictionary setObject:[response objectForKey:@"countries"] forKey:@"countries"];
//             NSLog(@"3........%lu",(unsigned long)[[countryDataDictionary objectForKey:@"countries"] count]);
//             
//             if ([[countryDataDictionary objectForKey:@"countries"] count] > 0) {
//                 
//                 NSMutableArray *countryArray = [[NSMutableArray alloc] initWithCapacity:[[countryDataDictionary objectForKey:@"countries"] count]];
//                 for (NSDictionary *dictionary in [countryDataDictionary objectForKey:@"countries"]) {
//                     CountryDetails *countryDetails = [[CountryDetails alloc] init];
//                     countryDetails.countryCode = [dictionary objectForKey:@"CountrCode"];
//                     countryDetails.countryName = [dictionary objectForKey:@"CountryName"];
//                     countryDetails.cities = [dictionary objectForKey:@"cities"];
//                     [countryArray addObject:countryDetails];
//                 }
//                 self.placeList = countryArray;
//                 
//                 if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.moreGrabTableView])) {
//                     NSMutableArray *array = [[NSMutableArray alloc] init];
//                     
//                     for (CountryDetails *country in self.placeList) {
//                         SectionInfo *section = [[SectionInfo alloc] init];
//                         section.country = country;
//                         section.open = YES;
//                         
//                         NSNumber *defaultHeight = [NSNumber numberWithInt:44];
//                         NSInteger count = [[section.country cities] count];
//                         
//                         for (NSInteger i= 0; i<count; i++) {
//                             NSLog(@"4......%d count...",count);
//                             NSLog(@"5.....%@",[section.country countryName]);
//                             [section insertObject:defaultHeight inRowHeightsAtIndex:i];
//                             NSLog(@"6......%@",section);
//                         }
//                         
//                         [array addObject:section];
//                     }
//                     //NSLog(@"%@",array);
//                     self.sectionInfoArray = array;
//                 }
//                 
//                 [self.moreGrabTableView reloadData];
//                 [ApplicationDelegate removeProgressHUD];
//             }
//         }
//     } errorHandler:^(NSError *error) {
//         
//         
//         
//     }];
//    
//}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self fetchDatafromServer];
    
    //self.moreGrabTableView.allowsSelection = NO;
    
    /////////////////////////////////////////////
//    NSMutableArray * sessionArray = [[NSMutableArray alloc]init];
//    NSArray * rowArray;
//    NSMutableDictionary * dummyDictionary;
//    for (int i=0; i<10; i++) {
//        dummyDictionary = [[NSMutableDictionary alloc]init];
//        [dummyDictionary setObject:[NSString stringWithFormat:@"Place %d",i] forKey:@"Place"];
//        [dummyDictionary setObject:[NSString stringWithFormat:@"PlaceId %d",i] forKey:@"PlaceID"];
//        rowArray = [[NSArray alloc]initWithObjects:@"objectOne",@"ObjectTwo", nil];
//        [dummyDictionary setObject:rowArray forKey:@"GrabList"];
//        [sessionArray addObject:dummyDictionary];
//    }
//    NSLog(@"%@",sessionArray);
//    
//    self.placeList = sessionArray;
//    
//    [self.moreGrabTableView reloadData];
    
    /////////////////////////////////////////////
    if ([self.locationString isEqualToString:@"Initial"]) {
        self.cancelBtn.hidden = YES;
    } else if ([self.locationString isEqualToString:@"NotLoged"]){
        userLocation = [NSUserDefaults standardUserDefaults];
        //(sender!=nil)&&(![sender isEqual:[NSNull null]])
        if (([userLocation objectForKey:@"City"] == nil) || ([[userLocation objectForKey:@"City"]isEqual:[NSNull null]])) {
            //NSLog(@"Empty");
            
        } else {
            //NSLog(@"%@",[userLocation objectForKey:@"City"]);
            self.cityLabel.text = [userLocation objectForKey:@"City"];
        }
        self.cancelBtn.hidden = NO;
        self.labelBackView.hidden = NO;
        CGRect tempFrame = self.moreGrabTableView.frame;
        self.moreGrabTableView.frame = CGRectMake(tempFrame.origin.x, tempFrame.origin.y+(self.labelBackView.frame.size.height - 8), tempFrame.size.width, tempFrame.size.height-(self.labelBackView.frame.size.height - 8));
    }
    self.moreGrabTableView.sectionHeaderHeight = 30;
    self.moreGrabTableView.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    
    //NSLog(@"7......%ld",(unsigned long)[self.placeList count]);
//    return [self.placeList count];
    //NSLog(@"%@",self.globalList);
    return [self.globalList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    
    SectionInfoMoreGrab *array = [self.sectionInfoArray objectAtIndex:section];
    
    NSInteger rows = [[array.moreGrabDetails dealArray] count];
    //NSLog(@"row count...%d",rows);
    return (array.open) ? rows : 0;
}

/*
 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
 
 // Configure the cell...
 
 return cell;
 }
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    MoreGrabCellTableViewCell *cell = (MoreGrabCellTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        //cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MoreGrabCellTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    MoreGrabDetails * tempMoreGrabDetails;
    tempMoreGrabDetails = [self.globalList objectAtIndex:indexPath.section];
    MoreGrabCellDetails *grabCellDetails = [[MoreGrabCellDetails alloc] init];
    grabCellDetails = [tempMoreGrabDetails.dealArray objectAtIndex:indexPath.row];
    //[cell.cellImage setImage:[UIImage imageNamed:@"sample.png"]];
    
    //NSLog(@"%@",grabCellDetails.dealImage);
    NSURL * Imageurl = [[NSURL alloc]init];
    
    Imageurl = [NSURL URLWithString:[ApplicationDelegate getHTTPCorrectedURLFromUrl:grabCellDetails.dealImage]];
    
    [cell.cellImage sd_setImageWithURL:Imageurl placeholderImage:[UIImage imageNamed:@"loading.png"]];
    if ([grabCellDetails.cityName isEqualToString:@""] && [grabCellDetails.countryName isEqualToString:@""]) {
        cell.placeLbl.text = tempMoreGrabDetails.sessionName;
    }
    else {
        if ([grabCellDetails.cityName isEqualToString:@""]) {
            cell.placeLbl.text = grabCellDetails.countryName;
        } else {
            cell.placeLbl.text = grabCellDetails.cityName;
        }
    }
    
    cell.cellHeading.text = grabCellDetails.title;
    cell.cellDescription.text = grabCellDetails.dealDescription;
    CGRect frame = cell.cellDescription.frame;
    frame.size.width = ([[UIScreen mainScreen]bounds].size.width - (frame.origin.x + 5)) ;
    cell.cellDescription.frame = frame;
     [ApplicationDelegate loadMarqueeLabelWithText:cell.cellDescription.text Font:cell.cellDescription.font InPlaceOfLabel:cell.cellDescription];
    
    //[cell.cellDescription setValue:grabCellDetails.dealDescription forKey:@"contentToHTMLString"];
    //cell.oldPrice.text = [NSString stringWithFormat:@"$ %@",grabCellDetails.oldPrice];
    
    NSMutableAttributedString *titleString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"$ %@",grabCellDetails.oldPrice]];
    
    [titleString addAttribute: NSStrikethroughStyleAttributeName value:[NSNumber numberWithInteger: NSUnderlineStyleSingle] range: NSMakeRange(0, [titleString length])];
    
    cell.oldPrice.attributedText = titleString;
    
    //[self.crossedPriceLbl  setAttributedText: titleString];
    
    cell.currentPrice.text = [NSString stringWithFormat:@"$ %@",grabCellDetails.currentPrice];
    //cell.
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
//    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
//    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
    return 113;
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfoMoreGrab *array  = [self.sectionInfoArray objectAtIndex:section];
    if (!array.sectionViewMoreGrab)
    {
        //        NSString *title = array.category.name;
        NSString *title = array.moreGrabDetails.sessionName;
        array.sectionViewMoreGrab = [[SectionViewMoreGrab alloc] initWithFrame:CGRectMake(0, 0, self.moreGrabTableView.bounds.size.width, 30) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionViewMoreGrab;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreGrabDetails * tempMoreGrabDetails;
    tempMoreGrabDetails = [self.globalList objectAtIndex:indexPath.section];
    //NSLog(@"%@",tempMoreGrabDetails);
    MoreGrabCellDetails *grabCellDetails = [[MoreGrabCellDetails alloc] init];
    grabCellDetails = [tempMoreGrabDetails.dealArray objectAtIndex:indexPath.row];
    //NSLog(@"%@",grabCellDetails.dealID);
    SingleGrabItDealViewController *singleDealViewObj = [[SingleGrabItDealViewController alloc] initWithNibName:@"SingleGrabItDealViewController" bundle:nil];
    singleDealViewObj.dealID = grabCellDetails.dealID;
    singleDealViewObj.view.backgroundColor = [UIColor clearColor];
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[singleDealViewObj class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:singleDealViewObj animated:NO];
    }
}

//- (void) sectionClosed : (NSInteger) section{
//    /*
//     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
//     */
//    SectionInfo *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
//    
//    sectionInfo.open = NO;
//    NSInteger countOfRowsToDelete = [self.moreGrabTableView numberOfRowsInSection:section];
//    
//    if (countOfRowsToDelete > 0) {
//        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
//        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
//            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
//        }
//        [self.moreGrabTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
//    }
//    self.openSectionIndex = NSNotFound;
//}
//
//- (void) sectionOpened : (NSInteger) section
//{
//    SectionInfo *array = [self.sectionInfoArray objectAtIndex:section];
//    
//    array.open = NO;
//    NSInteger count = [array.country.cities count];
//    NSLog(@"11.....%d",count);
//    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
//    for (NSInteger i = 0; i<count;i++)
//    {
//        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
//    }
//    
//    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
//    NSInteger previousOpenIndex = self.openSectionIndex;
//    
//    if (previousOpenIndex != NSNotFound)
//    {
//        SectionInfo *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
//        sectionArray.open = NO;
//        //        NSInteger counts = [sectionArray.category.list count];
//        NSInteger counts = [sectionArray.country.cities count];
//        [sectionArray.sectionView toggleButtonPressed:FALSE];
//        for (NSInteger i = 0; i<counts; i++)
//        {
//            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
//        }
//    }
//    UITableViewRowAnimation insertAnimation;
//    UITableViewRowAnimation deleteAnimation;
//    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
//    {
//        insertAnimation = UITableViewRowAnimationTop;
//        deleteAnimation = UITableViewRowAnimationBottom;
//    }
//    else
//    {
//        insertAnimation = UITableViewRowAnimationBottom;
//        deleteAnimation = UITableViewRowAnimationTop;
//    }
//    
//    [self.moreGrabTableView beginUpdates];
//    [self.moreGrabTableView insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
//    [self.moreGrabTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
//    [self.moreGrabTableView endUpdates];
//    self.openSectionIndex = section;
//    
//}

- (IBAction)cancelBtnActn:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


-(void)fetchDatafromServer
{
    /////////////////////////// nsurl /////////////////////////
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        /*
         NSMutableURLRequest *request = [NSMutableURLRequest
         requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/allgrab"]];
         
         NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
         @"112", @"country_id",
         @"6", @"city_id",
         nil];
         
         */
        
        NSMutableURLRequest *request;
        NSDictionary *requestData;
        
        userLocation = [NSUserDefaults standardUserDefaults];
        //NSLog(@"%@",[userLocation objectForKey:@"country_id"]);
        //NSLog(@"%@",[userLocation objectForKey:@"city_id"]);
        
        if (([userLocation objectForKey:@"country_id"] == nil) || ([[userLocation objectForKey:@"country_id"]isEqual:[NSNull null]])) {
            NSLog(@"Empty");
            
            request = [NSMutableURLRequest
                       requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/allgrab_by_location"]];
            
//            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
//                           [userLocation objectForKey:@"Country"], @"CountryName",
//                           [userLocation objectForKey:@"City"], @"city_name",
//                           nil];
            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                           @"Kuwait", @"CountryName",
                           @"Sharaq", @"city_name",
                           nil];
        } else {
            
            request = [NSMutableURLRequest
                       requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/allgrab"]];
            requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                           [userLocation objectForKey:@"country_id"], @"country_id",
                           [userLocation objectForKey:@"city_id"], @"city_id",
                           nil];
        }
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
    }
    /////////////////////////// nsurl /////////////////////////
    
}

////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    responseData = [[NSMutableData alloc] init];
    self.moreGrabDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    self.moreGrabDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    //NSLog(@"%@",self.moreGrabDictionary);
    
    self.sectionList = [NSArray arrayWithObjects:@"MY HOME CITY",@"NATIONAL DEAL",@"GLOBAL DEAL", nil];
    self.sectionInfoArray = [[NSMutableArray alloc]init];
    self.globalList = [[NSMutableArray alloc]init];
    //NSLog(@"%@",[[self.todayDealDictionary objectForKey:@"Deals"]objectAtIndex:0]);
    if ([ApplicationDelegate isValid:[self.moreGrabDictionary objectForKey:@"global_deals"]])
    {
        NSMutableArray *dealArray = [[NSMutableArray alloc] init];
        MoreGrabDetails *grabDetails = [[MoreGrabDetails alloc] init];
        for (NSMutableDictionary *dictionary in [self.moreGrabDictionary objectForKey:@"global_deals"]) {
            MoreGrabCellDetails *grabCellDetails = [[MoreGrabCellDetails alloc] init];
            grabCellDetails = [ApplicationDelegate.mapper getMoreGrabDealListFromDictionary:dictionary];
            //NSLog(@"%@",grabCellDetails.dealID);
            [dealArray addObject:grabCellDetails];
        }
        grabDetails.sessionName = @"GLOBAL DEAL";
        grabDetails.dealArray = dealArray;
        [self.globalList addObject: grabDetails];
        //self.placeList = dealArray;
        
//        NSMutableArray *array = [[NSMutableArray alloc] init];
//        
//        for (MoreGrabDetails *grabDetails in self.globalList) {
//            SectionInfoMoreGrab *sectionMoreGrab = [[SectionInfoMoreGrab alloc] init];
//            sectionMoreGrab.moreGrabDetails = grabDetails;
//            sectionMoreGrab.open = YES;
//            
//            NSNumber *defaultHeight = [NSNumber numberWithInt:44];
//            NSInteger count = [[sectionMoreGrab.moreGrabDetails dealArray] count];
//            
//            for (NSInteger i= 0; i<count; i++) {
//                [sectionMoreGrab insertObject:defaultHeight inRowHeightsAtIndex:i];
//            }
//            
//            [array addObject:sectionMoreGrab];
//        }
//        //NSLog(@"%@",array);
//        self.sectionInfoArray = array;
        
    }
    if ([ApplicationDelegate isValid:[self.moreGrabDictionary objectForKey:@"national_deals"]])
    {
        NSMutableArray *dealArray = [[NSMutableArray alloc] init];
        MoreGrabDetails *grabDetails = [[MoreGrabDetails alloc] init];
        for (NSMutableDictionary *dictionary in [self.moreGrabDictionary objectForKey:@"national_deals"]) {
            MoreGrabCellDetails *grabCellDetails = [[MoreGrabCellDetails alloc] init];
            grabCellDetails = [ApplicationDelegate.mapper getMoreGrabDealListFromDictionary:dictionary];
            [dealArray addObject:grabCellDetails];
        }
        grabDetails.sessionName = @"NATIONAL DEAL";
        grabDetails.dealArray = dealArray;
        [self.globalList addObject: grabDetails];
    }
    if ([ApplicationDelegate isValid:[self.moreGrabDictionary objectForKey:@"city_deals"]])
    {
        NSMutableArray *dealArray = [[NSMutableArray alloc] init];
        MoreGrabDetails *grabDetails = [[MoreGrabDetails alloc] init];
        for (NSMutableDictionary *dictionary in [self.moreGrabDictionary objectForKey:@"city_deals"]) {
            MoreGrabCellDetails *grabCellDetails = [[MoreGrabCellDetails alloc] init];
            grabCellDetails = [ApplicationDelegate.mapper getMoreGrabDealListFromDictionary:dictionary];
            [dealArray addObject:grabCellDetails];
        }
        grabDetails.sessionName = @"MY HOME CITY";
        grabDetails.dealArray = dealArray;
        [self.globalList addObject: grabDetails];
    }
    //NSLog(@"%@",self.globalList);
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    for (MoreGrabDetails *grabDetails in self.globalList) {
        SectionInfoMoreGrab *sectionMoreGrab = [[SectionInfoMoreGrab alloc] init];
        sectionMoreGrab.moreGrabDetails = grabDetails;
        sectionMoreGrab.open = YES;
        
        NSNumber *defaultHeight = [NSNumber numberWithInt:44];
        NSInteger count = [[sectionMoreGrab.moreGrabDetails dealArray] count];
        
        for (NSInteger i= 0; i<count; i++) {
            [sectionMoreGrab insertObject:defaultHeight inRowHeightsAtIndex:i];
        }
        
        [array addObject:sectionMoreGrab];
    }
    //NSLog(@"%@",array);
    self.sectionInfoArray = array;
    [self.moreGrabTableView reloadData];
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////


@end
