//
//  MyCitiesEditViewController.m
//  WeGrab
//
//  Created by satheesh on 2/2/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "MyCitiesEditViewController.h"
#import "MyCitiesEditTableViewCell.h"
#import "CityDetails.h"

@interface MyCitiesEditViewController ()
@property (nonatomic, strong) NSArray * cityList;
@end

@implementation MyCitiesEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"My Cities";
    
    self.myCitiesEditTableView.scrollEnabled = YES;
    
    //self.openSectionIndex = NSNotFound;
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getMyCityData];
    }
    
    self.myCitiesEditTableView.sectionHeaderHeight = 45;
    self.myCitiesEditTableView.sectionFooterHeight = 0;
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    //self.openSectionIndex = NSNotFound;
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)getMyCityData{
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/get_my_cities"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //NSLog(@"%@",self.cityList);
    return [self.cityList count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    MyCitiesEditTableViewCell *cell = (MyCitiesEditTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyCitiesEditTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CityDetails * tempCityDetails;
    tempCityDetails = [self.cityList objectAtIndex:indexPath.row];
    
    NSMutableDictionary *CitydetailsDictionary = [[NSMutableDictionary alloc]init];
    NSLog(@"%@",tempCityDetails.cityName);
    CitydetailsDictionary = tempCityDetails.cityName;
    //NSLog(@"%@",CitydetailsDictionary);
    //NSLog(@"%ld",(long)indexPath.section);
    //NSLog(@"%@",self.myOtherCityArray);
    cell.countryCityNameLabel.text = tempCityDetails.cityName;
    if ([[self.myOtherCityArray objectAtIndex:indexPath.row] isEqualToString:@"0"]) {
        [cell.radioImageView setImage:[UIImage imageNamed:@"round.png"]];
    } else {
        [cell.radioImageView setImage:[UIImage imageNamed:@"round_sel.png"]];
    }
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    for (int i =0; i< self.myOtherCityArray.count; i++) {
        if (indexPath.row == i) {
            [self.myOtherCityArray setObject:@"1" atIndexedSubscript:i];
        } else {
            [self.myOtherCityArray setObject:@"0" atIndexedSubscript:i];
        }
    }
    
    [self.myCitiesEditTableView reloadData];
}

- (IBAction)deleteBtnActn:(UIButton *)sender {
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/delete_my_cities"]];
        NSLog(@"%@",self.myOtherCityArray);
        NSString * tempCityId = [[NSString alloc]init];
        tempCityId = nil;
        for (int i=0; i<self.myOtherCityArray.count; i++) {
            if ([[self.myOtherCityArray objectAtIndex:i] isEqualToString:@"1"]) {
                CityDetails * tempCityDetails;
                tempCityDetails = [self.cityList objectAtIndex:i];
                tempCityId = tempCityDetails.cityId;
                break;
            }
        }
        if ([ApplicationDelegate isValid:tempCityId]) {
            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         tempCityId, @"city_mc_id",
                                         nil];
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            connectionDeleteMyCity = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        } else {
            [ApplicationDelegate showAlertWithMessage:@"Sorry, please select a city to delete" title:nil];
            [ApplicationDelegate removeProgressHUD];
        }
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (IBAction)cancelBtnActn:(UIButton *)sender {
    for (int i=0; i<self.myOtherCityArray.count; i++) {
        [self.myOtherCityArray setObject:@"0" atIndexedSubscript:i];
    }
    [self.myCitiesEditTableView reloadData];
}



- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionDeleteMyCity) {
        responseDataMyCitiesDelete = [[NSMutableData alloc] init];
        myCitiesDeleteDataDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseData = [[NSMutableData alloc] init];
        myCitiesDataDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    if (connection == connectionDeleteMyCity) {
        [responseDataMyCitiesDelete appendData:data];
    } else {
        [responseData appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    if (connection == connectionDeleteMyCity) {
        myCitiesDeleteDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataMyCitiesDelete options:kNilOptions error:&error];
        NSLog(@"%@",myCitiesDeleteDataDictionary);
        
        //self.myOtherCityArray = [[NSMutableArray alloc] initWithCapacity:[[myCitiesDataDictionary objectForKey:@"city_list"] count]];
        [ApplicationDelegate showAlertWithMessage:[myCitiesDeleteDataDictionary objectForKey:@"message"] title:nil];
        
        [ApplicationDelegate removeProgressHUD];
        
        [self getMyCityData];
        
    } else {
        myCitiesDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
        NSLog(@"%@",myCitiesDataDictionary);
        
        self.myOtherCityArray = [[NSMutableArray alloc] initWithCapacity:[[myCitiesDataDictionary objectForKey:@"city_list"] count]];
        
        if ([[myCitiesDataDictionary objectForKey:@"city_list"] count] > 0) {
            
            NSMutableArray *citiesArray = [[NSMutableArray alloc] initWithCapacity:[[myCitiesDataDictionary objectForKey:@"city_list"] count]];
            NSLog(@"%@",[myCitiesDataDictionary objectForKey:@"city_list"]);
            for (NSDictionary *dictionary in [myCitiesDataDictionary objectForKey:@"city_list"]) {
                CityDetails *cityDetails = [[CityDetails alloc] init];
                cityDetails.cityName = [dictionary objectForKey:@"city_name"];
                cityDetails.cityId = [dictionary objectForKey:@"mc_id:"];
                //NSLog(@"%@",cityDetails.cityId);
                [self.myOtherCityArray addObject:@"0"];
                [citiesArray addObject:cityDetails];
            }
            self.cityList = citiesArray;
            
            [self.myCitiesEditTableView reloadData];
            
            [ApplicationDelegate removeProgressHUD];
        }
    }
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

@end
