//
//  BillingViewController.m
//  WeGrab
//
//  Created by Ronish on 2/8/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "BillingViewController.h"
#import "BillingTableCell.h"
#import "BillingDetails.h"

@interface BillingViewController ()

@property (nonatomic, strong) NSArray * billingList;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSMutableArray *cardNumberArray;

@end

@implementation BillingViewController

@synthesize billingList;
@synthesize sectionInfoArray;
@synthesize cardNumberArray;

-(void)viewWillAppear:(BOOL)animated{
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getMyBillingData];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.selectedCardId = [[NSString alloc]init];
    self.selectedCityId = [[NSString alloc]init];
    self.selectedCountryId = [[NSString alloc]init];
    self.cardDropDownNameList = [[NSMutableArray alloc]initWithObjects:@"Visa",@"Knet",@"Master Card",@"Platinum", nil];
    self.cardDropDownIdList = [[NSMutableArray alloc]initWithObjects:@"1",@"2",@"3",@"5", nil];
    dropDownTag = 0;
    self.billingTableView.scrollEnabled = NO;
    self.billingTableView.sectionHeaderHeight = 45;
    self.billingTableView.sectionFooterHeight = 0;
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark - Initialization

-(void)getMyBillingData{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/billing_index"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionInitial = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.billingList.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier = @"Cell";
    
    BillingTableCell *cell = (BillingTableCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"BillingTableCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    BillingDetails *billingDetails = [[BillingDetails alloc] init];
    billingDetails = [self.billingList objectAtIndex:indexPath.section];
    cell.cardTypeTextView.text = [billingDetails.card_Details objectForKey:@"Card Type"];
    cell.nameTextView.text = [billingDetails.card_Details objectForKey:@"Card Holder Name"];
    cell.cardNumberTextView.text = billingDetails.card_No;
    cell.expiryDateTextView.text = [billingDetails.card_Details objectForKey:@"Expiry Date"];
    cell.makeThisDefaultBtn.tag = indexPath.section;
    cell.deleteBtn.tag = indexPath.section;
    [cell.makeThisDefaultBtn addTarget:self action:@selector(makeThisDefaultBtnActn:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteBtn addTarget:self action:@selector(deleteBtnActn:) forControlEvents:UIControlEventTouchUpInside];
    if ([billingDetails.default_Card isEqualToString:@"true"]) {
        cell.makeThisDefaultBtn.selected = YES;
    }
    else{
        cell.makeThisDefaultBtn.selected = NO;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        return 381;
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    BillingDetails * billing = (BillingDetails *)[self.billingList objectAtIndex:section];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.billingTableView.frame.size.width,40)];
    sectionView.tag=section;
    
    UIImageView * backGroundView = [[UIImageView alloc]init];
    backGroundView.frame = sectionView.bounds;
    backGroundView.backgroundColor = [UIColor darkGrayColor];
    [sectionView addSubview:backGroundView];
    UIImageView * seperatorView = [[UIImageView alloc]init];
    seperatorView.frame = CGRectMake(backGroundView.frame.origin.x, backGroundView.frame.size.height-1, backGroundView.frame.size.width,1);
    seperatorView.backgroundColor = [UIColor lightGrayColor];
    [sectionView addSubview:seperatorView];
    
    CGRect LabelFrame = sectionView.bounds;
    LabelFrame.size.width -= 30;
    LabelFrame.origin.x += 10;
    CGRectInset(LabelFrame, 0.0, 5.0);
    
    UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame];
    label.text = [NSString stringWithFormat:@"%@",billing.card_No];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentLeft;
    [sectionView addSubview:label];
    //self.sectionTitle = label;
    
    CGRect buttonFrame = CGRectMake(LabelFrame.size.width, 0, 20, LabelFrame.size.height);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = buttonFrame;
    [button setImage:[UIImage imageNamed:@"rightarrow1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"downarrow1.png"] forState:UIControlStateSelected];
    BOOL selectionStatus  = [[arrayForBool objectAtIndex:section] boolValue];
    if (selectionStatus) {
        button.selected = YES;
    }
    else{
        button.selected = NO;
    }
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button addTarget:self action:@selector(sectionHeaderTapped:) forControlEvents:UIControlEventTouchUpInside];
    //button.selected = YES;
    [sectionView addSubview:button];
    //self.discButton = button;
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    return  sectionView;
}


#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    self.createNewCardBtn.selected = NO;
    [self.createNewCardArrowView setImage:[UIImage imageNamed:@"rightarrow1.png"]];
    self.createNewCardDetailsBackView.hidden = YES;
    //self.citySelectionListArray = [[NSMutableArray alloc]init];
    //self.tickStatus = @"NO";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    NSLog(@"%@",indexPath);
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        NSLog(@"%hhd",collapsed);
        for (int i=0; i<([self.billingList count]); i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            else{
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
            }
        }
        [self.billingTableView reloadData];
        [self.billingTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        CGRect frame = self.billingTableView.frame;
        frame.size.height = self.billingTableView.contentSize.height;
        self.billingTableView.frame = frame;
        
        frame = self.createNewCardBtnBackView.frame;
        frame.origin.y = self.billingTableView.frame.origin.y + self.billingTableView.frame.size.height;
        self.createNewCardBtnBackView.frame = frame;
        
        frame = self.createNewCardDetailsBackView.frame;
        frame.origin.y = self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height;
        self.createNewCardDetailsBackView.frame = frame;
        
        self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height);
    }
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    if (connection == connectionInitial) {
        responseDataInitial = [[NSMutableData alloc] init];
        self.billingInformationDictionary = [[NSMutableDictionary alloc]init];
    } else if (connection == connectionCountryList){
        responseDataCountryList = [[NSMutableData alloc] init];
        self.countryDataDictionary = [[NSMutableDictionary alloc]init];
    }else if (connection == connectionCityList){
        responseDataCityList = [[NSMutableData alloc] init];
        self.cityDataDictionary = [[NSMutableDictionary alloc]init];
    }else if (connection == connectionDefaultCardReset){
        responseDataDefaultCardReset = [[NSMutableData alloc] init];
    }else if (connection == connectionDeleteCard){
        responseDataDeleteCard = [[NSMutableData alloc] init];
    }else if (connection == connectionAddNewCard){
        responseDataAddNewCard = [[NSMutableData alloc] init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    if (connection == connectionInitial) {
        [responseDataInitial appendData:data];
    } else if (connection == connectionCountryList){
        [responseDataCountryList appendData:data];
    }else if (connection == connectionCityList){
        [responseDataCityList appendData:data];
    }else if (connection == connectionDefaultCardReset){
        [responseDataDefaultCardReset appendData:data];
    }else if (connection == connectionDeleteCard){
        [responseDataDeleteCard appendData:data];
    }else if (connection == connectionAddNewCard){
        [responseDataAddNewCard appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (connection == connectionInitial) {
        NSError* error;
        if ([ApplicationDelegate isValid:[[NSJSONSerialization JSONObjectWithData:responseDataInitial options:kNilOptions error:&error]objectForKey:@"Billing_Information"]]) {
            NSLog(@"%@",[[NSJSONSerialization JSONObjectWithData:responseDataInitial options:kNilOptions error:&error]objectForKey:@"Billing_Information"]);
            [self.billingInformationDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseDataInitial options:kNilOptions error:&error]objectForKey:@"Billing_Information"] forKey:@"Billing_Information"];
            NSLog(@"%@",[self.billingInformationDictionary objectForKey:@"Billing_Information"]);
            
            if ([[self.billingInformationDictionary objectForKey:@"Billing_Information"] count] > 0) {
                
                NSMutableArray *billingArray = [[NSMutableArray alloc] initWithCapacity:[[self.billingInformationDictionary objectForKey:@"Billing_Information"] count]];
                for (NSDictionary *dictionary in [self.billingInformationDictionary objectForKey:@"Billing_Information"]) {
                    BillingDetails *billingDetails = [[BillingDetails alloc] init];
                    billingDetails.card_No = [dictionary objectForKey:@"Card No"];
                    billingDetails.card_Id = [dictionary objectForKey:@"Card Id"];
                    billingDetails.default_Card = [dictionary objectForKey:@"Default"];
                    billingDetails.card_Details = [dictionary objectForKey:@"CardDetails"];
                    [billingArray addObject:billingDetails];
                }
                self.billingList = billingArray;
                
                if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.billingTableView])) {
                    NSMutableArray *array = [[NSMutableArray alloc] init];
                    arrayForBool=[[NSMutableArray alloc]init];
                    self.cardNumberArray = [[NSMutableArray alloc]init];
                    for (BillingDetails *billing in self.billingList) {
                        [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                        [array addObject:billing];
                    }
                    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                    self.sectionInfoArray = array;
                }
                [self getCountryListData];
                [self.billingTableView reloadData];
                
                CGRect frame = self.billingTableView.frame;
                frame.size.height = self.billingTableView.contentSize.height;
                self.billingTableView.frame = frame;
                
                frame = self.createNewCardBtnBackView.frame;
                frame.origin.y = self.billingTableView.frame.origin.y + self.billingTableView.frame.size.height;
                self.createNewCardBtnBackView.frame = frame;
                
                frame = self.createNewCardDetailsBackView.frame;
                frame.origin.y = self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height;
                self.createNewCardDetailsBackView.frame = frame;
                
                self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height);
                
                [ApplicationDelegate removeProgressHUD];
            }
        }
        [ApplicationDelegate removeProgressHUD];
    } else if (connection == connectionCountryList){
        NSError* error;
        NSLog(@"%@",[[NSJSONSerialization JSONObjectWithData:responseDataCountryList options:kNilOptions error:&error]objectForKey:@"CountryList"]);
        [self.countryDataDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseDataCountryList options:kNilOptions error:&error]objectForKey:@"CountryList"] forKey:@"CountryList"];
        NSLog(@"%@",[self.countryDataDictionary objectForKey:@"CountryList"]);
        
        if ([[self.countryDataDictionary objectForKey:@"CountryList"] count] > 0) {
            
            NSMutableArray *countryNameArray = [[NSMutableArray alloc] initWithCapacity:[[self.countryDataDictionary objectForKey:@"CountryList"] count]];
            NSMutableArray *countryIdArray = [[NSMutableArray alloc] initWithCapacity:[[self.countryDataDictionary objectForKey:@"CountryList"] count]];
            for (NSDictionary *dictionary in [self.countryDataDictionary objectForKey:@"CountryList"]) {
                [countryNameArray addObject:[dictionary objectForKey:@"country_name"]];
                 [countryIdArray addObject:[dictionary objectForKey:@"country_id"]];
            }
            self.countryDropDownIdList = countryIdArray;
            self.countryDropDownNameList = countryNameArray;
            
            [ApplicationDelegate removeProgressHUD];
        }
        
        [ApplicationDelegate removeProgressHUD];
        
    }else if (connection == connectionCityList){
        NSError* error;
        if ([ApplicationDelegate isValid:[[NSJSONSerialization JSONObjectWithData:responseDataCityList options:kNilOptions error:&error]objectForKey:@"CityList"]]) {
            //NSLog(@"%@",[[NSJSONSerialization JSONObjectWithData:responseDataCityList options:kNilOptions error:&error]objectForKey:@"CityList"]);
            [self.cityDataDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseDataCityList options:kNilOptions error:&error]objectForKey:@"CityList"] forKey:@"CityList"];
            NSLog(@"%@",[self.cityDataDictionary objectForKey:@"CityList"]);
            
            if ([[self.cityDataDictionary objectForKey:@"CityList"] count] > 0) {
                
                NSMutableArray *cityNameArray = [[NSMutableArray alloc] initWithCapacity:[[self.countryDataDictionary objectForKey:@"CityList"] count]];
                NSMutableArray *cityIdArray = [[NSMutableArray alloc] initWithCapacity:[[self.countryDataDictionary objectForKey:@"CityList"] count]];
                for (NSDictionary *dictionary in [self.cityDataDictionary objectForKey:@"CityList"]) {
                    //NSLog(@"%@",dictionary);
                    [cityNameArray addObject:[[dictionary objectForKey:@"Cities"] objectForKey:@"city_name"]];
                    [cityIdArray addObject:[[dictionary objectForKey:@"Cities"] objectForKey:@"city_id"]];
                }
                self.cityDropDownIdList = cityIdArray;
                self.cityDropDownNameList = cityNameArray;
                //NSLog(@"%@",self.cityDropDownNameList);
                [ApplicationDelegate removeProgressHUD];
            }
        }
        [ApplicationDelegate removeProgressHUD];
        
    }else if (connection == connectionDefaultCardReset){
        NSError* error;
        NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:responseDataDefaultCardReset options:kNilOptions error:&error]);
        [ApplicationDelegate removeProgressHUD];
        [self getMyBillingData];
    }else if (connection == connectionDeleteCard){
        NSError* error;
        NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:responseDataDeleteCard options:kNilOptions error:&error]);
        [ApplicationDelegate removeProgressHUD];
        [self getMyBillingData];
    }else if (connection == connectionAddNewCard){
        NSError* error;
        NSLog(@"%@",[NSJSONSerialization JSONObjectWithData:responseDataAddNewCard options:kNilOptions error:&error]);
        [ApplicationDelegate removeProgressHUD];
        [self getMyBillingData];
    }
    [ApplicationDelegate removeProgressHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGRect frame = self.billingTableView.frame;
    frame.size.height = self.billingTableView.contentSize.height;
    self.billingTableView.frame = frame;
    
    frame = self.createNewCardBtnBackView.frame;
    frame.origin.y = self.billingTableView.frame.origin.y + self.billingTableView.frame.size.height;
    self.createNewCardBtnBackView.frame = frame;
    
    frame = self.createNewCardDetailsBackView.frame;
    frame.origin.y = self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height;
    self.createNewCardDetailsBackView.frame = frame;
    
    if (self.createNewCardDetailsBackView.hidden) {
        self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height);
    } else {
        self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardDetailsBackView.frame.origin.y + self.createNewCardDetailsBackView.frame.size.height);
    }
    
}

- (IBAction)createNewCardBtnActn:(UIButton *)sender {
    
    for (int i=0; i<([self.billingList count]); i++) {
        [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
    }
    [self.billingTableView reloadData];
    
    CGRect frame = self.billingTableView.frame;
    frame.size.height = self.billingTableView.contentSize.height;
    self.billingTableView.frame = frame;
    
    frame = self.createNewCardBtnBackView.frame;
    frame.origin.y = self.billingTableView.frame.origin.y + self.billingTableView.frame.size.height;
    self.createNewCardBtnBackView.frame = frame;
    
    frame = self.createNewCardDetailsBackView.frame;
    frame.origin.y = self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height;
    self.createNewCardDetailsBackView.frame = frame;
    
    self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height);
    
    if (sender.selected) {
        self.createNewCardBtn.selected = NO;
        [self.createNewCardArrowView setImage:[UIImage imageNamed:@"rightarrow1.png"]];
        self.createNewCardDetailsBackView.hidden = YES;
        self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardBtnBackView.frame.origin.y + self.createNewCardBtnBackView.frame.size.height);
    } else {
        self.createNewCardBtn.selected = YES;
        [self.createNewCardArrowView setImage:[UIImage imageNamed:@"downarrow1.png"]];
        self.createNewCardDetailsBackView.hidden = NO;
        self.billingMainScrollView.contentSize = CGSizeMake(self.billingMainScrollView.frame.size.width, self.createNewCardDetailsBackView.frame.origin.y + self.createNewCardDetailsBackView.frame.size.height);
    }
}
- (IBAction)dropDownBtnActn:(UIButton *)sender {
    if (sender.tag == 1) {
        
        if (self.dropDownCardBtn.selected) {
            [UIView animateWithDuration:0.4f animations:^{
                self.cardDropDownObj.view.frame =
                CGRectMake(self.cardDropDownObj.view.frame.origin.x,
                           self.cardDropDownObj.view.frame.origin.y,
                           self.cardDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.cardDropDownObj.view removeFromSuperview];
                }
                
            }];
            
            self.dropDownCardBtn.selected = NO;
            self.cardUpArrow.hidden = YES;
            self.cardDownArrow.hidden = NO;
        }
        
        //    if (self.dropDownCountryBtn.selected) {
        //        [UIView animateWithDuration:0.4f animations:^{
        //            self.countryDropDownObj.view.frame =
        //            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
        //                       self.countryDropDownObj.view.frame.origin.y,
        //                       self.countryDropDownObj.view.frame.size.width,
        //                       0);
        //        } completion:^(BOOL finished) {
        //            if (finished) {
        //                [self.countryDropDownObj.view removeFromSuperview];
        //            }
        //
        //        }];
        //
        //        self.dropDownCountryBtn.selected = NO;
        //        self.countryUpArrow.hidden = YES;
        //        self.countryDownArrow.hidden = NO;
        //    }
        if (self.dropDownCityBtn.selected) {
            [UIView animateWithDuration:0.4f animations:^{
                self.cityDropDownObj.view.frame =
                CGRectMake(self.cityDropDownObj.view.frame.origin.x,
                           self.cityDropDownObj.view.frame.origin.y,
                           self.cityDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.cityDropDownObj.view removeFromSuperview];
                }
                
            }];
            
            self.dropDownCityBtn.selected = NO;
            self.upArrowCity.hidden = YES;
            self.downArrowCity.hidden = NO;
        }
        
        dropDownTag = 1;
        self.dropDownCountryBtn.selected = !self.dropDownCountryBtn.selected;
        if (self.dropDownCountryBtn.selected)
        {
            self.countryDownArrow.hidden = YES;
            self.countryUpArrow.hidden = NO;
            if (self.countryDropDownObj.view.superview) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
            self.countryDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
            
            //////////// Customize the dropDown view Frame as your requirement
            
            self.countryDropDownObj.cellHeight = 40.0f;
            
            //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
            //            {
            self.countryDropDownObj.view.frame = CGRectMake((self.countryTxtView.frame.origin.x),(self.countryTxtView.frame.origin.y+self.countryTxtView.frame.size.height), (self.countryTxtView.frame.size.width), 0);
            
            self.countryDropDownObj.dropDownDelegate = self;
            self.countryDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:self.countryDropDownNameList];
            
            self.countryDropDownObj.view.layer.borderWidth = 0.1;
            self.countryDropDownObj.view.layer.shadowOpacity = 1.0;
            self.countryDropDownObj.view.layer.shadowRadius = 5.0;
            self.countryDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
            
            self.countryDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
            self.countryDropDownObj.textLabelColor = [UIColor blackColor];
            self.countryDropDownObj.view.backgroundColor = [UIColor whiteColor];
            
            if (self.countryDropDownNameList.count>0)
            {
                [self.dropDownCountryBtn.superview addSubview:self.countryDropDownObj.view];
            }
            else
            {
                self.dropDownCountryBtn.selected=NO;
            }
            
            /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
            CGFloat tableHT = 120.0f;
            
            if ((self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)<tableHT)
            {
                tableHT = (self.countryDropDownObj.cellHeight*self.countryDropDownObj.dataArray.count)+18.0f;
            }
            //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
            
            [UIView animateWithDuration:0.4f animations:^{
                self.countryDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                self.countryDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                self.countryDropDownObj.view.frame =
                CGRectMake(self.countryDropDownObj.view.frame.origin.x+2,
                           self.countryDropDownObj.view.frame.origin.y,
                           self.countryDropDownObj.view.frame.size.width-4,
                           tableHT);
            }];
        }
        else
        {
            self.countryUpArrow.hidden = YES;
            self.countryDownArrow.hidden = NO;
            self.countryDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
            self.countryDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
            
            ////////// ReEnabling the UserInteraction of all SubViews/////////
            for (UIView *vw in self.view.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
            ///////////////////
            
            [UIView animateWithDuration:0.4f animations:^{
                self.countryDropDownObj.view.frame =
                CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                           self.countryDropDownObj.view.frame.origin.y,
                           self.countryDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.countryDropDownObj.view removeFromSuperview];
                }
                
            }];
        }
    } else if(sender.tag == 2){
        
        if (self.cityDropDownNameList.count == 0) {
            [ApplicationDelegate showAlertWithMessage:@"City list is not currently available" title:@"Sorry!!"];
        } else {
            if (self.dropDownCardBtn.selected) {
                [UIView animateWithDuration:0.4f animations:^{
                    self.cardDropDownObj.view.frame =
                    CGRectMake(self.cardDropDownObj.view.frame.origin.x,
                               self.cardDropDownObj.view.frame.origin.y,
                               self.cardDropDownObj.view.frame.size.width,
                               0);
                } completion:^(BOOL finished) {
                    if (finished) {
                        [self.cardDropDownObj.view removeFromSuperview];
                    }
                    
                }];
                
                self.dropDownCardBtn.selected = NO;
                self.cardUpArrow.hidden = YES;
                self.cardDownArrow.hidden = NO;
            }
            if (self.dropDownCountryBtn.selected) {
                [UIView animateWithDuration:0.4f animations:^{
                    self.countryDropDownObj.view.frame =
                    CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                               self.countryDropDownObj.view.frame.origin.y,
                               self.countryDropDownObj.view.frame.size.width,
                               0);
                } completion:^(BOOL finished) {
                    if (finished) {
                        [self.countryDropDownObj.view removeFromSuperview];
                    }
                    
                }];
                
                self.dropDownCountryBtn.selected = NO;
                self.countryUpArrow.hidden = YES;
                self.countryDownArrow.hidden = NO;
            }
            dropDownTag = 2;
            self.dropDownCityBtn.selected = !self.dropDownCityBtn.selected;
            if (self.dropDownCityBtn.selected)
            {
                self.downArrowCity.hidden = YES;
                self.upArrowCity.hidden = NO;
                if (self.cityDropDownObj.view.superview) {
                    [self.cityDropDownObj.view removeFromSuperview];
                }
                
                self.cityDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
                
                //////////// Customize the dropDown view Frame as your requirement
                
                self.cityDropDownObj.cellHeight = 40.0f;
                
                //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
                //            {
                self.cityDropDownObj.view.frame = CGRectMake((self.cityTxtView.frame.origin.x),(self.cityTxtView.frame.origin.y+self.cityTxtView.frame.size.height), (self.cityTxtView.frame.size.width), 0);
                
                self.cityDropDownObj.dropDownDelegate = self;
                self.cityDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:self.cityDropDownNameList];
                
                self.cityDropDownObj.view.layer.borderWidth = 0.1;
                self.cityDropDownObj.view.layer.shadowOpacity = 1.0;
                self.cityDropDownObj.view.layer.shadowRadius = 5.0;
                self.cityDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
                
                self.cityDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
                self.cityDropDownObj.textLabelColor = [UIColor blackColor];
                self.cityDropDownObj.view.backgroundColor = [UIColor whiteColor];
                
                if (self.cityDropDownNameList.count>0)
                {
                    [self.dropDownCityBtn.superview addSubview:self.cityDropDownObj.view];
                }
                else
                {
                    self.dropDownCityBtn.selected=NO;
                }
                
                /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
                CGFloat tableHT = 120.0f;
                
                if ((self.cityDropDownObj.cellHeight*self.cityDropDownObj.dataArray.count)<tableHT)
                {
                    tableHT = (self.cityDropDownObj.cellHeight*self.cityDropDownObj.dataArray.count)+18.0f;
                }
                //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
                
                [UIView animateWithDuration:0.4f animations:^{
                    self.cityDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                    self.cityDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                    self.cityDropDownObj.view.frame =
                    CGRectMake(self.cityDropDownObj.view.frame.origin.x+2,
                               self.cityDropDownObj.view.frame.origin.y,
                               self.cityDropDownObj.view.frame.size.width-4,
                               tableHT);
                }];
            }
            else
            {
                self.upArrowCity.hidden = YES;
                self.downArrowCity.hidden = NO;
                self.cityDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
                self.cityDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
                
                ////////// ReEnabling the UserInteraction of all SubViews/////////
                for (UIView *vw in self.view.subviews) {
                    [vw setUserInteractionEnabled:YES];
                }
                ///////////////////
                
                [UIView animateWithDuration:0.4f animations:^{
                    self.cityDropDownObj.view.frame =
                    CGRectMake(self.cityDropDownObj.view.frame.origin.x,
                               self.cityDropDownObj.view.frame.origin.y,
                               self.cityDropDownObj.view.frame.size.width,
                               0);
                } completion:^(BOOL finished) {
                    if (finished) {
                        [self.cityDropDownObj.view removeFromSuperview];
                    }
                    
                }];
            }
        }
    }else if(sender.tag == 3){
        
        if (self.dropDownCountryBtn.selected) {
            [UIView animateWithDuration:0.4f animations:^{
                self.countryDropDownObj.view.frame =
                CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                           self.countryDropDownObj.view.frame.origin.y,
                           self.countryDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.countryDropDownObj.view removeFromSuperview];
                }
                
            }];
            
            self.dropDownCountryBtn.selected = NO;
            self.countryUpArrow.hidden = YES;
            self.countryDownArrow.hidden = NO;
        }
        
        if (self.dropDownCityBtn.selected) {
            [UIView animateWithDuration:0.4f animations:^{
                self.cityDropDownObj.view.frame =
                CGRectMake(self.cityDropDownObj.view.frame.origin.x,
                           self.cityDropDownObj.view.frame.origin.y,
                           self.cityDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.cityDropDownObj.view removeFromSuperview];
                }
                
            }];
            
            self.dropDownCityBtn.selected = NO;
            self.upArrowCity.hidden = YES;
            self.downArrowCity.hidden = NO;
        }
        
        dropDownTag = 3;
        self.dropDownCardBtn.selected = !self.dropDownCardBtn.selected;
        if (self.dropDownCardBtn.selected)
        {
            self.cardDownArrow.hidden = YES;
            self.cardUpArrow.hidden = NO;
            if (self.cardDropDownObj.view.superview) {
                [self.cardDropDownObj.view removeFromSuperview];
            }
            
            self.cardDropDownObj= [[DropDownView alloc] initWithNibName:@"DropDownView" bundle:nil];
            
            //////////// Customize the dropDown view Frame as your requirement
            
            self.cardDropDownObj.cellHeight = 40.0f;
            
            //            if ([ApplicationDelegate.language isEqualToString:kENGLISH])
            //            {
            self.cardDropDownObj.view.frame = CGRectMake((self.cardTxtView.frame.origin.x),(self.cardTxtView.frame.origin.y+self.cardTxtView.frame.size.height), (self.cardTxtView.frame.size.width), 0);
            
            self.cardDropDownObj.dropDownDelegate = self;
            self.cardDropDownObj.dataArray = [[NSMutableArray alloc] initWithArray:self.cardDropDownNameList];
            
            self.cardDropDownObj.view.layer.borderWidth = 0.1;
            self.cardDropDownObj.view.layer.shadowOpacity = 1.0;
            self.cardDropDownObj.view.layer.shadowRadius = 5.0;
            self.cardDropDownObj.view.layer.shadowOffset = CGSizeMake(2, 2);
            
            self.cardDropDownObj.textLabelFont= [UIFont fontWithName:@"BREESERIF-REGULAR" size:13.0f];
            self.cardDropDownObj.textLabelColor = [UIColor blackColor];
            self.cardDropDownObj.view.backgroundColor = [UIColor whiteColor];
            
            if (self.cardDropDownNameList.count>0)
            {
                [self.dropDownCardBtn.superview addSubview:self.cardDropDownObj.view];
            }
            else
            {
                self.dropDownCardBtn.selected=NO;
            }
            
            /////// ADJUST DROPDOWN HEIGHT DYNAMICALLY
            CGFloat tableHT = 120.0f;
            
            if ((self.cardDropDownObj.cellHeight*self.cardDropDownObj.dataArray.count)<tableHT)
            {
                tableHT = (self.cardDropDownObj.cellHeight*self.cardDropDownObj.dataArray.count)+18.0f;
            }
            //     [self.restaurantDetailsContainerScroll setContentOffset:CGPointMake(self.restaurantDetailsContainerScroll.contentOffset.x, (self.areaDropDownObj.view.frame.origin.y-tableHT+20)) animated:YES];
            
            [UIView animateWithDuration:0.4f animations:^{
                self.cardDropDownObj.view.layer.borderColor = [UIColor blackColor].CGColor;
                self.cardDropDownObj.view.layer.shadowColor = [UIColor grayColor].CGColor;
                self.cardDropDownObj.view.frame =
                CGRectMake(self.cardDropDownObj.view.frame.origin.x+2,
                           self.cardDropDownObj.view.frame.origin.y,
                           self.cardDropDownObj.view.frame.size.width-4,
                           tableHT);
            }];
        }
        else
        {
            self.cardUpArrow.hidden = YES;
            self.cardDownArrow.hidden = NO;
            self.cardDropDownObj.view.layer.borderColor = [UIColor clearColor].CGColor;
            self.cardDropDownObj.view.layer.shadowColor = [UIColor clearColor].CGColor;
            
            ////////// ReEnabling the UserInteraction of all SubViews/////////
            for (UIView *vw in self.view.subviews) {
                [vw setUserInteractionEnabled:YES];
            }
            ///////////////////
            
            [UIView animateWithDuration:0.4f animations:^{
                self.cardDropDownObj.view.frame =
                CGRectMake(self.cardDropDownObj.view.frame.origin.x,
                           self.cardDropDownObj.view.frame.origin.y,
                           self.cardDropDownObj.view.frame.size.width,
                           0);
            } completion:^(BOOL finished) {
                if (finished) {
                    [self.cardDropDownObj.view removeFromSuperview];
                }
                
            }];
        }
    }
}

-(void) getCountryListData{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/country_list"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        //                             @"3", @"user_id",
        //                             nil];
        
        //NSError *error;
        //NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        //[request setHTTPBody:postData];
        connectionCountryList = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}


-(void)selectList:(int)selectedIndex
{
    if (dropDownTag == 1) {
        [UIView animateWithDuration:0.4f animations:^{
            self.countryDropDownObj.view.frame =
            CGRectMake(self.countryDropDownObj.view.frame.origin.x,
                       self.countryDropDownObj.view.frame.origin.y,
                       self.countryDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.countryDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.dropDownCountryBtn.selected = NO;
        self.countryUpArrow.hidden = YES;
        self.countryDownArrow.hidden = NO;
        self.countryTxtView.text = [self.countryDropDownNameList objectAtIndex:selectedIndex];
        self.cityTxtView.text = @"";
        [self getCityListData:[self.countryDropDownIdList objectAtIndex:selectedIndex]];
        self.selectedCountryId = [self.countryDropDownIdList objectAtIndex:selectedIndex];
        //brandId = [[NSString alloc]initWithString:[brandCodeArray objectAtIndex:selectedIndex]];
    } else if (dropDownTag == 2) {
        [UIView animateWithDuration:0.4f animations:^{
            self.cityDropDownObj.view.frame =
            CGRectMake(self.cityDropDownObj.view.frame.origin.x,
                       self.cityDropDownObj.view.frame.origin.y,
                       self.cityDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cityDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.dropDownCityBtn.selected = NO;
        self.upArrowCity.hidden = YES;
        self.downArrowCity.hidden = NO;
        self.cityTxtView.text =[self.cityDropDownNameList objectAtIndex:selectedIndex];
        self.selectedCityId = [self.cityDropDownIdList objectAtIndex:selectedIndex];
    }else if (dropDownTag == 3) {
        [UIView animateWithDuration:0.4f animations:^{
            self.cardDropDownObj.view.frame =
            CGRectMake(self.cardDropDownObj.view.frame.origin.x,
                       self.cardDropDownObj.view.frame.origin.y,
                       self.cardDropDownObj.view.frame.size.width,
                       0);
        } completion:^(BOOL finished) {
            if (finished) {
                [self.cardDropDownObj.view removeFromSuperview];
            }
            
        }];
        
        self.dropDownCardBtn.selected = NO;
        self.cardUpArrow.hidden = YES;
        self.cardDownArrow.hidden = NO;
        self.cardTxtView.text = [self.cardDropDownNameList objectAtIndex:selectedIndex];
        self.selectedCardId = [self.cardDropDownIdList objectAtIndex:selectedIndex];
        //self.cardTxtView.text = @"";
        //[self getCityListData:[self.cardDropDownIdList objectAtIndex:selectedIndex]];
        //brandId = [[NSString alloc]initWithString:[brandCodeArray objectAtIndex:selectedIndex]];
    }
}

-(void) getCityListData :(NSString *)countryId{
    //NSLog(@"%@",countryId);
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/city_list"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     countryId, @"country_id",
                                     nil];
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionCityList = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (void) makeThisDefaultBtnActn:(UIButton *)sender{
    NSLog(@"%d",sender.tag);
    if (sender.selected) {
        //NSLog(@"selected");
    } else {
        //NSLog(@"Notselected");
        BillingDetails *billingDetails = [[BillingDetails alloc] init];
        billingDetails = [self.billingList objectAtIndex:sender.tag];
        [self updateDefaultCard:billingDetails.card_Id];
    }
}

- (void) deleteBtnActn:(UIButton *)sender{
    //NSLog(@"%d",sender.tag);
    BillingDetails *billingDetails = [[BillingDetails alloc] init];
    billingDetails = [self.billingList objectAtIndex:sender.tag];
    [self deleteCard:billingDetails.card_Id];
}
-(void) updateDefaultCard:(NSString *)cardId{
    NSLog(@"%@",cardId);
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/billing_default"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     cardId, @"cardid",
                                     @"true",@"default",
                                     nil];
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionDefaultCardReset = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

-(void) deleteCard:(NSString *)cardId{
    NSLog(@"%@",cardId);
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/billing_delete"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     cardId, @"cardid",
                                     nil];
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionDeleteCard = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (IBAction)makeThisMyDefaultCreditCardBtnActn:(UIButton *)sender {
    if (self.makeThisMyDefaultCreditCardBtn.selected) {
        self.makeThisMyDefaultCreditCardBtn.selected = NO;
    } else {
        self.makeThisMyDefaultCreditCardBtn.selected = YES;
    }
}
- (IBAction)addBtnActn:(UIButton *)sender {
    if (self.cardTxtView.text.length == 0) {
        [ApplicationDelegate showAlertWithMessage:@"Please select a payment method" title:nil];
    } else if (self.nameTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your name" title:nil];
    }else if (self.cardNumberTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your card number" title:nil];
    }else if (self.expiryDateTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your card expiry date" title:nil];
    }else if (self.fullNameTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your full name" title:nil];
    }else if (self.address1TextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your address" title:nil];
    }else if (self.countryTxtView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please select your country" title:nil];
    }else if (self.cityTxtView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please select your city" title:nil];
    }else if (self.stateProvinceTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your state/province/region" title:nil];
    }else if (self.phoneNumberTextView.text.length == 0 ){
        [ApplicationDelegate showAlertWithMessage:@"Please enter your phone number" title:nil];
    }else{
        [self AddNewCard];
    }
}

-(void) AddNewCard{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://mawaqaatest.com/typo/webservice/billing_add"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     self.selectedCardId, @"cp_id",
                                     self.nameTextView.text, @"holder_name",
                                     self.cardNumberTextView.text, @"card_no",
                                     self.expiryDateTextView.text, @"month",
                                     self.expiryDateTextView.text, @"year",
                                     (self.makeThisMyDefaultCreditCardBtn.selected)? @"1":@"0", @"default",
                                     self.fullNameTextView.text, @"fullname",
                                     self.address1TextView.text, @"address",
                                     self.address1TextView.text, @"address1",
                                     self.address2TextView.text, @"address2",
                                     self.selectedCityId, @"city_id",
                                     self.selectedCountryId, @"country_id",
                                     self.stateProvinceTextView.text, @"state",
                                     self.postalCodeTextView.text, @"zip",
                                     self.phoneNumberTextView.text, @"phone",
                                     nil];
        NSLog(@"%@",requestData);
        
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        connectionAddNewCard = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        
        self.createNewCardBtn.selected = NO;
        [self.createNewCardArrowView setImage:[UIImage imageNamed:@"rightarrow1.png"]];
        self.createNewCardDetailsBackView.hidden = YES;
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

@end
