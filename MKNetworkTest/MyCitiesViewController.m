//
//  MyCitiesViewController.m
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "MyCitiesViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SDWebImage/UIImageView+WebCache.h"
#import "SectionInfo.h"
#import "SectionInfoCities.h"
#import "CityDetails.h"
#import "MyCitiesTableViewCell.h"
#import "MyCitiesEditViewController.h"
#import "AddCityViewController.h"

@interface MyCitiesViewController ()
@property (nonatomic, assign) NSInteger openSectionIndex;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSArray * cityList;
- (void) setCategoryArray;
@end

@implementation MyCitiesViewController

@synthesize sectionInfoArray;
@synthesize openSectionIndex;

-(void)viewWillAppear:(BOOL)animated{
    self.myCitiesTableView.scrollEnabled = YES;
    
    self.openSectionIndex = NSNotFound;
    
    userLoginStatus = [NSUserDefaults standardUserDefaults];
    
    if ([ApplicationDelegate isValid:[userLoginStatus objectForKey:@"UserId"]]) {
        [self getMyCityData];
    }
    
    self.myCitiesTableView.sectionHeaderHeight = 45;
    self.myCitiesTableView.sectionFooterHeight = 0;
    self.openSectionIndex = NSNotFound;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [HomeViewController sharedViewController].backBtn.hidden = NO;
    [HomeViewController sharedViewController].citiesBtn.hidden = YES;
    [HomeViewController sharedViewController].cityLabel.text = @"My Cities";
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getMyCityData{
    
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/get_my_cities"]];
        
        //NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
        // [userLoginStatus objectForKey:@"UserId"], @"user_id",
        // nil];
        
        NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                     @"3", @"user_id",
                                     nil];
        NSError *error;
        NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        [request setHTTPBody:postData];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return [self.cityList count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    SectionInfoCities *array = [self.sectionInfoArray objectAtIndex:section];
    
    NSInteger rows = 1;
    NSLog(@"row count...%ld",(long)rows);
    return (array.open) ? rows : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    MyCitiesTableViewCell *cell = (MyCitiesTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"MyCitiesTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    CityDetails * tempCityDetails;
    tempCityDetails = [self.cityList objectAtIndex:indexPath.section];
    
    NSMutableDictionary *CitydetailsDictionary = [[NSMutableDictionary alloc]init];
    NSLog(@"%@",tempCityDetails.cityName);
    CitydetailsDictionary = tempCityDetails.cityName;
    //NSLog(@"%@",CitydetailsDictionary);
    //NSLog(@"%ld",(long)indexPath.section);
    //NSLog(@"%@",self.myOtherCityArray);
    if ([[self.myOtherCityArray objectAtIndex:indexPath.section] isEqualToString:@"0"]) {
        [cell.tickBox setImage:[UIImage imageNamed:@"tick_box.png"]];
    } else {
        [cell.tickBox setImage:[UIImage imageNamed:@"tick_box_sel.png"]];
    }
    
    
    return cell;
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    
    SectionInfo *array = [self.sectionInfoArray objectAtIndex:indexPath.section];
    return [[array objectInRowHeightsAtIndex:indexPath.row] floatValue];
}

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    SectionInfoCities *array  = [self.sectionInfoArray objectAtIndex:section];
    if (!array.sectionViewCities)
    {
        NSString *title = array.citydetails.cityName;
        array.sectionViewCities = [[SectionViewCities alloc] initWithFrame:CGRectMake(0, 0, self.myCitiesTableView.bounds.size.width, 45) WithTitle:title Section:section delegate:self];
    }
    
    return array.sectionViewCities;
}
#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    for (int i =0; i< self.myOtherCityArray.count; i++) {
        if (indexPath.section == i) {
            [self.myOtherCityArray setObject:@"1" atIndexedSubscript:i];
        } else {
            [self.myOtherCityArray setObject:@"0" atIndexedSubscript:i];
        }
    }
    [self.myCitiesTableView reloadData];
}


- (void) sectionClosed : (NSInteger) section{
    
    SectionInfoCities *sectionInfo = [self.sectionInfoArray objectAtIndex:section];
    
    sectionInfo.open = NO;
    NSInteger countOfRowsToDelete = [self.myCitiesTableView numberOfRowsInSection:section];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:section]];
        }
        [self.myCitiesTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    self.openSectionIndex = NSNotFound;
    
}

- (void) sectionOpened : (NSInteger) section
{
    SectionInfoCities *array = [self.sectionInfoArray objectAtIndex:section];
    
    array.open = YES;
    NSInteger count = 1;
    NSLog(@"11.....%d",count);
    NSMutableArray *indexPathToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i<count;i++)
    {
        [indexPathToInsert addObject:[NSIndexPath indexPathForRow:i inSection:section]];
    }
    
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    NSInteger previousOpenIndex = self.openSectionIndex;
    
    if (previousOpenIndex != NSNotFound)
    {
        SectionInfoCities *sectionArray = [self.sectionInfoArray objectAtIndex:previousOpenIndex];
        sectionArray.open = NO;
        NSInteger counts = 1;
        [sectionArray.sectionViewCities toggleButtonPressed:FALSE];
        for (NSInteger i = 0; i<counts; i++)
        {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenIndex]];
        }
    }
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenIndex == NSNotFound || section < previousOpenIndex)
    {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else
    {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    [self.myCitiesTableView beginUpdates];
    [self.myCitiesTableView insertRowsAtIndexPaths:indexPathToInsert withRowAnimation:insertAnimation];
    [self.myCitiesTableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.myCitiesTableView endUpdates];
    self.openSectionIndex = section;
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    responseData = [[NSMutableData alloc] init];
    myCitiesDataDictionary = [[NSMutableDictionary alloc]init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    NSError* error;
    
    myCitiesDataDictionary = [NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error];
    NSLog(@"%@",myCitiesDataDictionary);
    
    self.myOtherCityArray = [[NSMutableArray alloc] initWithCapacity:[[myCitiesDataDictionary objectForKey:@"city_list"] count]];
    
    if ([[myCitiesDataDictionary objectForKey:@"city_list"] count] > 0) {
        
        NSMutableArray *citiesArray = [[NSMutableArray alloc] initWithCapacity:[[myCitiesDataDictionary objectForKey:@"city_list"] count]];
        NSLog(@"%@",[myCitiesDataDictionary objectForKey:@"city_list"]);
        for (NSDictionary *dictionary in [myCitiesDataDictionary objectForKey:@"city_list"]) {
            CityDetails *cityDetails = [[CityDetails alloc] init];
            cityDetails.cityName = [dictionary objectForKey:@"city_name"];
            cityDetails.cityId = [dictionary objectForKey:@"mc_id"];
            [self.myOtherCityArray addObject:@"0"];
            [citiesArray addObject:cityDetails];
        }
        self.cityList = citiesArray;
        
        if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.myCitiesTableView])) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            
            for (CityDetails * citydetails in self.cityList) {
                SectionInfoCities *section = [[SectionInfoCities alloc] init];
                section.citydetails = citydetails;
                section.open = NO;
                
                NSNumber *defaultHeight = [NSNumber numberWithInt:44];
                //NSInteger count = [[section.follower followerDetails] count];
                NSInteger count = 1;
                //NSLog(@"%d count...",count);
                
                for (NSInteger i= 0; i<count; i++) {
                    
                    NSLog(@"5.....%@",[section.citydetails cityName]);
                    [section insertObject:defaultHeight inRowHeightsAtIndex:i];
                    NSLog(@"6......%@",section);
                }
                
                [array addObject:section];
            }
            //NSLog(@"%@",array);
            self.sectionInfoArray = array;
        }
        
        
        [self.myCitiesTableView reloadData];
        
//        CGRect frame = self.myCitiesTableView.frame;
//        frame.size.height = self.myCitiesTableView.contentSize.height;
//        self.myCitiesTableView.frame = frame;
        
        //self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.myCitiesTableView.frame.origin.y + self.myCitiesTableView.frame.size.height);
        
        [ApplicationDelegate removeProgressHUD];
    }
    
    [ApplicationDelegate removeProgressHUD];
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)editButtonAction:(UIButton *)sender {
    MyCitiesEditViewController *myCitiesEditVc = [[MyCitiesEditViewController alloc] initWithNibName:@"MyCitiesEditViewController" bundle:nil];
    myCitiesEditVc.view.backgroundColor = [UIColor clearColor];
    
    //ApplicationDelegate.homeTabNav = [[UINavigationController alloc] initWithRootViewController:grabitDealsDetailVc];
    
    //ApplicationDelegate.homeTabNav.navigationBar.translucent = NO;
    //ApplicationDelegate.homeTabNav.view.backgroundColor = [UIColor clearColor];
    //ApplicationDelegate.homeTabNav.navigationBarHidden=YES;
    
    //ApplicationDelegate.homeTabNav.view.frame=CGRectMake(0, 0, self.homeTabContainerView.frame.size.width, self.homeTabContainerView.frame.size.height);
    
    //[self.myCitiesInnerContainerView addSubview:myCitiesEditVc];
    
    if (![ApplicationDelegate.homeTabNav.visibleViewController isKindOfClass:[myCitiesEditVc class]])
    {
        [ApplicationDelegate.homeTabNav pushViewController:myCitiesEditVc animated:NO];
    }
}
- (IBAction)addAnotherCityBtnActn:(UIButton *)sender {
    
//    CATransition *transition = [CATransition animation];
//    transition.duration = 0.3;
//    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
//    transition.type = kCATransitionPush;
//    transition.subtype = kCATransitionFromRight;
//    [self.view.window.layer addAnimation:transition forKey:nil];
//    AddAnotherCityViewController * addAnotherCityViewObj = [[AddAnotherCityViewController alloc]initWithNibName:@"AddAnotherCityViewController" bundle:nil];
//    addAnotherCityViewObj.locationString = @"NotLoged";
//    [self.view.window.rootViewController presentViewController:addAnotherCityViewObj animated:NO completion:nil];
//    //[self.navigationController presentViewController:addAnotherCityViewObj animated:NO completion:nil];
    
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromRight;
    [self.view.window.layer addAnimation:transition forKey:nil];
    AddCityViewController * addCityViewObj = [[AddCityViewController alloc]initWithNibName:@"AddCityViewController" bundle:nil];
    addCityViewObj.locationString = @"NotLoged";
    [self.view.window.rootViewController presentViewController:addCityViewObj animated:NO completion:nil];
    //[self.navigationController presentViewController:addAnotherCityViewObj animated:NO completion:nil];
}
@end
