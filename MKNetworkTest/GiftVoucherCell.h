//
//  GiftVoucherCell.h
//  WeGrab
//
//  Created by satheesh on 1/29/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftVoucherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *voucherAmountLbl;

@end
