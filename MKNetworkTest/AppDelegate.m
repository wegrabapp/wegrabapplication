//
//  AppDelegate.m
//  MKNetworkTest
//
//  Created by Sreeraj VR on 29/10/2014.
//  Copyright (c) 2014 mawaqaa. All rights reserved.
//

#import "AppDelegate.h"
#import "MarqueeLabel.h"
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    self.homeTabObj = [[HomeViewController alloc]
                       initWithNibName:@"HomeViewController" bundle:nil];
    self.mainNav = [[UINavigationController alloc]initWithRootViewController:[HomeViewController sharedViewController]];
    //self.homeNav = [[UINavigationController alloc] initWithRootViewController:[HomeTabViewController sharedViewController]];
    self.mainNav.navigationBar.translucent = NO;
    self.mainNav.navigationBar.hidden = YES;
    
    [self initializer];
    
    [application setStatusBarHidden:YES];    
     self.engine = [[AppEngine alloc] initWithHostName:kHOST_URL customHeaderFields:nil];
  
    [self.engine useCache];
      self.launchViewObj = [[LaunchingScreenViewController alloc] initWithNibName:@"LaunchingScreenViewController" bundle:nil];
    UINavigationController *navigation = [[UINavigationController alloc]initWithRootViewController:self.launchViewObj];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.rootViewController = navigation;
    [self.window makeKeyAndVisible];
    self.HUD = [[MBProgressHUD alloc] initWithView:self.launchViewObj.view];
    [ApplicationDelegate.HUD.delegate self];
    return YES;
}



							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


-(void)addProgressHUDToView:(UIView *)parentview
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD removeFromSuperview];
    }
    ApplicationDelegate.HUD.center = parentview.center;
    [parentview addSubview:ApplicationDelegate.HUD];
    [ApplicationDelegate.HUD removeFromSuperViewOnHide];
    [ApplicationDelegate.HUD show:YES];
}
-(void)removeProgressHUD
{
    if ([ApplicationDelegate.HUD superview]) {
        [ApplicationDelegate.HUD hide:YES];
    }
}

-(BOOL)isValid:(id)sender
{
    BOOL valid = NO;
    if ((sender!=nil)&&(![sender isEqual:[NSNull null]]))
    {
        valid=YES;
    }
    return valid;
}

-(void)initializer
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    internetReachable = [Reachability reachabilityForInternetConnection];
    [internetReachable startNotifier];
    
    self.engine = [[AppEngine alloc] initWithHostName:kHOST_URL customHeaderFields:nil];
    [self.engine emptyCache];
    [self.engine useCache];
    self.mapper = [[ObjectMapper alloc] init];
}

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    
    if (internetStatus == NotReachable)
    {
        UIAlertView *erroralert = [[UIAlertView alloc] initWithTitle:@"Message" message:@"Sorry, network appears to be offline. Please try later."delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil, nil];
        [erroralert show];
    }
    else
    {
        
    }
}

-(BOOL)checkNetworkAvailability
{
    BOOL status;
    Reachability *network = [Reachability reachabilityForInternetConnection];
    NetworkStatus statusOfConnection = [network currentReachabilityStatus];
    if (statusOfConnection == NotReachable)
    {
        status = NO;
    }
    else
    {
        status = YES;
    }
    return status;
}


-(void)showAlertWithMessage:(NSString *)message title:(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle: title
                              message: message
                              delegate: nil
                              cancelButtonTitle: @"Okay"
                              otherButtonTitles: nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        //Show alert here
        [alertView show];
    });
    //[alertView show];
}

-(NSString *)getHTTPCorrectedURLFromUrl:(NSString *)urlString
{
    NSString *correctedURLString;
    if ([ApplicationDelegate isValid:urlString])
    {
        if (urlString.length>0) {
            if (![urlString hasPrefix:@"http"])
            {
                correctedURLString = [NSString stringWithFormat:@"http://%@",urlString];
            }
            else
            {
                correctedURLString = urlString;
            }
        }
    }
    
    return correctedURLString;
}

-(void)loadMarqueeLabelWithText:(NSString *)lblText Font:(UIFont *)fontVal InPlaceOfLabel:(UILabel *)uilbl
{
    for (UIView *lb in uilbl.superview.subviews)
    {
        if ([lb isKindOfClass:[MarqueeLabel class]])
        {
            if (lb.frame.origin.x==uilbl.frame.origin.x)
            {
                [lb removeFromSuperview];
            }
            
        }
        
    }
    
    uilbl.hidden = YES;
    
    if (lblText.length>0)
    {
        MarqueeLabel *nameLabel = [[MarqueeLabel alloc] initWithFrame:uilbl.frame];
        //        if ([ApplicationDelegate.language isEqualToString:kENGLISH])
        //        {
        nameLabel.marqueeType = MLContinuous;
        //        }
        //        else
        //        {
        //            nameLabel.marqueeType = MLContinuousReverse;
        //        }
        
        //nameLabel.scrollDuration = 8.0;
        CGSize messageSize = [lblText sizeWithAttributes:@{NSFontAttributeName:fontVal}];
        float duration = messageSize.width / 20;
        nameLabel.scrollDuration = duration;
        
        nameLabel.animationCurve = UIViewAnimationOptionCurveEaseInOut;
        nameLabel.fadeLength = 10.0f;
        nameLabel.continuousMarqueeExtraBuffer = 10.0f;
        nameLabel.textColor=uilbl.textColor;
        nameLabel.font = fontVal;
        nameLabel.text = lblText;
        nameLabel.textAlignment = uilbl.textAlignment;
        nameLabel.backgroundColor = [UIColor clearColor];
        [uilbl.superview addSubview:nameLabel];
    }
    
}

@end
