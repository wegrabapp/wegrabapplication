//
//  AddCityViewController.m
//  WeGrab
//
//  Created by satheesh on 2/3/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import "AddCityViewController.h"
#import "CountryDetails.h"
#import "AddCityNormalTableViewCell.h"
//#import "AddCityButtonTableViewCell.h"

@interface AddCityViewController ()
@property (nonatomic, strong) NSArray * countryList;
@property (nonatomic, strong) NSMutableArray *sectionInfoArray;
@property (nonatomic, strong) NSMutableArray *countryNameArray;
@property (nonatomic, strong) NSMutableArray *citySelectionListArray;
@property (nonatomic, strong) NSString *addedCountryId;
@property (nonatomic, strong) NSString *addedCityId;
@property (nonatomic, strong) NSString * tickStatus;
@end

@implementation AddCityViewController
@synthesize countryList;
@synthesize sectionInfoArray;
@synthesize countryNameArray;

-(void)viewWillAppear:(BOOL)animated{
    [self getCountryData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.addedCountryId = [[NSString alloc]init];
    self.tickStatus = @"NO";
    if ([self.locationString isEqualToString:@"Initial"]) {
        self.cancelBtn.hidden = YES;
    } else if ([self.locationString isEqualToString:@"NotLoged"]){
        userLocation = [NSUserDefaults standardUserDefaults];
        if (([userLocation objectForKey:@"City"] == nil) || ([[userLocation objectForKey:@"City"]isEqual:[NSNull null]])) {
            NSLog(@"Empty");
            
        } else {
            self.cityLabel.text = [userLocation objectForKey:@"City"];
        }
        self.cancelBtn.hidden = NO;
        self.labelBackView.hidden = NO;
//        CGRect tempFrame = self.countryTableView.frame;
//        self.countryTableView.frame = CGRectMake(tempFrame.origin.x, tempFrame.origin.y+(self.labelBackView.frame.size.height - 8), tempFrame.size.width, tempFrame.size.height-(self.labelBackView.frame.size.height - 8));
    }
    self.countryTableView.sectionHeaderHeight = 45;
    self.countryTableView.sectionFooterHeight = 0;
}

#pragma  mark - Initialization

-(void)getCountryData{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/cities"]];
        NSError *error;
        [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
        [request setHTTPMethod:@"POST"];
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self];
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

#pragma mark -
#pragma mark TableView DataSource and Delegate Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ([[arrayForBool objectAtIndex:section] boolValue]) {
        CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:section];
        if ([country.cities count] == 0) {
            return ([country.cities count]);
        }
        else{
            return ([country.cities count] + 1);
        }
    }
    else
        return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:indexPath.section];
    static NSString *cellid;
    cellid = @"hello";

    AddCityNormalTableViewCell *cell =(AddCityNormalTableViewCell*)[self.countryTableView dequeueReusableCellWithIdentifier:cellid];
    
    if (cell == nil) {
        NSString * myNib;
        myNib = @"AddCityNormalTableViewCell";
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:myNib owner:self options:nil];
        if (indexPath.row == country.cities.count) {
            cell = [topLevelObjects objectAtIndex:1];
        }else {
            cell = [topLevelObjects objectAtIndex:0];
        }
        
    }
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    
    /********** If the section supposed to be closed *******************/
    if(!manyCells)
    {
        cell.backgroundColor=[UIColor clearColor];
        
        cell.textLabel.text=@"";
    }
    /********** If the section supposed to be Opened *******************/
    else
    {
        if (indexPath.row == country.cities.count) {
            cell.addBtn.tag = indexPath.section;
            [cell.addBtn addTarget:self action:@selector(addButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        } else {
            static NSMutableArray *colors = nil;
            if (colors == nil) {
                colors = [[NSMutableArray alloc] initWithCapacity:3];
                UIColor *color = nil;
                color = [UIColor colorWithRed:0.3 green:0.3 blue:0.3 alpha:1];
                [colors addObject:(id)[color CGColor]];
                color = [UIColor colorWithRed:0.2 green:0.2 blue:0.2 alpha:1];
                [colors addObject:(id)[color CGColor]];
                color = [UIColor colorWithRed:0.1 green:0.1 blue:0.1 alpha:1];
                [colors addObject:(id)[color CGColor]];
            }
            CAGradientLayer *gradient = [CAGradientLayer layer];
            gradient.frame = cell.bounds;
            gradient.frame = CGRectMake(gradient.frame.origin.x, gradient.frame.origin.y, gradient.frame.size.width, 44);
            [gradient setColors:colors];
            [gradient setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0], [NSNumber numberWithFloat:0.48], [NSNumber numberWithFloat:1.0], nil]];
            [cell.backGroundView.layer addSublayer:gradient];
        
            cell.CityNameLabel.text = [[country.cities objectAtIndex:indexPath.row] objectForKey:@"city_name"];
            NSLog(@"hhh......%@",self.citySelectionListArray);
            if ((self.citySelectionListArray.count == 0)||[self.tickStatus isEqualToString:@"NO"]) {
                [self.citySelectionListArray setObject:@"0" atIndexedSubscript:indexPath.row];
            } else {
                if ([[self.citySelectionListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
                    [cell.tickBox setImage:[UIImage imageNamed:@"tick_box_sel.png"]];
                } else {
                    [cell.tickBox setImage:[UIImage imageNamed:@"tick_box.png"]];
                }
            }
        }
    }
    
    /********** Add a custom Separator with cell *******************/
//    UIView* separatorLineView = [[UIView alloc] initWithFrame:CGRectMake(15, 40, self.countryTableView.frame.size.width-15, 1)];
//    separatorLineView.backgroundColor = [UIColor blackColor];
//    [cell.contentView addSubview:separatorLineView];
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    //return [sectionTitleArray count];
    return [self.countryList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:indexPath.section];
    /*************** Close the section, once the data is selected ***********************************/
    self.tickStatus = @"YES";
    self.addedCountryId = country.country_id;
    if (country.cities.count == indexPath.row) {
        
    } else {
        if ([[self.citySelectionListArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            [self.citySelectionListArray setObject:@"0" atIndexedSubscript:indexPath.row];
        } else {
            [self.citySelectionListArray setObject:@"1" atIndexedSubscript:indexPath.row];
        }
        [self.countryTableView reloadData];
    }
    
//    [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:NO]];
//    
//    [self.countryTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:indexPath.section];
        if (indexPath.row == country.cities.count) {
            return 60;
        } else {
            return 40;
        }
    }
    return 0;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

#pragma mark - Creating View for TableView Section

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:section];
    UIView *sectionView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.countryTableView.frame.size.width,40)];
    sectionView.tag=section;
    
    UIImageView * backGroundView = [[UIImageView alloc]init];
    backGroundView.frame = sectionView.bounds;
    backGroundView.backgroundColor = [UIColor darkGrayColor];
    [sectionView addSubview:backGroundView];
    UIImageView * seperatorView = [[UIImageView alloc]init];
    seperatorView.frame = CGRectMake(backGroundView.frame.origin.x, backGroundView.frame.size.height-1, backGroundView.frame.size.width,1);
    seperatorView.backgroundColor = [UIColor lightGrayColor];
    [sectionView addSubview:seperatorView];
    
    CGRect LabelFrame = sectionView.bounds;
    LabelFrame.size.width -= 30;
    LabelFrame.origin.x += 10;
    CGRectInset(LabelFrame, 0.0, 5.0);
    
    UILabel *label = [[UILabel alloc] initWithFrame:LabelFrame];
    label.text = [NSString stringWithFormat:@"%@",country.countryName];
    label.font = [UIFont boldSystemFontOfSize:16.0];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentLeft;
    [sectionView addSubview:label];
    //self.sectionTitle = label;
    
    CGRect buttonFrame = CGRectMake(LabelFrame.size.width, 0, 20, LabelFrame.size.height);
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = buttonFrame;
    [button setImage:[UIImage imageNamed:@"rightarrow1.png"] forState:UIControlStateNormal];
    [button setImage:[UIImage imageNamed:@"downarrow1.png"] forState:UIControlStateSelected];
    BOOL selectionStatus  = [[arrayForBool objectAtIndex:section] boolValue];
    if (selectionStatus) {
        button.selected = YES;
    }
    else{
        button.selected = NO;
    }
    button.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [button addTarget:self action:@selector(sectionHeaderTapped:) forControlEvents:UIControlEventTouchUpInside];
    //button.selected = YES;
    [sectionView addSubview:button];
    //self.discButton = button;
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [sectionView addGestureRecognizer:headerTapped];
    
    return  sectionView;
}


#pragma mark - Table header gesture tapped

- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    self.citySelectionListArray = [[NSMutableArray alloc]init];
    self.tickStatus = @"NO";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    NSLog(@"%@",indexPath);
    NSLog(@"%ld",(long)indexPath.row);
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        for (int i=0; i<[self.countryList count]; i++) {
            if (indexPath.section==i) {
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:!collapsed]];
            }
            else{
                [arrayForBool replaceObjectAtIndex:i withObject:[NSNumber numberWithBool:NO]];
            }
        }
        [self.countryTableView reloadData];
        [self.countryTableView reloadSections:[NSIndexSet indexSetWithIndex:gestureRecognizer.view.tag] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////// nsurl ///////////////////////

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    if (connection == connectionAddMyCity) {
        responseDataAddCity = [[NSMutableData alloc] init];
        addCityDataDictionary = [[NSMutableDictionary alloc]init];
    } else {
        responseData = [[NSMutableData alloc] init];
        countryDataDictionary = [[NSMutableDictionary alloc]init];
    }
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    if (connection == connectionAddMyCity) {
        [responseDataAddCity appendData:data];
    } else {
        [responseData appendData:data];
    }
}

- (NSCachedURLResponse *)connection:(NSURLConnection *)connection
                  willCacheResponse:(NSCachedURLResponse*)cachedResponse {
    // Return nil to indicate not necessary to store a cached response for this connection
    return nil;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    if (connection == connectionAddMyCity) {
        NSError* error;
        addCityDataDictionary = [NSJSONSerialization JSONObjectWithData:responseDataAddCity options:kNilOptions error:&error];
        NSLog(@"%@",addCityDataDictionary);
        
        [ApplicationDelegate showAlertWithMessage:[addCityDataDictionary objectForKey:@"message"] title:nil];
        
        [ApplicationDelegate removeProgressHUD];
    } else {
        NSError* error;
        [countryDataDictionary setObject:[[NSJSONSerialization JSONObjectWithData:responseData options:kNilOptions error:&error]objectForKey:@"countries"] forKey:@"countries"];
        NSLog(@"%@",countryDataDictionary);
        
        if ([[countryDataDictionary objectForKey:@"countries"] count] > 0) {
            
            NSMutableArray *countryArray = [[NSMutableArray alloc] initWithCapacity:[[countryDataDictionary objectForKey:@"countries"] count]];
            for (NSDictionary *dictionary in [countryDataDictionary objectForKey:@"countries"]) {
                CountryDetails *countryDetails = [[CountryDetails alloc] init];
                countryDetails.country_id = [dictionary objectForKey:@"country_id"];
                countryDetails.countryName = [dictionary objectForKey:@"CountryName"];
                countryDetails.cities = [dictionary objectForKey:@"cities"];
                [countryArray addObject:countryDetails];
            }
            self.countryList = countryArray;
            
            if ((self.sectionInfoArray == nil)|| ([self.sectionInfoArray count] != [self numberOfSectionsInTableView:self.countryTableView])) {
                NSMutableArray *array = [[NSMutableArray alloc] init];
                arrayForBool=[[NSMutableArray alloc]init];
                self.countryNameArray = [[NSMutableArray alloc]init];
                for (CountryDetails *country in self.countryList) {
                    [arrayForBool addObject:[NSNumber numberWithBool:NO]];
                    [array addObject:country];
                }
                self.sectionInfoArray = array;
            }
            [self.countryTableView reloadData];
            [ApplicationDelegate removeProgressHUD];
        }
    }
    [ApplicationDelegate removeProgressHUD];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
}


////////////////////// nsurl ///////////////////////

- (IBAction)cancelBtnActn:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    CATransition *transition = [CATransition animation];
    transition.duration = 0.3;
    transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    transition.type = kCATransitionPush;
    transition.subtype = kCATransitionFromLeft;
    [self.view.window.layer addAnimation:transition forKey:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(IBAction)addButtonAction:(UIButton *)sender{
    NSLog(@"%ld",(long)sender.tag);
    self.addedCityId = [[NSString alloc]init];
    CountryDetails *country = (CountryDetails *)[self.countryList objectAtIndex:sender.tag];
    NSLog(@"%@",country.cities);
    for (int i = 0; i<country.cities.count; i++) {
        if ([[self.citySelectionListArray objectAtIndex:i]isEqualToString:@"1"]) {
            NSLog(@"%@",[country.city_id objectAtIndex:i]);
            self.addedCityId = [self.addedCityId stringByAppendingString:[NSString stringWithFormat:@"%@,",[[country.cities objectAtIndex:i]objectForKey:@"city_id"]]];
        }
    }
    if (self.addedCityId.length > 0) {
        self.addedCityId = [self.addedCityId substringToIndex:[self.addedCityId length] - 1];
        NSLog(@"%@",self.addedCityId);
        [self loadAddAnotherCityService];
    } else {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, please select a city." title:nil];
    }
}

-(void)loadAddAnotherCityService{
    if ([ApplicationDelegate checkNetworkAvailability])
    {
        
        [ApplicationDelegate addProgressHUDToView:self.view];
        
        userLoginStatus = [NSUserDefaults standardUserDefaults];
        
        NSMutableURLRequest *request = [NSMutableURLRequest
                                        requestWithURL:[NSURL URLWithString:@"http://wegrab.mawaqaatest.com/webservice/add_my_cities"]];
        
        if ([ApplicationDelegate isValid:self.addedCountryId]) {
            
            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
                                         @"3", @"user_id",
                                         self.addedCountryId, @"country_id",
                                         self.addedCityId, @"city_id",
                                         nil];
            
            NSLog(@"%@",requestData);
            
//            NSDictionary *requestData = [[NSDictionary alloc] initWithObjectsAndKeys:
//                                         [userLoginStatus objectForKey:@"UserId"], @"user_id",
//                                         self.addedCountryId, @"country_id",
//                                         self.addedCityId, @"city_id",
//                                         nil];
            
            NSError *error;
            NSData *postData = [NSJSONSerialization dataWithJSONObject:requestData options:0 error:&error];
            [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPMethod:@"POST"];
            [request setHTTPBody:postData];
            connectionAddMyCity = [[NSURLConnection alloc] initWithRequest:request delegate:self];
        } else {
            [ApplicationDelegate showAlertWithMessage:@"Sorry, please select a city to delete" title:nil];
            [ApplicationDelegate removeProgressHUD];
        }
    }
    
    else
    {
        [ApplicationDelegate showAlertWithMessage:@"Sorry, network appears to be offline. Please try later." title:nil];
        
    }
}

@end
