//
//  BillingTableCell.h
//  WeGrab
//
//  Created by Ronish on 2/9/16.
//  Copyright © 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BillingTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UITextField *cardTypeTextView;
@property (weak, nonatomic) IBOutlet UITextField *nameTextView;
@property (weak, nonatomic) IBOutlet UITextField *cardNumberTextView;
@property (weak, nonatomic) IBOutlet UITextField *expiryDateTextView;

@property (weak, nonatomic) IBOutlet UIButton *makeThisDefaultBtn;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@end
