//
//  ShareItViewController.h
//  WeGrab
//
//  Created by satheesh on 1/6/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareItViewController : UIViewController{
    NSUserDefaults * userLocation;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

- (IBAction)cancelBtnActn:(UIButton *)sender;
@end
