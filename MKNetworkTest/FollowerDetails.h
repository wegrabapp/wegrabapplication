//
//  FollowerDetails.h
//  WeGrab
//
//  Created by satheesh on 1/20/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FollowerDetails : NSObject

@property (nonatomic, retain) NSString *followerName;
@property (nonatomic, retain) NSString *followerUserId;
@property (nonatomic, retain) NSArray *followerDetails;

@end
