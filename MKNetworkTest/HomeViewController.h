//
//  HomeViewController.h
//  WeGrab
//
//  Created by satheesh on 1/4/16.
//  Copyright (c) 2016 Mawaqaa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController{
    NSUserDefaults * userLocation;
    NSUserDefaults * userLoginStatus;
}
@property (weak, nonatomic) IBOutlet UIButton *moreGrabsBtn;
@property (weak, nonatomic) IBOutlet UIButton *howItWorksBtn;
@property (weak, nonatomic) IBOutlet UIButton *myAccountBtn;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UIView *homeTabContainerView;
@property (weak, nonatomic) IBOutlet UIButton *citiesBtn;
- (IBAction)citiesBtnActn:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *cityLabel;
+(HomeViewController*)sharedViewController;
- (IBAction)homeTabButtonAction:(UIButton *)sender;
- (IBAction)backBtnActn:(UIButton *)sender;
@end
