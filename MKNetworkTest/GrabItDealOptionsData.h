//
//  GrabItDealOptionsData.h
//  WeGrab
//
//  Created by satheesh on 1/21/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GrabItDealOptionsData : NSObject

@property (nonatomic, retain) NSString *optionID;
@property (nonatomic, retain) NSString *optionName;
@property (nonatomic, retain) NSString *optionPrice;
@property (nonatomic, retain) NSString *optionQuantity;

@end
