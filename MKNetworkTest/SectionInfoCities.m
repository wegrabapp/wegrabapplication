//
//  SectionInfoCities.m
//  WeGrab
//
//  Created by satheesh on 2/1/16.
//  Copyright (c) 2016 mawaqaa. All rights reserved.
//

#import "SectionInfoCities.h"

@implementation SectionInfoCities

@synthesize  open;
@synthesize category;
//@synthesize country;
//@synthesize follower;
//@synthesize balanceDetails;
@synthesize sectionViewCities;
@synthesize rowHeights;

- init {
    
    self = [super init];
    if (self) {
        rowHeights = [[NSMutableArray alloc] init];
    }
    return self;
}


- (NSUInteger)countOfRowHeights {
    return [rowHeights count];
}

- (id)objectInRowHeightsAtIndex:(NSUInteger)idx {
    return [rowHeights objectAtIndex:idx];
}

- (void)insertObject:(id)anObject inRowHeightsAtIndex:(NSUInteger)idx {
    [rowHeights insertObject:anObject atIndex:idx];
}

- (void)insertRowHeights:(NSArray *)rowHeightArray atIndexes:(NSIndexSet *)indexes {
    [rowHeights insertObjects:rowHeightArray atIndexes:indexes];
}

- (void)removeObjectFromRowHeightsAtIndex:(NSUInteger)idx {
    [rowHeights removeObjectAtIndex:idx];
}

- (void)removeRowHeightsAtIndexes:(NSIndexSet *)indexes {
    [rowHeights removeObjectsAtIndexes:indexes];
}

- (void)replaceObjectInRowHeightsAtIndex:(NSUInteger)idx withObject:(id)anObject {
    [rowHeights replaceObjectAtIndex:idx withObject:anObject];
}

- (void)replaceRowHeightsAtIndexes:(NSIndexSet *)indexes withRowHeights:(NSArray *)rowHeightArray {
    [rowHeights replaceObjectsAtIndexes:indexes withObjects:rowHeightArray];
}


@end
